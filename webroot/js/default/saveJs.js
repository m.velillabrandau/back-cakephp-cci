
    // $( document ).ready(function() {

    //     $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    //         checkboxClass: 'icheckbox_minimal-red',
    //         radioClass   : 'iradio_minimal-red'
    //     });

    //     function getRandomInt(min, max) {
    //         return Math.floor(Math.random() * (max - min)) + min;
    //     }

    //     function cleanClass(){
    //         $('li.sortable-index-element').each(function( index ){
    //             $(this).removeClass("click-ag-mark");
    //             $(this).find('.index-ag-name').css( "color", "black" );
    //         });
    //     }

    //     function markAllCheck(type){
    //         switch(type) {
    //             case 1:
    //                 $('div.sortable-world-element').each(function( index ){
    //                     $(this).find('.icheckbox_minimal-red').addClass('checked');
    //                 });
    //                 break;
    //             case 2:
    //                 $('div.sortable-world-element').each(function( index ){
    //                     $(this).find('.icheckbox_minimal-red').removeClass('checked');
    //                 });
    //                 break;
    //         }
    //     }

    //     $('.select-all').on('click',function(){
    //         markAllCheck(1);
    //     });

    //     $('.disable-all').on('click',function(){
    //         markAllCheck(2);
    //     });

    //     //FUNCION QUE REINICIA LA BARRA DE CARGA
    //     $(document).ajaxStart(function () {
    //         Pace.restart();
    //     });

    //     //FUNCION PARA ORDERNAR LOS MUNDOS EN EL INDICE
    //     $( ".sortable-world-index" ).on( "sortstop", function( event, ui ) {
    //         console.log('world index move');
    //         let idWorld = [];
    //         if ($(ui.item).hasClass('sortable-world-element')) {
    //             $('div.sortable-world-element',this).each(function( index ){
    //                 idWorld.push($(this).attr("data-id"));
    //             });
    //             if(idWorld){
    //                 $.ajax({
    //                     headers: {
    //                         'X-CSRF-Token': csrfToken
    //                     },
    //                     type: "POST",
    //                     url: "<?= $this->Url->build(["controller" => "Magazines", "action" => "orderWorlds"]) ?>",
    //                     dataType: 'json',
    //                     data: {
    //                         'idWorld': idWorld
    //                     }
    //                 })
    //                 .done(function(response){
    //                     toastr.success('Orden de mundo guardado','',{timeOut: 1000});
    //                 });
    //             }else{
    //                 toastr.error('Intente nuevamente','',{timeOut: 1000});
    //             }
    //         }
    //     });

    //     //FUNCION PARA ORDERNAR LOS GRUPOS DE ANALISIS
    //     $('.sortable-index').on('sortstop', function( event, ui ) {
    //         console.log('ga index move');
    //         let idAnalysisGroup = [];
    //         $('li.sortable-index-element',this).each(function( index ){
    //             idAnalysisGroup.push($(this).attr("data-id"));
    //         });
    //         if(idAnalysisGroup){
    //             $.ajax({
    //                 headers: {
    //                     'X-CSRF-Token': csrfToken
    //                 },
    //                 type: 'POST',
    //                 url: '<?= $this->Url->build(['controller' => 'Magazines', 'action' => 'orderAnalysisGroups']) ?>',
    //                 dataType: 'json',
    //                 data: {
    //                     'idAnalysisGroup': idAnalysisGroup
    //                 }
    //             })
    //             .done(function(response){
    //                 toastr.success('Orden de grupo de análisis guardado','',{timeOut: 1000});
    //             });
    //         }else{
    //             toastr.error('Intente nuevamente','',{timeOut: 1000});
    //         }
    //     });

    //     //FUNCION PARA OBTENER LA INFORMACION DE LOS MUNDOS SELECCIONADOS
    //     $('#accordion-index').on('accordionbeforeactivate', function( event, ui ) {
    //         cleanClass();
    //         console.log('click world event');
    //         var idWorldClick = ui.newHeader.parent().attr('data-id');
    //         if(idWorldClick!=undefined){
    //              $.ajax({
    //                 headers: {
    //                     'X-CSRF-Token': csrfToken
    //                 },
    //                 type: 'POST',
    //                 url: '<?= $this->Url->build(['controller' => 'Magazines', 'action' => 'getWorldInfo']) ?>',
    //                 dataType: 'json',
    //                 data: {
    //                     'idWorldClick': idWorldClick
    //                     },
    //                 })
    //             .done(function(response){
    //                 if(response.status==true){
    //                     htmlWorld = '<div class="box-body">';
    //                         htmlWorld += '<div class="row">';
    //                             htmlWorld += '<div class="col-md-12" id="">';
    //                                 htmlWorld += '<h2>FERIA INVIERNO 01 2019 / FERIA <?=$indexMagazine->ferias_id?></h2>';
    //                             htmlWorld += '<hr id="hr-world-magazine">';
    //                             htmlWorld += '</div>';    
    //                             htmlWorld += '<div class="col-md-12" id="">';
    //                                 htmlWorld += '<h3 class="pull-right">llegada Marzo 2019</h3>';
    //                                 htmlWorld += '</br>';
    //                             htmlWorld += '</div>';    
    //                             htmlWorld += '<div class="col-md-12" id="">';
    //                                 htmlWorld += '<div class="small-box bg-red pull-right" id="world-label-magazine">';
    //                                     htmlWorld += '<h3 class="pull-right">'+response.data_response.name.toUpperCase()+'</h3>';
    //                                 htmlWorld += '</div>';    
    //                             htmlWorld += '</div>';
    //                         htmlWorld += '</div>';
    //                     htmlWorld += '</div>';
    //                     $('#content-page-magazine').html(htmlWorld);
    //                 }else{
    //                     htmlWorld = '';
    //                     $('#content-page-magazine').html(htmlWorld);
    //                 }
    //             });
    //         }else{
    //             // toastr.error('Intente nuevamente','',{timeOut: 1000});
    //         }
    //     });

    //     //FUNCION AL HACER CLICK EN X DE PRODUCTO
    //     $(document).on('click','.delete-card-element',function() {
    //         let cardElement = $(this).closest('li#list-product');
    //         let idElementDisable = $(this).attr("data-id");
    //         if(idElementDisable){
    //             $.ajax({
    //                 headers: {
    //                     'X-CSRF-Token': csrfToken
    //                 },
    //                 type: "POST",
    //                 url: "<?= $this->Url->build(["controller" => "Magazines", "action" => "changeStateContent"]) ?>",
    //                 dataType: 'json',
    //                 data: {
    //                     'idElementDisable': idElementDisable,
    //                     'state':0
    //                 }
    //             })
    //             .done(function(response){
    //                 cardElement.remove();
    //                 toastr.success('producto eliminado '+idElementDisable,'',{timeOut: 1000});
    //             });
    //         }else{
    //            toastr.error('Intente nuevamente','',{timeOut: 1000});
    //         }
    //     });

    //     //FUNCION AL HACER CLICK EN ICONO DE ACTIVA DE PRODUCTO
    //     $(document).on('click','.active-card-element',function() {
    //         let cardElement = $(this).closest('li#list-product');
    //         let idElementDisable = $(this).attr("data-id");
    //         if(idElementDisable){
    //             $.ajax({
    //                 headers: {
    //                     'X-CSRF-Token': csrfToken
    //                 },
    //                 type: "POST",
    //                 url: "<?= $this->Url->build(["controller" => "Magazines", "action" => "changeStateContent"]) ?>",
    //                 dataType: 'json',
    //                 data: {
    //                     'idElementDisable': idElementDisable,
    //                     'state':1
    //                 }
    //             })
    //             .done(function(response){
    //                 cardElement.remove();
    //                 toastr.success('producto eliminado '+idElementDisable,'',{timeOut: 1000});
    //             });
    //         }else{
    //             toastr.error('Intente nuevamente','',{timeOut: 1000});
    //         }
    //     });

    //     //FUNCION PARA ORDERNAR LOS PRODUCTOS DENTRO DE UNA PAGINA
    //     $(document).on("sortbeforestop",".sortable",function() {
    //         console.log('content products move');
    //         let idProducts = [];
    //         $( "li#list-product" ).each(function( index ){
    //             idProducts.push($(this).attr("data-id"));
    //         });
    //         if(idProducts){
    //             $.ajax({
    //                 headers: {
    //                     'X-CSRF-Token': csrfToken
    //                 },
    //                 type: "POST",
    //                 url: "<?= $this->Url->build(["controller" => "Magazines", "action" => "orderProduct"]) ?>",
    //                 dataType: 'json',
    //                 data: {
    //                     'idProducts': idProducts
    //                 }

    //             })
    //             .done(function(response){

    //             });
    //         }else{
    //             toastr.error('Intente nuevamente','',{timeOut: 1000});
    //         }
    //     });

    //     //FUNCION PARA OBTENER LA INFORMACION DE UN GRUPO DE ANALISIS
    //     $(document).on('click','.sortable-index-element',function() {
    //         cleanClass();
    //         $(this).addClass("click-ag-mark");
    //         $(this).find('.index-ag-name').css( "color", "white" );
    //         console.log('click ga event');
    //         let idAgClick = $(this).attr("data-id");
    //         if(idAgClick!=undefined){
    //              $.ajax({
    //                 headers: {
    //                     'X-CSRF-Token': csrfToken
    //                 },
    //                 type: 'POST',
    //                 url: '<?= $this->Url->build(['controller' => 'Magazines', 'action' => 'getAgInfo']) ?>',
    //                 dataType: 'json',
    //                 data: {
    //                     'idAgClick': idAgClick
    //                     },
    //                 })
    //             .done(function(response){
    //                 if(response.status==true){
    //                     htmlWorld = '<div class="box-header">';
    //                         htmlWorld += '<br>';
    //                         htmlWorld += '<div class="row">';
    //                             htmlWorld += '<div class="col-md-12" id="header-page-magazine">';
    //                                 htmlWorld += '<h3 class="box-title pull-left">'+response.data_response.name+'</h3>';
    //                                 // htmlWorld += '<h3 class="box-title pull-right"><span style="color:#dd4b39;" contenteditable>'+response.data_response.ag_alias+'</span> / FERIA INVIERNO 01 2019 / FERIA <?=$indexMagazine->ferias_id?></h3>';
    //                             htmlWorld += '</div>';
    //                         htmlWorld += '</div>';
    //                         htmlWorld += '<br>';
    //                         htmlWorld += '<div class="row">';
    //                             htmlWorld += '<div class="col-md-12">';
    //                                 htmlWorld += '<button type="button" class="btn btn-success pull-right show-disable-elements" data-id="'+response.data_response.id+'">Elementos disponibles</button>';
    //                             htmlWorld += '</div>';
    //                         htmlWorld += '</div>';
    //                     htmlWorld += '</div>';
    //                     htmlWorld += '<hr>';
    //                         htmlWorld += '<div class="box-body">';
    //                             htmlWorld += '<div class="row">';
    //                                 htmlWorld += '<div class="col-md-12" id="page-content">';
    //                                 htmlWorld += '<ul id="contents-products" class="sortable contents-products">';

    //                                 if(response.data_response.contents!=''){
    //                                     response.data_response.contents.forEach(function(index){
    //                                         htmlWorld += '<li id="list-product" class="ui-state-default" data-id="'+index.id+'" data-order="">';
    //                                             htmlWorld += '<div id="card-style">';
    //                                                 htmlWorld += '<div id="card-product" class="row">';
    //                                                     htmlWorld += '<div class="info-options col-md-12" >';
    //                                                         htmlWorld += '<div class="pull-right">';
    //                                                             htmlWorld += '<a href="#" data-id="'+index.id+'" class="mark-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>';
    //                                                             htmlWorld += '<a href="#" data-id="'+index.id+'" class="delete-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>';
    //                                                         htmlWorld += '</div>';
    //                                                     htmlWorld += '</div>';
    //                                                     htmlWorld += '<div class="info-product col-md-12" >';
    //                                                         htmlWorld += '<dl>';
    //                                                             htmlWorld += '<dt>Nombre: <data id="name-product">'+index.name_desc+'</data></dt>';
    //                                                             htmlWorld += '<dt>Diseño: <data id="design-product">50 X 70 CM</data></dt>';
    //                                                             htmlWorld += '<dd>';
    //                                                                 htmlWorld += '<table style="width: 100%;">';
    //                                                                     htmlWorld += '<thead>';
    //                                                                         htmlWorld += '<tr>';
    //                                                                             htmlWorld += '<th><span>SKU</span></th>';
    //                                                                             htmlWorld += '<th><span>PP</span></th>';
    //                                                                         htmlWorld += '</tr>';
    //                                                                     htmlWorld += '</thead>';
    //                                                                     htmlWorld += '<tbody>';
    //                                                                         htmlWorld += '<tr>';
    //                                                                             htmlWorld += '<td><data id="sku-product">'+index.sku_mod+'</data></td>';
    //                                                                             htmlWorld += '<td><span>'+index.punta_precio+'</span></td>';
    //                                                                         htmlWorld += '</tr>';
    //                                                                     htmlWorld += '</tbody>';
    //                                                                 htmlWorld += '</table>';
    //                                                             htmlWorld += '</dd>';
    //                                                         htmlWorld += '</dl>';
    //                                                     htmlWorld += '</div>';
    //                                                     htmlWorld += '<div class="image-product col-md-12" >';
    //                                                         htmlWorld += '<div class="image-product" style="width: 100%; height: 100%;">';
    //                                                             htmlWorld += '<img src="https://picsum.photos/200/200?image='+getRandomInt(0,1000)+'" alt="product" style="width:85%; height:100%;">';
    //                                                         htmlWorld += '</div>';
    //                                                     htmlWorld += '</div>';
    //                                                 htmlWorld += '</div>';
    //                                             htmlWorld += '</div>';
    //                                         htmlWorld += '</li>';
    //                                     });
    //                                 }else{
    //                                     htmlWorld += '<div class="col-md-12" style="padding-left:0px; padding-right:35px;">';
    //                                         htmlWorld += '<div class="callout callout-success">';
    //                                             htmlWorld += '<h4>No se encontraron productos activos</h4>';
    //                                             htmlWorld += '<p></p>';
    //                                         htmlWorld += '</div>';
    //                                     htmlWorld += '</div>';
    //                                 }
                                                               
    //                                 htmlWorld += '</ul>';
    //                             htmlWorld += '</div>';
    //                         htmlWorld += '</div>';
    //                     htmlWorld += '</div>';
    //                     $('#content-page-magazine').html(htmlWorld);
    //                     $( ".sortable" ).sortable();
    //                     // .droppable({
    //                     //     // accept: ".sortable", 
    //                     //     // drop: function (event, ui) {
    //                     //     //     alert('test');
    //                     //     // }
    //                     // });
    //                     $( ".sortable" ).disableSelection();
    //                 }else{
    //                     toastr.error('Error al generar pagina','',{timeOut: 1000});
    //                 }
    //             });
    //         }else{
    //             toastr.error('Intente nuevamente','',{timeOut: 1000});
    //         }
    //     });

    //     //FUNCION PARA MOSTRAR LOS ELEMENTOS DESACTIVADOS
    //     $(document).on('click','.show-disable-elements',function() {
    //         let idAgShowDisable = $(this).attr("data-id");
    //         if(idAgShowDisable){
    //             $.ajax({
    //                 headers: {
    //                     'X-CSRF-Token': csrfToken
    //                 },
    //                 type: "POST",
    //                 url: "<?= $this->Url->build(["controller" => "Magazines", "action" => "showDisableContent"]) ?>",
    //                 dataType: 'json',
    //                 data: {
    //                     'idAgShowDisable': idAgShowDisable
    //                 }
    //             })
    //             .done(function(response){
    //                  if(response.status==true){
    //                     htmlWorld = '<div class="box-header">';
    //                         htmlWorld += '<br>';
    //                         htmlWorld += '<div class="row">';
    //                             htmlWorld += '<div class="col-md-12" id="header-page-magazine">';
    //                                 htmlWorld += '<h3 class="box-title pull-left">'+response.data_response.name+'</h3>';
    //                                 // htmlWorld += '<h3 class="box-title pull-right"><span style="color:#dd4b39;" contenteditable>'+response.data_response.ag_alias+'</span> / FERIA INVIERNO 01 2019 / FERIA <?=$indexMagazine->ferias_id?></h3>';
    //                             htmlWorld += '</div>';
    //                         htmlWorld += '</div>';
    //                         htmlWorld += '<br>';
    //                         htmlWorld += '<div class="row">';
    //                             htmlWorld += '<div class="col-md-12">';
    //                                 htmlWorld += '<button type="button" class="btn btn-danger pull-right sortable-index-element" data-id="'+response.data_response.id+'">Elementos eliminados</button>';
    //                             htmlWorld += '</div>';
    //                         htmlWorld += '</div>';
    //                     htmlWorld += '</div>';
    //                     htmlWorld += '<hr>';
    //                         htmlWorld += '<div class="box-body">';
    //                             htmlWorld += '<div class="row">';
    //                                 htmlWorld += '<div class="col-md-12" id="page-content">';
    //                                 htmlWorld += '<ul id="contents-products" class="sortable contents-products">';

    //                                 if(response.data_response.contents!=''){
    //                                     response.data_response.contents.forEach(function(index){
    //                                         htmlWorld += '<li id="list-product" class="ui-state-default" data-id="'+index.id+'" data-order="">';
    //                                             htmlWorld += '<div id="card-style">';
    //                                                 htmlWorld += '<div id="card-product" class="row">';
    //                                                     htmlWorld += '<div class="info-options col-md-12" >';
    //                                                         htmlWorld += '<div class="pull-right">';
    //                                                             htmlWorld += '<a href="#" data-id="'+index.id+'" class="active-card-element"><i class="glyphicon glyphicon-ok" style="color: #00a65a; padding-right: 5px;"></i></a>';
    //                                                         htmlWorld += '</div>';
    //                                                     htmlWorld += '</div>';
    //                                                     htmlWorld += '<div class="info-product col-md-12" >';
    //                                                         htmlWorld += '<dl>';
    //                                                             htmlWorld += '<dt>Nombre: <data id="name-product">'+index.name_desc+'</data></dt>';
    //                                                             htmlWorld += '<dt>Diseño: <data id="design-product">50 X 70 CM</data></dt>';
    //                                                             htmlWorld += '<dd>';
    //                                                                 htmlWorld += '<table style="width: 100%;">';
    //                                                                     htmlWorld += '<thead>';
    //                                                                         htmlWorld += '<tr>';
    //                                                                             htmlWorld += '<th><span>SKU</span></th>';
    //                                                                             htmlWorld += '<th><span>PP</span></th>';
    //                                                                         htmlWorld += '</tr>';
    //                                                                     htmlWorld += '</thead>';
    //                                                                     htmlWorld += '<tbody>';
    //                                                                         htmlWorld += '<tr>';
    //                                                                             htmlWorld += '<td><data id="sku-product">'+index.sku_mod+'</data></td>';
    //                                                                             htmlWorld += '<td><span>'+index.punta_precio+'</span></td>';
    //                                                                         htmlWorld += '</tr>';
    //                                                                     htmlWorld += '</tbody>';
    //                                                                 htmlWorld += '</table>';
    //                                                             htmlWorld += '</dd>';
    //                                                         htmlWorld += '</dl>';
    //                                                     htmlWorld += '</div>';
    //                                                     htmlWorld += '<div class="image-product col-md-12" >';
    //                                                         htmlWorld += '<div class="image-product" style="width: 100%; height: 100%;">';
    //                                                             htmlWorld += '<img src="https://picsum.photos/200/200?image='+getRandomInt(0,1000)+'" alt="product" style="width:85%; height:100%;">';
    //                                                         htmlWorld += '</div>';
    //                                                     htmlWorld += '</div>';
    //                                                 htmlWorld += '</div>';
    //                                             htmlWorld += '</div>';
    //                                         htmlWorld += '</li>';
    //                                     });
    //                                 }else{
    //                                     htmlWorld += '<div class="col-md-12" style="padding-left:0px; padding-right:35px;">';
    //                                         htmlWorld += '<div class="callout callout-danger">';
    //                                             htmlWorld += '<h4>No se encontraron productos desactivados</h4>';
    //                                             htmlWorld += '<p></p>';
    //                                         htmlWorld += '</div>';
    //                                     htmlWorld += '</div>';
    //                                 }

                                                               
    //                                 htmlWorld += '</ul>';
    //                             htmlWorld += '</div>';
    //                         htmlWorld += '</div>';
    //                     htmlWorld += '</div>';
    //                     $('#content-page-magazine').html(htmlWorld);
    //                     // $( ".sortable" ).sortable();
    //                     // $( ".sortable" ).disableSelection();
    //                 }else{
    //                     toastr.error('Error al generar pagina','',{timeOut: 1000});
    //                 }
    //                 toastr.success('producto eliminado '+idAgShowDisable,'',{timeOut: 1000});
    //             });
    //         }else{
    //             toastr.error('Intente nuevamente','',{timeOut: 1000});
    //         }
    //     });

    //     //EFECTOS DE ELEMENTOS DOPOVER Y DROPOUT (SOLO PARA HACER CAMBIO DE ESTILO)
    //     $(document).on('dropover','.droppable', function( event, ui ) {
    //         $( this ).addClass( "highlight" );
    //     });
    //     $(document).on('dropout', '.droppable',function( event, ui ) {
    //         $( this ).removeClass( "highlight" )
    //     });

    //     //ACCION AL SOLTAR UN ELEMENTO DENTRO DE UN CUADRO EN EL CREADOR DE PAGINAS
    //     $(document).on('drop','.droppable', function( event, ui ) {
    //         console.log('hola');
    //         let idContent = ui.draggable.attr("data-id"); // ID CONTENT
    //         let textContent = ui.draggable[0].textContent;  // TEXTO DE LA TARJETA QUE VA A CAER EN EL ESPACIO
            
    //         //AJAX PARA GRABAR LA POSICION Y PAGINA DE LA TARJETA 

    //         //CREACION DE TARJETA UNA VEZ PUESTA
    //         let htmlPutCard = '';
    //         htmlPutCard += '<div class="row">';
    //             htmlPutCard += '<div class="info-options col-md-12" >';
    //                 htmlPutCard += '<div class="pull-right">';
    //                     htmlPutCard += '<a href="#" data-id="'+idContent+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>';
    //                     htmlPutCard += '<a href="#" data-id="'+idContent+'" class="plus-put-card-element"><i class="glyphicon glyphicon-ok" style="color: #bfbc00; padding-right: 5px;"></i></a>';
    //                     htmlPutCard += '<a href="#" data-id="'+idContent+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>';
    //                     htmlPutCard += '<a href="#" data-id="'+idContent+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>';
    //                 htmlPutCard += '</div>';
    //             htmlPutCard += '</div>';
    //             htmlPutCard += '<div class="info-options col-md-12" >';
    //                 htmlPutCard += textContent;
    //             htmlPutCard += '</div>';    
    //         htmlPutCard += '</div>';
            
    //         //SI LA CAJA YA POSEE LA CLASE DE SELECCIONADO PREGUNTARA POR UNIR OBJETOS, SI NO LO UTILIZARA
    //         if($(this).hasClass('ui-state-highlight')!=true){
    //             $( this ).addClass( "ui-state-highlight" ).find( "p" ).html(htmlPutCard);
    //             ui.draggable.remove();
    //             $( this ).removeClass( "highlight" )
    //         }else{
    //             $( this ).removeClass( "highlight" )
    //             var txt;
    //             var r = confirm("Quiere unir los objetos?");
    //             if (r == true) {
    //                 txt = "UNIENDO";
    //             } else {
    //                 txt = "NADA";
    //             }
    //             console.log(txt);
    //         }

    //     });


    //     //ACCION AL SELECCIONAR LAS DIMENSIONES DE LA GRILLA
    //     $('.change-colum-page').on('click',function(){
    //         let xGrid = $(this).attr("data-x");
    //         let yGrid = $(this).attr("data-y");
    //         let grid = xGrid*yGrid;
    //         let xParam;
            
    //         //CALCULA EL MAXIMO DE LAS TARJETAS
    //         switch(parseInt(xGrid)){
    //             case 3:
    //                 xParam = 99/xGrid;
    //                 break;
    //             case 4:
    //                 xParam = 99/xGrid;
    //                 break;
    //             case 5:
    //                 xParam = 99/xGrid;
    //                 break;
    //         }

    //         //AJAX PARA OBTENER LAS TARJETAS QUE YA SE ENCUENTREN EN ESTA PAGINA
            
            
    //         //CREACUIN DE GRILLA DEPENDIENDO DE LA CANTIDAD DE TARJETAS
    //         var htmlPageStyle = '';
    //         for (let i = 1; i <= grid; i++) { 
    //             htmlPageStyle += '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 200px;">';
    //                 htmlPageStyle += '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 200px;">';
    //                     htmlPageStyle += '<p>Bloque'+i+'</p>';
    //                 htmlPageStyle += '</div>';
    //             htmlPageStyle += '</div>';
    //         }
    //         $('#page-maker').html(htmlPageStyle);

    //         //AGREGA CARACTERISTICA DROPPABLE A LOS ELEMENTOS CREADOS EN EL PUNTO ANTERIOR
    //         $( ".droppable" ).droppable({
    //             accept: "#list-product"
    //         });
    //         //alert('Cambiando tamañado de pagina');
    //     });
    // });