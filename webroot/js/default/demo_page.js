var base_url = window.location.origin;
var pathArray = window.location.pathname.split( '/' );
var urlBase = base_url+'/'+pathArray[1]+'/';

$( document ).ready(function() {

    //ESTILO DE CHECKBOX
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red'
    });
    
    //FUNCION QUE CAPTURA AL SELECCION UN CHECKBOX DE MUNDO O AG
    $('.minimal-red-catalog').on('ifClicked', function(event){
    // $('.icheckbox_minimal-red').on('ifChanged', function(event){
        let selectedCheckbox = this;

        console.log(this);

        elementId = $(this).attr('data-id');
        elementType = $(this).attr('data-type');

        console.log('elementType: '+elementType);
        if(elementId && elementType){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: "POST",
                url: urlBase+'magazines/checkStatus',
                dataType: 'json',
                data: {
                    'elementId': elementId,
                    'elementType': elementType
                }
            })
            .done(function(response){
                console.log("..done");
                
                if (typeof response.isChecked !== 'undefined') {
                    if (response.isChecked) {
                        console.log("..checked true");
                        /*$(selectedCheckbox).addClass('checked');
                        $(selectedCheckbox).prop('checked', true);*/
                        $(selectedCheckbox).iCheck('check');
                        toastr.success('Elemento seleccionado','',{timeOut: 1000});
                    } else {
                        console.log("..checked false");
                        /*$(selectedCheckbox).removeClass('checked');
                        $(selectedCheckbox).prop('checked', false);*/
                        $(selectedCheckbox).iCheck('uncheck');
                        toastr.success('Elemento deseleccionado','',{timeOut: 1000});
                    }
                }
                //console.log($(this).parent(".icheckbox_minimal-red"));
            });
        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //FUNCION QUE CAPTURA LA SELECCION DE COVER DEL MUNDO.
    $('.check-cover').click(function( event ) {

        event.stopPropagation();
        elementId = $(this).attr('data-id');
        console.log("aca");

        if(elementId != null){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: "POST",
                url: urlBase+'magazines/changeWithCover',
                dataType: 'json',
                data: {
                    'elementId': elementId
                }
            })
            .done(function(response){

                console.log(response);
                console.log($(this));
                if(response.status){
                    toastr.success('Portada Modificada','',{timeOut: 1000});
                    if(response.check == 0 ){
                        $('#check-cover-'+elementId).css("background-color", "gray");
                    }else{
                        $('#check-cover-'+elementId).css("background-color", "green");
                    }
                }else{
                    toastr.error('Intente nuevamente','',{timeOut: 1000});
                }

            });
        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });


    function cargarImagenes(){
      var img1 = document.getElementsByClassName('product-img');
      img1.onerror = cargarImagenPorDefecto;
      // var img2 = document.getElementsByClassName('imagen2');
      // img2.onerror = cargarImagenPorDefecto;
    }

    function cargarImagenPorDefecto(e){
      e.target.src= 'https://www.blackwallst.directory/images/NoImageAvailable.png';
    }

    //AGREGA IMAGEN AL SELECT2 DE PIE DE PAGINA
    function formatState (state) {
      if (!state.id) {
        return state.text;
      }
      // var baseUrl = "/user/pages/images/flags";
      var $state = $(
        '<span><img class="" src="'+urlBase+'img/icon_4x/'+state.title+'" alt="" style="height:60px; width:60px">'+state.text+'</span>'
      );
      return $state;
    };

    //FUNCION QUE REINICIA LA BARRA DE CARGA
    $(document).ajaxStart(function () {
        Pace.restart();
    });

    $(document).on('error','img',function() {
        console.log('acaaaaaaaaaaaaaaaaaaaaa');
        $(this).attr('src', 'http://localhost/Back-CCI/img/default/dftl-image.png');
    });

    //EFECTOS DE ELEMENTOS DROPPOVER Y DROPOUT (SOLO PARA HACER CAMBIO DE ESTILO)
    $(document).on('dropover','.droppable', function( event, ui ) {
        $( this ).addClass( "highlight" );
    });
    $(document).on('dropout', '.droppable',function( event, ui ) {
        $( this ).removeClass( "highlight" )
    });
    //////////////////////////////////////////////////

    //OBTIENE UN VALOR RANDOM ENTERO ENTRE MINIMO Y MAXIMO
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    //LIMPIA DE CLASE
    function cleanClass(){
        $('li.sortable-index-element').each(function( index ){
            $(this).removeClass("click-ag-mark");
            $(this).find('.index-ag-name').css( "color", "black" );
        });
    }

    //SIRVE USAR MARCAR O DESMARCAR TODOS
    function markAllCheck(type){
        Pace.start();
        console.log('Cambiando estados...');
        let elementId = idMagazine;
        let elementType;
        console.log('------------');
        console.log(type);
        console.log(elementId);
        console.log(elementType);
        console.log('------------');
        switch(type) {
            case 1:
                console.log('case 1');
                $('div.sortable-world-element-catalog').each(function( index,val ){

                    console.log('index: ' + index);
                    console.log(val);
                    console.log('__________________');
                    console.log($(this).find('.icheckbox_minimal-red'));
                    /*$(this).find('.icheckbox_minimal-red').addClass('checked');
                    $(this).find('.icheckbox_minimal-red').prop('checked', true);*/
                    $(this).find('.icheckbox_minimal-red').iCheck('check');
                });
                elementType = 2;
                break;
            case 2:
                console.log('case 2');
                $('div.sortable-world-element-catalog').each(function( index, val ){
                    console.log('index: ' + index);
                    console.log(val);
                    /*$(this).find('.icheckbox_minimal-red').removeClass('checked');
                    $(this).find('.icheckbox_minimal-red').prop('checked', false);*/    
                    $(this).find('.icheckbox_minimal-red').iCheck('uncheck');
                });
                elementType = 3;
                break;
        }
        
        if(elementId){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: "POST",
                url: urlBase+'magazines/checkStatus',
                dataType: 'json',
                data: {
                    'elementId': elementId,
                    'elementType': elementType
                }
            })
            .done(function(response){
                
                toastr.success('Estados actualizados','',{timeOut: 1000});
                Pace.stop();
            });
        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
            Pace.stop();
        }
    }

    //SELECCIONAR TODO
    $('.select-all').on('click',function(){
        markAllCheck(1);
    });

    //DESELECICONAR TODO
    $('.disable-all').on('click',function(){
        markAllCheck(2);
    });

    //FUNCION PARA ORDERNAR LOS MUNDOS EN EL INDICE
    $( ".sortable-world-index" ).on( "sortstop", function( event, ui ) {
        console.log('Ordenando mundos...');
        let idWorld = [];
        if ($(ui.item).hasClass('sortable-world-element')) {
            $('div.sortable-world-element',this).each(function( index ){
                idWorld.push($(this).attr("data-id"));
            });
            if(idWorld){
                $.ajax({
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    type: "POST",
                    url: urlBase+'magazines/orderWorlds',
                    dataType: 'json',
                    data: {
                        'idWorld': idWorld
                    }
                })
                .done(function(response){
                    toastr.success('Orden de mundo guardado','',{timeOut: 1000});
                });
            }else{
                toastr.error('Intente nuevamente','',{timeOut: 1000});
            }
        }
    });

    //FUNCION PARA ORDERNAR LOS GRUPOS DE ANALISIS
    $('.sortable-index').on('sortstop', function( event, ui ) {
        console.log('Ordenando grupos de analisis...');
        let idAnalysisGroup = [];
        $('li.sortable-index-element',this).each(function( index ){
            idAnalysisGroup.push($(this).attr("data-id"));
        });
        if(idAnalysisGroup){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/orderAnalysisGroups',
                dataType: 'json',
                data: {
                    'idAnalysisGroup': idAnalysisGroup
                }
            })
            .done(function(response){
                toastr.success('Orden de grupo de análisis guardado','',{timeOut: 1000});
            });
        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //FUNCION PARA OBTENER LA INFORMACION DE LOS MUNDOS SELECCIONADOS
    $('#accordion-index').on('accordionbeforeactivate', function( event, ui ) {
        cleanClass();
        console.log('Obteniendo informacion del mundo...');
        var idWorldClick = ui.newHeader.parent().attr('data-id');
        if(idWorldClick!=undefined){
             $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/getWorldInfo',
                dataType: 'json',
                data: {
                    'idWorldClick': idWorldClick
                    },
                cache:false
            })
            .done(function(response){
                if(response.status==true){
                    let htmlWorld = 
                    '<div class="box-body">'+
                        '<div class="row">'+
                            '<div class="col-md-12" id="">'+
                                '<h2>'+response.data_response.magazine.name_feria.toUpperCase()+' / FERIA '+response.data_response.magazine.ferias_id+'</h2>'+
                            '<hr id="hr-world-magazine">'+
                            '</div>'+    
                            '<div class="col-md-12" id="">'+
                                '<h3 class="pull-right">'+response.data_response.magazine.feria_desde+'</h3>'+
                                '</br>'+
                            '</div>'+    
                            '<div class="col-md-12" id="">'+
                                '<div class="small-box bg-red pull-right" id="world-label-magazine">'+
                                    '<h3 class="pull-right" style="font-size: 150%; width: 100%; text-align: right;">'+response.data_response.name.toUpperCase()+'</h3>'+
                                '</div>'+    
                            '</div>'+
                        '</div>'+
                    '</div>';
                    $('#content-page-magazine').html(htmlWorld);
                }else{
                    htmlWorld = '';
                    $('#content-page-magazine').html(htmlWorld);
                }
            });
        }
    });

    //FUNCION AL HACER CLICK EN X DE PRODUCTO
    $(document).on('click','.delete-card-element',function() {
        let cardElement = $(this).closest('li#list-product');
        let idElementDisable = $(this).attr("data-id");
        if(idElementDisable){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/changeStateContent',
                dataType: 'json',
                data: {
                    'idElementDisable': idElementDisable,
                    'state':0
                }
            })
            .done(function(response){
                cardElement.remove();
                toastr.success('producto eliminado','',{timeOut: 1000});
            });
        }else{
           toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //FUNCION AL HACER CLICK EN ICONO DE ACTIVA DE PRODUCTO
    $(document).on('click','.active-card-element',function() {
        let cardElement = $(this).closest('li#list-product');
        let idElementDisable = $(this).attr("data-id");
        if(idElementDisable){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/changeStateContent',
                dataType: 'json',
                data: {
                    'idElementDisable': idElementDisable,
                    'state':1
                }
            })
            .done(function(response){
                cardElement.remove();
                toastr.success('Producto eliminado','',{timeOut: 1000});
            });
        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //ACCION AL HACER CICLO EN TARJETA PUESTA EN LA PAGINA (-)
    $(document).on('click','.minus-put-card-element',function() {
        console.log('AGREGANDO SIMBOLO (-)...');
        let idContent = $(this).attr("data-id");
        let nameProduct = $(this).closest('#card-product').find('#name-product');
        let type = 2; //MARCA COMO NUEVO
        if(idContent){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/markContent',
                dataType: 'json',
                data: {
                    'idContent': idContent,
                    'type':type
                }
            })
            .done(function(response){
                if(response.data_response.pp_symbol==2){
                    cardText = 
                    '<div id="minus-icon" style="float:right;">'+
                            '<h2>(-)</h2>'+
                    '</div>';
                }else{
                    cardText = '';
                }
                nameProduct.next().remove();
                nameProduct.after(cardText);
                toastr.success('Producto marcado con (-)','',{timeOut: 1000});
            });
        }else{
           toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //ACCION AL HACER CICLO EN TARJETA PUESTA EN LA PAGINA (+)
    $(document).on('click','.plus-put-card-element',function() {
        console.log('AGREGANDO SIMBOLO (+)...');
        let idContent = $(this).attr("data-id");
        let nameProduct = $(this).closest('#card-product').find('#name-product');
        let type = 1; //MARCA COMO NUEVO
        if(idContent){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/markContent',
                dataType: 'json',
                data: {
                    'idContent': idContent,
                    'type':type
                }
            })
            .done(function(response){
                if(response.data_response.pp_symbol==1){
                    cardText = 
                    '<div id="plus-icon" style="float:right;">'+
                            '<h2>(+)</h2>'+
                    '</div>';
                }else{
                    cardText = '';
                }
                nameProduct.next().remove();
                nameProduct.after(cardText);
                toastr.success('Producto marcado con (+)','',{timeOut: 1000});
            });
        }else{
           toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //ACCION AL HACER CICLO EN TARJETA PUESTA EN LA PAGINA COMO PRODUCTO NUEVO
    $(document).on('click','.mark-put-card-element',function() {
        console.log('MARCANDO TARJETA COMO NUEVO PRODUCTO...');
        let idContent = $(this).attr("data-id");
        let sku = $(this).closest('#card-product').find('#sku-product');
        let type = 0; //MARCA COMO NUEVO
        if(idContent){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/markContent',
                dataType: 'json',
                data: {
                    'idContent': idContent,
                    'type': type
                }
            })
            .done(function(response){
                if(response.status==true){
                    if(response.data_response.is_new==1){
                        sku.css('background-color','#FFED00');
                        sku.css('color','black');
                    }else{
                        sku.css('background-color','');
                        sku.css('color','');
                    };
                    toastr.success('Producto marcado como nuevo','',{timeOut: 1000});
                }else{
                    toastr.error('Intente nuevamente','',{timeOut: 1000});
                }
            });
        }else{
           toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //CONJUNTO DE ACCIONES PARA EL CAMBIO DE ALIAS SECCION SUPERIOR DERECHA DE LA PAGINA
        $(document).on('mouseenter','.world-alias',function () {
            $(this).css('background-color','yellow');
            $(this).css('border-radius', '15px');
        });
        $(document).on('mouseleave','.world-alias',function () {
            $(this).css('border','');
            $(this).css('background-color','');
        });
        $(document).on('dblclick','.world-alias', function() {
            let textAlias = $(this).html();
            let dataId = $(this).attr('data-id');
            let html = 
                '<div class="form-change-alias">'+
                    '<input type="text" class="change-alias" value="'+textAlias+'" data-id="'+dataId+'" required maxlength="30">'+
                    '<a class="btn btn-primary change-alias-submit" >'+
                        '<i class="glyphicon glyphicon-pencil"></i>'+
                    '</a>'+
                '</div>';
            $(this).replaceWith(html);
        });
        $(document).on('click','.change-alias-submit', function() {
            formAlias = $(this).parent();
            textInput = $(this).prev().val();
            idWorld = $(this).prev().attr('data-id');
            if(textInput){
                $.ajax({
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    type: 'POST',
                    url: urlBase+'magazines/changeAlias',
                    dataType: 'json',
                    data: {
                        'textInput': textInput,
                        'idWorld': idWorld
                    }
                })
                .done(function(response){
                    if(response.status==true){
                        let html =
                            '<span class="world-alias" style="color:#dd4b39;" data-id="'+idWorld+'">'+textInput+'</span>';
                        formAlias.replaceWith(html);
                        toastr.success('Alias cambiado correctamente','',{timeOut: 1000});
                    }else{
                        toastr.error('Intente nuevamente','',{timeOut: 1000});
                    }
                });
            }else{
                let html =
                    '<span class="world-alias" style="color:#dd4b39;" data-id="'+idWorld+'">ALIAS PROVISORIO</span>';
                formAlias.replaceWith(html);
                toastr.error('El campo no puede ir vacio','',{timeOut: 1000});
            }
        });
    //////////////////////////////////////////////////

    //CONJUNTO DE ACCIONES PARA EL CAMBIO DE MEDIDAS
        $(document).on('mouseenter','.measurements-product',function () {
            $(this).css('background-color','yellow');
            $(this).css('border-radius', '15px');
        });
        $(document).on('mouseleave','.measurements-product',function () {
            $(this).css('border', '');
            $(this).css('background-color','');
        });
        $(document).on('dblclick','.measurements-product', function() {
            let textAlias = $(this).html();
            let dataId = $(this).attr('data-id');
            let html =
                '<div class="form-change-alias">'+
                    '<textarea type="text" class="change-measurements" data-id="'+dataId+'" maxlength="255">'+textAlias+'</textarea>'+
                    '<a class="btn btn-primary change-measurements-submit" >'+
                        '<i class="glyphicon glyphicon-pencil"></i>'+
                    '</a>'+
                '</div>';
            $(this).replaceWith(html);
        });
        $(document).on('click','.change-measurements-submit', function() {
            formAlias = $(this).parent();
            textInput = $(this).prev().val();
            idContent = $(this).prev().attr('data-id');
            if(textInput){
                $.ajax({
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    type: 'POST',
                    url: urlBase+'magazines/changeMeasurements',
                    dataType: 'json',
                    data: {
                        'textInput': textInput,
                        'idContent': idContent
                    }
                })
                .done(function(response){
                    if(response.status==true){
                        let html =
                            '<pre class="measurements-product" data-id="'+idContent+'">'+textInput+'</pre>';
                        formAlias.replaceWith(html);
                        toastr.success('Texto agregado correctamente','',{timeOut: 2000});
                    }else{
                        toastr.error('Intente nuevamente','',{timeOut: 2000});
                    }
                });
            }else{
                $.ajax({
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    type: 'POST',
                    url: urlBase+'magazines/changeMeasurements',
                    dataType: 'json',
                    data: {
                        'textInput': null,
                        'idContent': idContent
                    }
                })
                .done(function(response){
                    if(response.status==true){
                        let html =
                            '<pre class="measurements-product" data-id="'+idContent+'">Texto Opcional</pre>';
                        formAlias.replaceWith(html);
                        toastr.success('Texto agregado correctamente','',{timeOut: 2000});
                    }else{
                        toastr.error('Intente nuevamente','',{timeOut: 2000});
                    }
                });
            }
        });
    //////////////////////////////////////////////////

    //CONJUNTO DE ACCIONES PARA EL CAMBIO DE ESTRATEGIA
        $(document).on('mouseenter','.strategyGroup',function () {
            $(this).css('background-color','yellow');
            $(this).css('border-radius', '15px');
        });
        $(document).on('mouseleave','.strategyGroup',function () {
            $(this).css('border', '');
            $(this).css('background-color','');
        });
        $(document).on('dblclick','.strategyGroup', function() {
            let textAlias = $(this).html();
            let dataId = $(this).attr('data-page');
            let dataPos = $(this).attr('data-pos');
            let html = 
                '<div class="form-change-strategyGroup">'+
                    '<input type="text" class="change-strategyGroup" value="'+textAlias+'" data-pos="'+dataPos+'" data-page="'+dataId+'" required maxlength="30">'+
                    '<a class="btn btn-primary change-strategyGroup-submit" >'+
                        '<i class="glyphicon glyphicon-pencil"></i>'+
                    '</a>'+
                '</div>';
            $(this).replaceWith(html);
        });
        $(document).on('click','.change-strategyGroup-submit', function() {
            let formAlias = $(this).parent();
            let textInput = $(this).prev().val();
            let idPage = $(this).prev().attr('data-page');
            let position = $(this).prev().attr('data-pos');
            if(textInput){
                $.ajax({
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    type: 'POST',
                    url: urlBase+'magazines/strategyGroup',
                    dataType: 'json',
                    data: {
                        'textInput': textInput,
                        'idPage': idPage,
                        'position': position
                    }
                })
                .done(function(response){
                    if(response.status==true){
                        let html =
                            '<span class="strategyGroup" data-pos="'+position+'" data-page="'+idPage+'" style="font-size:25px;">'+textInput+'</span>';
                            // '<span class="world-alias" style="color:#dd4b39;" data-id="'+idPage+'">'+textInput+'</span>';
                        formAlias.replaceWith(html);
                        toastr.success('Alias cambiado correctamente','',{timeOut: 1000});
                    }else{
                        toastr.error('Intente nuevamente','',{timeOut: 1000});
                    }
                });
            }else{
               toastr.error('Intente nuevamente','',{timeOut: 1000});
            }
        });
    //////////////////////////////////////////////////

    //CONJUNTO DE ACCIONES PARA EL CAMBIO DE ESTRATEGIA
        $(document).on('mouseenter','.subTitleGroup',function () {
            $(this).css('background-color','yellow');
            $(this).css('border-radius', '15px');
        });
        $(document).on('mouseleave','.subTitleGroup',function () {
            $(this).css('border', '');
            $(this).css('background-color','');
        });
        $(document).on('dblclick','.subTitleGroup', function() {
            let textAlias = $(this).html();
            let dataId = $(this).attr('data-page');
            let dataPos = $(this).attr('data-pos');
            let html = 
                '<div class="form-change-subTitleGroup">'+
                    '<input type="text" class="change-subTitleGroup" value="'+textAlias+'" data-pos="'+dataPos+'" data-page="'+dataId+'" required maxlength="30">'+
                    '<a class="btn btn-primary change-subTitleGroup-submit" >'+
                        '<i class="glyphicon glyphicon-pencil"></i>'+
                    '</a>'+
                '</div>';
            $(this).replaceWith(html);
        });
        $(document).on('click','.change-subTitleGroup-submit', function() {
            let formAlias = $(this).parent();
            let textInput = $(this).prev().val();
            let idPage = $(this).prev().attr('data-page');
            let position = $(this).prev().attr('data-pos');
            if(textInput){
                $.ajax({
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    type: 'POST',
                    url: urlBase+'magazines/subTitleGroup',
                    dataType: 'json',
                    data: {
                        'textInput': textInput,
                        'idPage': idPage,
                        'position': position
                    }
                })
                .done(function(response){
                    if(response.status==true){
                        let html =
                            '<span class="subTitleGroup" data-pos="'+position+'" data-page="'+idPage+'" style="font-size:25px;">'+textInput+'</span>';
                            // '<span class="world-alias" style="color:#dd4b39;" data-id="'+idPage+'">'+textInput+'</span>';
                        formAlias.replaceWith(html);
                        toastr.success('Alias cambiado correctamente','',{timeOut: 1000});
                    }else{
                        toastr.error('Intente nuevamente','',{timeOut: 1000});
                    }
                });
            }else{
               toastr.error('Intente nuevamente','',{timeOut: 1000});
            }
        });
    //////////////////////////////////////////////////
    
    //ELIMINA UN ELEMENTO DE LA PAGINA
    $(document).on('click','.delete-put-card-element',function() {
        console.log('Quitando elemento de la pagina...');
        let idContent = $(this).attr("data-id");    //ID DEL CONTENIDO
        let cardElement = $(this).closest('div.row');   //TARJETA DE INFORMACION EN LA PAGINA
        let cardDroppable = $(this).closest('div.droppable'); //ESPACIO DROPPABLE
        let cardText = 
        '<div class="row">'+
            '<div class="col-md-12">'+
                '<p hidden data-id="">DISPONIBLE</p>'+
            '</div>'+
        '</div>';
        if(idContent){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/cleanPosition',
                dataType: 'json',
                data: {
                    'idContent': idContent
                }
            })
            .done(function(response){
                //CONDICIONES DE ICONOS Y DISEÑO
                if(response.data_response.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                switch(response.data_response.pp_symbol){
                    case 1:
                        iconPlus =
                        '<div id="plus-icon" style="float:right;">'+
                            '<h2>(+)</h2>'+
                        '</div>';
                    break;
                    case 2:
                        iconPlus =
                        '<div id="minus-icon" style="float:right;">'+
                            '<h2>(-)</h2>'+
                        '</div>';
                    break;
                    default:
                        iconPlus = '';
                    break;
                }
                let cardReturn = 
                '<li id="list-product" class="ui-state-default" data-id="'+response.data_response.id+'" data-order="">'+
                    '<div id="card-style">'+
                        '<div id="card-product" class="row">';
                            if(response.data_response.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                            cardReturn+=
                            '<div class="col-md-12">'+
                                '<div class="info-options" >'+
                                   '<div class="pull-left icon-section">'+
                                        '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                            '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                        '</a>'+
                                        '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                            '<div style="width: 60px;">'+
                                                '<span class="icon-product center-block" data-id="'+response.data_response.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                            '</div>'+
                                            '<div class="divider"></div>'+
                                            '<div style="width: 60px;">'+
                                                '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                            '</div>'+
                                            '<div class="divider"></div>'+
                                            '<div style="width: 60px;">'+
                                                '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                            '</div>'+
                                            '<div class="divider"></div>'+
                                            '<div style="width: 60px;">'+
                                                '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="pull-right">'+
                                        '<a href="#" data-id="'+response.data_response.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                        '<a href="#" data-id="'+response.data_response.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                        '<a href="#" data-id="'+response.data_response.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                        // '<a href="#" data-id="'+index.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12">'+
                                '<div class="info-product">'+
                                    '<dl>';
                                        if(response.data_response.measurements==null){extraText = 'Texto Opcional'}else{extraText=response.data_response.measurements}
                                        cardReturn+=
                                        '<dt><div id="name-product" style="word-break: break-word;">'+response.data_response.name_desc+'</div>'+
                                        iconPlus+'</dt>'+
                                        '<dt>Diseño: <span class="design-product" data-id="'+response.data_response.id+'">'+response.data_response.diseno+'</span></dt>'+
                                        '<dt>Color: <span class="color-product" data-id="'+response.data_response.id+'">'+response.data_response.color+'</span></dt>'+
                                        '<dt><pre class="measurements-product" data-id="'+response.data_response.id+'">'+extraText+'</pre></dt>'+
                                        '<dt>'+
                                            '<table style="width: 100%">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th><span>SKU</span></th>'+
                                                        '<th><span>PP</span></th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr>'+
                                                        '<td><div id="sku-product" style='+styleNew+'>'+response.data_response.sku_mod+'</div></td>'+
                                                        '<td><span>'+Math.abs(response.data_response.punta_precio)+'</span></td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</dt>'+
                                    '</dl>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12">'+
                                '<div class="image-product">'+
                                    '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+response.data_response.sku.substring(5)+'.jpg" alt="product">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</li>';
                cardElement.remove();
                $('#contents-products').append(cardReturn)
                cardDroppable.removeClass( "ui-state-highlight" );
                cardDroppable.html(cardText);
                cargarImagenes();
                toastr.success('Elemento retornado','',{timeOut: 1000});
            });
        }else{
           toastr.error('Intente nuevamente','',{timeOut: 2000});
        }
    });

    //ACCION PARA LA NAVEGACION DE PAGINAS (ADELANTE)
    $(document).on('click','.go-next-page',function() {
        console.log('Cargando pagina siguiente...');
        let idAg = $(this).attr("data-id");
        let nPage = $(this).closest('div.box-header').find('#page-number').attr("data-page");
        let type = 1;
        let idPageFooter = $(this).closest('#finish-page-element').find('div.box-footer').find('.footer-icon');
        if(idAg){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/movePage',
                dataType: 'json',
                data: {
                    'idAg': idAg,
                    'nPage': nPage,
                    'type': type
                }
            })
            .done(function(response){
                if(response.status==true){
                    let grid = response.data_response[0].x_value * response.data_response[0].y_value;
                    console.log("Tamaño pagina sgte");
                    console.log(grid);
                    let htmlPageStyle = '';
                    let xParam = 99/response.data_response[0].x_value;
                    if(response.data_response[0].contents!= ''){
                        let strategyPlus = 1;
                        let varControl = 0;
                        let j = 1;
                        let test = 0;
                        for (let i = 1; i <= grid; i++) {
                            //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                            let variableTest = 0;
                            if(i==strategyPlus+(response.data_response[0].x_value*test)){
                                htmlPageStyle +=
                                '<div class="row asdasd">';
                                if(response.data_response[0].content_groups!=''){
                                    response.data_response[0].content_groups.forEach(function(index){
                                        if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                        if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                        if(index.cg_order==j){
                                            htmlPageStyle += 
                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                            test++;
                                            varControl++;
                                        }
                                    });
                                    if(varControl==0){
                                        htmlPageStyle += 
                                        '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                        '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                        test++;
                                    }
                                    varControl = 0;
                                    j++;
                                }else{
                                    htmlPageStyle += 
                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                    test++;
                                }
                                htmlPageStyle+=
                                '</div>';
                            }

                            //FIN LOGICA GRUPO DE ANALISIS
                            response.data_response[0].contents.forEach(function(index){
                                if(index.c_order == i){
                                    if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                    switch(index.pp_symbol){
                                        case 1:
                                            iconPlus =
                                            '<div id="plus-icon" style="float:right;">'+
                                                '<h2>(+)</h2>'+
                                            '</div>';
                                        break;
                                        case 2:
                                            iconPlus =
                                            '<div id="minus-icon" style="float:right;">'+
                                                '<h2>(-)</h2>'+
                                            '</div>';
                                        break;
                                        default:
                                            iconPlus = '';
                                        break;
                                    }
                                    htmlPageStyle +=
                                    '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                        '<div id="slot-'+i+'-id-'+index.id+'" class="ui-widget-header droppable ui-state-highlight" data-pos="'+i+'" style="height: 300px;">'+
                                            '<p hidden data-id="'+index.id+'">'+
                                                '<div id="card-product" data-pos="'+i+'" data-id="'+index.id+'" class="row ui-widget-content draggable" style="z-index: 100">';
                                                    if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                                    htmlPageStyle+=
                                                    '<div class="col-md-12">'+
                                                        '<div class="info-options">'+
                                                            '<div class="pull-left icon-section">'+
                                                                '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                                    '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                                '</a>'+
                                                                '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                                    '</div>'+
                                                                    '<div class="divider"></div>'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                    '</div>'+
                                                                    '<div class="divider"></div>'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                    '</div>'+
                                                                    '<div class="divider"></div>'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</div>'+
                                                            '<div class="pull-right">'+
                                                                '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                '<a href="#" data-id="'+index.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '<div class="col-md-12">'+
                                                        '<div class="info-product">'+
                                                            '<dl>';
                                                                if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                htmlPageStyle +=
                                                                '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                                iconPlus+
                                                                '</dt>'+
                                                                '<dt>Diseño: <span class="design-product" data-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                                '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                                '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                '<dt>'+
                                                                    '<table style="width: 100%;">'+
                                                                        '<thead>'+
                                                                            '<tr>'+
                                                                                '<th><span>SKU</span></th>'+
                                                                                '<th><span>PP</span></th>'+
                                                                            '</tr>'+
                                                                        '</thead>'+
                                                                        '<tbody>'+
                                                                            '<tr>'+
                                                                                '<td><div id="sku-product" style='+styleNew+'>'+index.sku_mod+'</div></td>'+
                                                                                '<td><span>'+Math.abs(index.punta_precio)+'</span></td>'+
                                                                            '</tr>'+
                                                                        '</tbody>'+
                                                                    '</table>'+
                                                                '</dt>';
                                                                if(index.childrens!=''){
                                                                    index.childrens.forEach(function(index){
                                                                        if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                                                        if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                        htmlPageStyle +=
                                                                        '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                        '<dt>'+
                                                                            '<table style="width: 100%;">'+
                                                                                '<thead>'+
                                                                                    '<tr>'+
                                                                                        '<th><span>SKU</span></th>'+
                                                                                        '<th><span>PP</span></th>'+
                                                                                    '</tr>'+
                                                                                '</thead>'+
                                                                                '<tbody>'+
                                                                                    '<tr>'+
                                                                                        '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+'</div></td>'+
                                                                                        '<td><span>'+Math.abs(index.punta_precio)+'</span></td>'+
                                                                                    '</tr>'+
                                                                                '</tbody>'+
                                                                            '</table>'+
                                                                        '</dt>';
                                                                    });
                                                                }
                                                                htmlPageStyle +=
                                                            '</dl>'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '<div class="col-md-12">'+
                                                        '<div class="image-product">'+
                                                            '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</p>'+
                                        '</div>'+
                                    '</div>';
                                    variableTest++;
                                }
                            });
                            if(variableTest==0){
                                htmlPageStyle +=
                                '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                    '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                        '<p hidden data-id="">'+
                                            'DISPONIBLE'+
                                        '</p>'+
                                    '</div>'+
                                '</div>';
                                variableTest=0;
                            }
                        }
                    }else{
                        let strategyPlus = 1;
                        let varControl = 0;
                        let j = 1;
                        let test = 0;
                        for (let i = 1; i <= grid; i++) {
                            //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                            let variableTest = 0;
                            if(i==strategyPlus+(response.data_response[0].x_value*test)){
                                htmlPageStyle +=
                                '<div class="row asdasd">';
                                if(response.data_response[0].content_groups!=''){
                                    response.data_response[0].content_groups.forEach(function(index){
                                        if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                        if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                        if(index.cg_order==j){
                                            htmlPageStyle += 
                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                            test++;
                                            varControl++;
                                        }
                                    });
                                    if(varControl==0){
                                        htmlPageStyle += 
                                        '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                        '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                        test++;
                                    }
                                    varControl = 0;
                                    j++;
                                }else{
                                    htmlPageStyle += 
                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                    test++;
                                }
                                htmlPageStyle +=
                                '</div>';
                            }
                            //FIN LOGICA GRUPO DE ANALISIS
                            htmlPageStyle +=
                            '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                    '<p hidden data-id="">'+
                                        'DISPONIBLE'+
                                    '</p>'+
                                '</div>'+
                            '</div>';
                        }
                    }
                    $('#page-maker').html(htmlPageStyle);
                    $('#page-maker').attr('data-areagrid', grid);
                    $('#page-number').html('Pagina '+response.data_response[0].page_order+' de '+response.count_page.count);
                    $('#page-number').attr('data-page',response.data_response[0].page_order)
                    $( ".droppable" ).droppable({
                        accept: "#list-product, #card-product"
                    });
                    idPageFooter.attr('data-page',response.data_response[0].id);
                    nPage++;
                    toastr.success('Pagina siguiente ('+nPage+')','',{timeOut: 1000});

                    $( ".draggable" ).draggable({ revert: "invalid" });

                }else{
                    toastr.error('Esta es la ultima pagina, cree una nueva','',{timeOut: 3000});
                }
            });
        }else{
           toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //ACCION PARA LA NAVEGACION DE PAGINAS (ATRAS)
    $(document).on('click','.go-prev-page',function() {
        console.log('Cargando pagina anterior...');
        let idAg = $(this).attr("data-id");
        let nPage = $(this).closest('div.box-header').find('#page-number').attr("data-page");
        let type = 0;
        let idPageFooter = $(this).closest('#finish-page-element').find('div.box-footer').find('.footer-icon');
        if(idAg){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/movePage',
                dataType: 'json',
                data: {
                    'idAg': idAg,
                    'nPage': nPage,
                    'type': type
                }
            })
            .done(function(response){
                if(response.status==true){
                    let htmlPageStyle = '';
                    let grid = response.data_response[0].x_value * response.data_response[0].y_value;
                    console.log("Tamaño pagina anterior");
                    console.log(grid);
                    let xParam = 99/response.data_response[0].x_value;
                    if(response.data_response[0].contents!= ''){
                        let strategyPlus = 1;
                        let varControl = 0;
                        let j = 1;
                        let test = 0;
                        for (let i = 1; i <= grid; i++) {
                            //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                            let variableTest = 0;
                            if(i==strategyPlus+(response.data_response[0].x_value*test)){
                                htmlPageStyle +=
                                '<div class="row">';
                                if(response.data_response[0].content_groups!=''){
                                    response.data_response[0].content_groups.forEach(function(index){
                                        if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                        if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                        if(index.cg_order==j){
                                            htmlPageStyle += 
                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                            test++;
                                            varControl++;
                                        }
                                    });
                                    if(varControl==0){
                                        htmlPageStyle += 
                                        '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                        '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                        test++;
                                    }
                                    varControl = 0;
                                    j++;
                                }else{
                                    htmlPageStyle += 
                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                    test++;
                                }
                                htmlPageStyle +=
                                '</div>';
                            }
                            //FIN LOGICA GRUPO DE ANALISIS   
                            response.data_response[0].contents.forEach(function(index){
                                if(index.c_order == i){
                                    if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                    switch(index.pp_symbol){
                                        case 1:
                                            iconPlus =
                                            '<div id="plus-icon" style="float:right;">'+
                                                '<h2>(+)</h2>'+
                                            '</div>';
                                        break;
                                        case 2:
                                            iconPlus =
                                            '<div id="minus-icon" style="float:right;">'+
                                                '<h2>(-)</h2>'+
                                            '</div>';
                                        break;
                                        default:
                                            iconPlus = '';
                                        break;
                                    }
                                    htmlPageStyle +=
                                    '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                        '<div id="slot-'+i+'-id-'+index.id+'" class="ui-widget-header droppable ui-state-highlight" data-pos="'+i+'" style="height: 300px;">'+
                                            '<p hidden data-id="'+index.id+'">'+
                                                '<div id="card-product" data-pos="'+i+'" data-id="'+index.id+'" class="row ui-widget-content draggable" style="z-index: 100">';
                                                    if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                                    htmlPageStyle+=
                                                    '<div class="col-md-12">'+
                                                        '<div class="info-options">'+
                                                           '<div class="pull-left icon-section">'+
                                                                '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                                    '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                                '</a>'+
                                                                '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                                    '</div>'+
                                                                    '<div class="divider"></div>'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                    '</div>'+
                                                                    '<div class="divider"></div>'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                    '</div>'+
                                                                    '<div class="divider"></div>'+
                                                                    '<div style="width: 60px;">'+
                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</div>'+
                                                            '<div class="pull-right">'+
                                                                '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                '<a href="#" data-id="'+index.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '<div class="col-md-12">'+
                                                        '<div class="info-product">'+
                                                            '<dl>';
                                                                if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                htmlPageStyle +=
                                                                '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                                iconPlus+
                                                                '</dt>'+
                                                                '<dt>Diseño: <span class="design-product" data-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                                '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                                '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                '<dt>'+
                                                                    '<table style="width: 100%;">'+
                                                                        '<thead>'+
                                                                            '<tr>'+
                                                                                '<th><span>SKU</span></th>'+
                                                                                '<th><span>PP</span></th>'+
                                                                            '</tr>'+
                                                                        '</thead>'+
                                                                        '<tbody>'+
                                                                            '<tr>'+
                                                                                '<td><div id="sku-product" style='+styleNew+'>'+index.sku_mod+'</div></td>'+
                                                                                '<td><span>'+Math.abs(index.punta_precio)+'</span></td>'+
                                                                            '</tr>'+
                                                                        '</tbody>'+
                                                                    '</table>'+
                                                                '</dt>';
                                                                if(index.childrens!=''){
                                                                    index.childrens.forEach(function(index){
                                                                        if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                                                        if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                        htmlPageStyle +=
                                                                        '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                        '<dt>'+
                                                                            '<table style="width: 100%;">'+
                                                                                '<thead>'+
                                                                                    '<tr>'+
                                                                                        '<th><span>SKU</span></th>'+
                                                                                        '<th><span>PP</span></th>'+
                                                                                    '</tr>'+
                                                                                '</thead>'+
                                                                                '<tbody>'+
                                                                                    '<tr>'+
                                                                                        '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+'</div></td>'+
                                                                                        '<td><span>'+Math.abs(index.punta_precio)+'</span></td>'+
                                                                                    '</tr>'+
                                                                                '</tbody>'+
                                                                            '</table>'+
                                                                        '</dt>';
                                                                    });
                                                                }
                                                                htmlPageStyle +=
                                                            '</dl>'+
                                                        '</div>'+    
                                                    '</div>'+
                                                    '<div class="col-md-12">'+
                                                        '<div class="image-product">'+
                                                            '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</p>'+
                                        '</div>'+
                                    '</div>';
                                    variableTest++;
                                }
                            });
                            if(variableTest==0){
                                htmlPageStyle +=
                                '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                    '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                        '<p hidden data-id="">'+
                                            'DISPONIBLE'+
                                        '</p>'+
                                    '</div>'+
                                '</div>';
                                variableTest=0;
                            }
                        }
                    }else{
                        let strategyPlus = 1;
                        let varControl = 0;
                        let j = 1;
                        let test = 0;
                        for (let i = 1; i <= grid; i++) {
                            //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                            let variableTest = 0;
                            if(i==strategyPlus+(response.data_response[0].x_value*test)){
                                htmlPageStyle +=
                                '<div class="row">';
                                if(response.data_response[0].content_groups!=''){
                                    response.data_response[0].content_groups.forEach(function(index){
                                        if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                        if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                        if(index.cg_order==j){
                                            htmlPageStyle += 
                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                            test++;
                                            varControl++;
                                        }
                                    });
                                    if(varControl==0){
                                        htmlPageStyle += 
                                        '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                        '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                        test++;
                                    }
                                    varControl = 0;
                                    j++;
                                }else{
                                    htmlPageStyle += 
                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.data_response[0].id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                    test++;
                                }
                                htmlPageStyle +=
                                '</div>';
                            }
                            //FIN LOGICA GRUPO DE ANALISIS 
                            htmlPageStyle +=
                            '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                    '<p hidden data-id="">'+
                                        'DISPONIBLE'+
                                    '</p>'+
                                '</div>'+
                            '</div>';
                        }
                    }
                    $('#page-maker').html(htmlPageStyle);
                    $('#page-maker').attr('data-areagrid', grid);
                    $('#page-number').html('Pagina '+response.data_response[0].page_order+' de '+response.count_page.count);
                    $('#page-number').attr('data-page',response.data_response[0].page_order)
                    $( ".droppable" ).droppable({
                        accept: "#list-product, #card-product"
                    });
                    $( ".draggable" ).draggable({ revert: "invalid" });
                    idPageFooter.attr('data-page',response.data_response[0].id);
                    nPage--;
                    toastr.success('Pagina anterior ('+nPage+')','',{timeOut: 1000});
                }else{
                    toastr.error('Esta es la primera pagina','',{timeOut: 2000});
                }
            });
        }else{
           toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //FUNCION PARA OBTENER LA INFORMACION DE UN GRUPO DE ANALISIS sasdasdasd
    $(document).on('click','.sortable-index-element',function() {
        console.log('Obteniendo información de grupo de análisis...');
        cleanClass();
        $(this).addClass("click-ag-mark");
        $(this).find('.index-ag-name').css( "color", "white" );
        let idAgClick = $(this).attr("data-id");
        if(idAgClick!=undefined){
            let createPage=1;
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/getAgInfo',
                dataType: 'json',
                data: {
                    'idAgClick': idAgClick,
                    'createPage': createPage
                },
                cache:false
            })
            .done(function(response){
                if(response.status==true){
                    let grid = response.page_info.x_value*response.page_info.y_value;
                    console.log("tamaño grilla al cargar");
                    console.log("Antes");
                    console.log(response);

                    // console.log("Despues");
                    // prueba = response.data_response.contents;
                    console.log(sortarr(response.data_response.contents, 'madre_id', 'asc'));

                    let xParam = 99/response.page_info.x_value;
                    let arrayTest = [];
                    let styleNew ='';
                    let htmlWorld = 
                    '<div class="box-header">'+
                        '<br>'+
                        '<div class="row">'+
                            '<div class="col-md-6 pull-left" id="header-page-magazine">'+
                                '<h3 class="box-title pull-left">'+response.data_response.name+'</h3>'+
                            '</div>'+
                            '<div class="col-md-6 pull-right" id="header-page-magazine">'+
                                '<button type="button" class="btn btn-success pull-right sortable-index-element" data-id="'+response.data_response.id+'">Recargar productos</button>'+
                            '</div>'+
                        '</div>'+
                        '<br>';

                        if(response.data_response.world.nivel1_id!=10000){ 
                        htmlWorld += 
                        '<div class="row">'+
                            '<div class="col-md-12">'+
                                '<div class="col-md-6 pull-left" >'+
                                    '<label for="rangeAge" style="display:block;">Rango etario:</label>'+
                                    '<select name="rangeAge" class="range-age" name="state" data-page="'+response.page_info.id+'">'+
                                        '<option data-id="0">TODOS</option>';
                                        $.each(response.etarios,function(index,val){
                                            htmlWorld += 
                                            '<option data-id="'+index+'" data-page="">'+val+'</option>';
                                        });
                                    htmlWorld += 
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                        }

                        htmlWorld +=
                        '</div>'+
                        '<div class="row" style="text-align: center; padding-bottom: 1px;">'+

                            '<div class="col-md-4" >'+
                                '<div style="text-align: center;"></div>'+
                                '<div><button type="button" class="btn btn-warning filters btn-xs" data-filter="madre_id" data-idga="'+idAgClick+'" data-status="asc" >Madre <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>'+
                            '</div>'+

                            '<div class="col-md-4" >'+
                                '<div style="text-align: center;"></div>'+
                                '<div><button type="button" class="btn btn-warning filters btn-xs" data-filter="name_desc" data-idga="'+idAgClick+'" data-status="asc" >Nombre <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>'+
                            '</div>'+

                            '<div class="col-md-4">'+
                                '<div style="text-align: center;"></div>'+
                                '<div><button type="button" class="btn btn-warning filters btn-xs" data-filter="diseno" data-idga="'+idAgClick+'" data-status="asc" >Diseño <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>'+
                            '</div>'+


                        '</div>'+

                        '<div class="row" style="text-align: center; padding-bottom: 1px;">'+


                            '<div class="col-md-4" >'+
                                '<div style="text-align: center;"></div>'+
                                '<div><button type="button" class="btn btn-warning filters btn-xs" data-filter="codn3" data-idga="'+idAgClick+'" data-status="asc" >N3 <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>'+
                            '</div>'+

                            '<div class="col-md-4">'+
                                '<div style="text-align: center;"></div>'+
                                '<div><button type="button" class="btn btn-warning filters btn-xs" data-filter="codn4" data-idga="'+idAgClick+'" data-status="asc" >N4 <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>'+
                            '</div>'+

                            '<div class="col-md-4">'+
                                '<div style="text-align: center;"></div>'+
                                '<div><button type="button" class="btn btn-warning filters btn-xs" data-filter="punta_precio" data-idga="'+idAgClick+'" data-status="asc" >PP <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>'+
                            '</div>'+


                        '</div>'+
                        '<hr>'+
                        '<div class="box-body">'+
                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<div id="page-content">'+
                                        '<ul id="contents-products" class="sortable contents-products">';
                                            if(response.data_response.contents!=''){

                                                response.data_response.contents.forEach(function(index){
                                                    if(index.is_new==1){styleNew = 'color:black;background-color: #FFED00;'}else{styleNew=''};
                                                    switch(index.pp_symbol){
                                                        case 1:
                                                            iconPlus =
                                                            '<div id="plus-icon" style="float:right;">'+
                                                                '<h2>(+)</h2>'+
                                                            '</div>';
                                                        break;
                                                        case 2:
                                                            iconPlus =
                                                            '<div id="minus-icon" style="float:right;">'+
                                                                '<h2>(-)</h2>'+
                                                            '</div>';
                                                        break;
                                                        default:
                                                            iconPlus = '';
                                                        break;
                                                    }
                                                    htmlWorld += 
                                                    '<li id="list-product" class="ui-state-default" data-id="'+index.id+'" data-order="">'+
                                                        '<div id="card-style">'+
                                                            '<div id="card-product" class="row">';
                                                                if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                                                htmlWorld+=
                                                                '<div class="col-md-12">'+
                                                                    '<div class="info-options">'+
                                                                        '<div class="pull-left icon-section">'+
                                                                            '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                                                '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                                            '</a>'+
                                                                            '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                                                '<div style="width: 60px;">'+
                                                                                    '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                                                '</div>'+
                                                                                '<div class="divider"></div>'+
                                                                                '<div style="width: 60px;">'+
                                                                                    '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                                '</div>'+
                                                                                '<div class="divider"></div>'+
                                                                                '<div style="width: 60px;">'+
                                                                                    '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                                '</div>'+
                                                                                '<div class="divider"></div>'+
                                                                                '<div style="width: 60px;">'+
                                                                                    '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                                '</div>'+
                                                                            '</div>'+
                                                                        '</div>'+
                                                                        '<div class="pull-right">'+
                                                                            '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                            '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                            '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</div>'+

                                                                '<div class="col-md-12">'+
                                                                    '<div class="info-product">'+
                                                                        '<dl>';
                                                                            if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                            htmlWorld +=
                                                                            '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                                            iconPlus+
                                                                            '</dt>'+
                                                                            '<dt>Diseño: <span class="design-product" data-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                                            '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                                            '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                            '<dt>'+
                                                                                '<table style="width: 100%;">'+
                                                                                    '<thead>'+
                                                                                        '<tr>'+
                                                                                            '<th><span>SKU</span></th>'+
                                                                                            '<th></th>'+
                                                                                        '</tr>'+
                                                                                    '</thead>'+
                                                                                    '<tbody>'+
                                                                                        '<tr>'+
                                                                                            '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                                            '<td></td>'+
                                                                                        '</tr>'+
                                                                                    '</tbody>'+
                                                                                '</table>'+
                                                                            '</dt>'+
                                                                        '</dl>'+
                                                                    '</div>'+
                                                                '</div>'+

                                                                '<div class="col-md-12">'+
                                                                    '<div class="image-product">'+
                                                                        '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                                    '</div>'+
                                                                '</div>'+

                                                            '</div>'+
                                                        '</div>'+
                                                    '</li>';
                                                });
                                            }else{
                                                htmlWorld += 
                                                '<div class="col-md-12" style="padding-left:0px; padding-right:35px;">'+
                                                    '<div class="callout callout-success">'+
                                                        '<h4>No se encontraron productos activos</h4>'+
                                                    '</div>'+
                                                '</div>';
                                            }
                                            htmlWorld +=                        
                                        '</ul>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
                    $('#content-page-magazine').html(htmlWorld); //ELEMENTO TERMINADO

                    //ARMADO DE LA PRIMERA PAGINA DE UNA AG
                    let worldAlias ='';
                    if(response.data_response.world.wld_alias==null){worldAlias = response.data_response.world.name}else{worldAlias=response.data_response.world.wld_alias}
                    let htmlPageStyle = 
                    '<div class="box-header">'+
                        '<div class="row">'+
                            '<div class="col-md-5">'+
                                '<h4 class="pull-left">'+response.data_response.name+'</h4>'+
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<div class="row">'+
                                    '<h4 id="page-number" class="text-center" data-page="1">'+
                                        'Pagina 1 de '+response.count_page.count+ 
                                    '</h4>'+
                                '</div>'+
                                '<div class="row">'+
                                    '<div class="col-md-12">'+
                                        '<div class="control-page.button" style="text-align:center;">'+
                                            '<a href="#" data-id="'+response.data_response.id+'" class="go-prev-page">'+
                                                '<i class="glyphicon glyphicon-chevron-left" style="color:#bfbc00; padding-right:20px;"></i>'+
                                            '</a>'+
                                            '<a href="#" data-id="'+response.data_response.id+'" class="add-new-page">'+
                                                '<i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding: 0px 5px 0px 5px;"></i>'+
                                            '</a>'+
                                            '<a href="#" data-id="'+response.data_response.id+'" class="go-next-page">'+
                                                '<i class="glyphicon glyphicon-chevron-right" style="color: #bfbc00; padding-left:20px;"></i>'+
                                            '</a>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+
                            '<div class="col-md-5">'+
                                '<h4 class="pull-right">'+
                                    '<span class="world-alias" style="color:#dd4b39;" data-id="'+response.data_response.world.id+'">'+worldAlias+'</span> / '+
                                    '<span class="magazine-name" data-id="'+response.data_response.world.magazine.id+'">'+response.data_response.world.magazine.name_feria+'</span> / '+
                                    '<span class="magazine-id" data-id="'+response.data_response.world.magazine.id+'">FERIA '+response.data_response.world.magazine.ferias_id+'</span>'+
                                '</h4>'+
                            '</div>'+
                        '</div>'+
                        '<hr>'+
                        '<div class="row">'+
                            '<div class="col-md-12">'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="2" data-y="2" data-id="'+response.data_response.id+'" data-page="1">2 X 2</button>'+ 
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="3" data-id="'+response.data_response.id+'" data-page="1">3 X 3</button>'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="5" data-id="'+response.data_response.id+'" data-page="1">3 X 5</button>'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="6" data-id="'+response.data_response.id+'" data-page="1">3 X 6</button>'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="4" data-y="4" data-id="'+response.data_response.id+'" data-page="1">4 X 4</button>'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="4" data-y="6" data-id="'+response.data_response.id+'" data-page="1">4 X 6</button>'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="5" data-id="'+response.data_response.id+'" data-page="1">5 X 5</button>'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="6" data-id="'+response.data_response.id+'" data-page="1">5 X 6</button>'+
                                '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="7" data-id="'+response.data_response.id+'" data-page="1">5 X 7</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="box-body">'+
                        '<div class="row">'+
                            '<div class="col-md-12" id="page-maker" data-areagrid="'+grid+'" >';
                            
                            console.log("-aca");
                            console.log(response.page_response.ga_pages[0].contents);

                            if(response.page_response.ga_pages[0].contents!= ''){
                                let strategyPlus = 1;
                                let varControl = 0;
                                let j = 1;
                                let test = 0;
                                for (let i = 1; i <= grid; i++) {
                                    //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                                    let variableTest = 0;
                                    if(i==strategyPlus+(response.page_info.x_value*test)){
                                        htmlPageStyle +=
                                        '<div class="row">';
                                        if(response.page_info.content_groups!=''){
                                            response.page_info.content_groups.forEach(function(index){
                                                if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                                if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                                if(index.cg_order==j){
                                                    htmlPageStyle += 
                                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                                    test++;
                                                    varControl++;
                                                }
                                            });
                                            if(varControl==0){
                                                htmlPageStyle += 
                                                '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                test++;
                                            }
                                            varControl = 0;
                                            j++;
                                        }else{
                                            htmlPageStyle += 
                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                            test++;
                                        }
                                        htmlPageStyle +=
                                        '</div>';
                                    }
                                    //FIN LOGICA GRUPO DE ANALISIS
                                    console.log("RESPUESTA AL CLICK GA");
                                    console.log(response);
                                    htmlPageStyle += 
                                    '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">';
                                    response.page_response.ga_pages[0].contents.forEach(function(index){
                                        if(index.c_order == i){
                                            let styleNew ='';
                                            if(index.is_new==1){styleNew = 'color:black;background-color: #FFED00;'}else{styleNew=''};
                                            switch(index.pp_symbol){
                                                case 1:
                                                    iconPlus =
                                                    '<div id="plus-icon" style="float:right;">'+
                                                        '<h2>(+)</h2>'+
                                                    '</div>';
                                                break;
                                                case 2:
                                                    iconPlus =
                                                    '<div id="minus-icon" style="float:right;">'+
                                                        '<h2>(-)</h2>'+
                                                    '</div>';
                                                break;
                                                default:
                                                    iconPlus = '';
                                                break;
                                            }
                                            console.log("ACA");
                                            console.log(index.id);
                                            htmlPageStyle +=
                                            '<div id="slot-'+i+'-id-'+index.id+'" class="ui-widget-header droppable ui-state-highlight" data-pos="'+i+'" style="height: 300px;">'+
                                                '<p hidden data-id="'+index.id+'"></p>'+
                                                    '<div id="card-product" data-pos="'+i+'" data-id="'+index.id+'" class="row ui-widget-content draggable" style="z-index: 100">';
                                                        if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                                        htmlPageStyle+=
                                                        '<div class="col-md-12">'+
                                                            '<div class="info-options">'+
                                                                '<div class="pull-left icon-section">'+
                                                                    '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                                        '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                                    '</a>'+
                                                                    '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                                        '</div>'+
                                                                        '<div class="divider"></div>'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                        '</div>'+
                                                                        '<div class="divider"></div>'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                        '</div>'+
                                                                        '<div class="divider"></div>'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                                '<div class="pull-right">'+
                                                                    '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                    '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                    '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                    '<a href="#" data-id="'+index.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>'+
                                                        '<div class="col-md-12">'+
                                                            '<div class="info-product">'+
                                                                '<dl>';
                                                                    if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                    htmlPageStyle +=
                                                                    '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                                    iconPlus+
                                                                    '</dt>'+
                                                                    '<dt>Diseño: <span class="design-index-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                                    '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                                    '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                    '<dt>'+
                                                                        '<table style="width: 100%;">'+
                                                                            '<thead>'+
                                                                                '<tr>'+
                                                                                    '<th><span>SKU</span></th>'+
                                                                                    '<th></th>'+
                                                                                '</tr>'+
                                                                            '</thead>'+
                                                                            '<tbody>'+
                                                                                '<tr>'+
                                                                                    '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                                    '<td></td>'+
                                                                                '</tr>'+
                                                                            '</tbody>'+
                                                                        '</table>';
                                                                        if(index.childrens!=''){
                                                                            index.childrens.forEach(function(index){
                                                                                if(index.is_new==1){styleNew = 'color:black;background-color:#FFED00;'}else{styleNew=''};
                                                                                if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                                htmlPageStyle +=
                                                                                '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                                '<dt>'+
                                                                                    '<table style="width: 100%;">'+
                                                                                        '<thead>'+
                                                                                            '<tr>'+
                                                                                                '<th><span>SKU</span></th>'+
                                                                                                '<th></th>'+
                                                                                            '</tr>'+
                                                                                        '</thead>'+
                                                                                        '<tbody>'+
                                                                                            '<tr>'+
                                                                                                '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                                                '<td></td>'+
                                                                                            '</tr>'+
                                                                                        '</tbody>'+
                                                                                    '</table>'+
                                                                                '</dt>';
                                                                            });
                                                                        }
                                                                    htmlPageStyle +=
                                                                    '</dt>'+
                                                                '</dl>'+
                                                            '</div>'+
                                                        '</div>'+
                                                        '<div class="col-md-12">'+
                                                            '<div class="image-product">'+
                                                                '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                            '</div>';
                                            variableTest++;
                                        }
                                    });

                                    if(variableTest==0){
                                        htmlPageStyle += 
                                        '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                            '<p hidden data-id="">'+
                                                'DISPONIBLE'+
                                            '</p>'+
                                        '</div>';
                                        variableTest=0;
                                    }
                                    htmlPageStyle +=
                                    '</div>';
                                }

                            }else{
                                let strategyPlus = 1;
                                let varControl = 0;
                                let j = 1;
                                let test = 0;
                                for (let i = 1; i <= grid; i++) {
                                    //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                                    let variableTest = 0;
                                    if(i==strategyPlus+(response.page_info.x_value*test)){
                                        htmlPageStyle +=
                                        '<div class="row">';
                                        if(response.page_info.content_groups!=''){
                                            response.page_info.content_groups.forEach(function(index){
                                                if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                                if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                                if(index.cg_order==j){
                                                    htmlPageStyle += 
                                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                                    test++;
                                                    varControl++;
                                                }
                                            });
                                            if(varControl==0){
                                                htmlPageStyle += 
                                                '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                test++;
                                            }
                                            varControl = 0;
                                            j++;
                                        }else{
                                            htmlPageStyle += 
                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                            test++;
                                        }
                                        htmlPageStyle +=
                                        '</div>';
                                    }
                                    //FIN LOGICA GRUPO DE ANALISIS 
                                    htmlPageStyle += 
                                    '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                        '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                            '<p hidden data-id="">'+
                                                'DISPONIBLE'+
                                            '</p>'+
                                        '</div>'+
                                    '</div>';
                                }
                            } 
                            htmlPageStyle += 
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="box-footer">'+
                        '<div class="row">'+
                            '<div class="col-md-12">'+
                                '<label for="footerIcon" style="display:block;">Seleccione icono pie de pagina:</label>'+
                                '<select name="footerIcon" id="footer-icon" class="footer-icon" name="state" data-page="'+response.page_info.id+'" style="width:100%">'+
                                    '<option data-id="0">Seleccione un icono</option>';
                                    response.tags.forEach(function(index){
                                        let state = '';
                                        if(response.page_info.tags_id==index.id){state='selected';};
                                        htmlPageStyle += 
                                        '<option title="'+index.img_url+'" data-id="'+index.id+'" data-page="'+response.page_info.id+'" '+state+'>'+index.titulo+'</option>';
                                    });
                                htmlPageStyle += 
                                '</select>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

                    $('#finish-page-element').html(htmlPageStyle);
                    $( ".footer-icon" ).select2({
                        templateResult: formatState
                    });
                    $( ".sortable" ).sortable();
                    $( ".sortable" ).disableSelection();
                    $( ".droppable" ).droppable({
                        accept: "#list-product, #card-product"
                    });

                    $( ".draggable" ).draggable({ revert: "invalid" });

                }else{
                    toastr.error('Error al generar pagina','',{timeOut: 1000});
                }
            });
        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //FUNCION PARA MOSTRAR LOS ELEMENTOS DESACTIVADOS
    $(document).on('click','.show-disable-elements',function() {
        console.log('Mostrando elementos desactivados...');
        let idAgShowDisable = $(this).attr("data-id");
        if(idAgShowDisable){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/showDisableContent',
                dataType: 'json',
                data: {
                    'idAgShowDisable': idAgShowDisable
                }
            })
            .done(function(response){
                if(response.status==true){
                    let htmlWorld = 
                    '<div class="box-header">'+
                        '<br>'+
                        '<div class="row">'+
                            '<div class="col-md-6 pull-left" id="header-page-magazine">'+
                                '<h3 class="box-title pull-left">'+response.data_response.name+'</h3>'+
                            '</div>'+
                            '<div class="col-md-6 pull-right" id="header-page-magazine">'+
                                '<button type="button" class="btn btn-danger pull-right sortable-index-element" data-id="'+response.data_response.id+'">Elementos eliminados</button>'+
                            '</div>'+
                        '</div>'+
                        '<br>'+
                        '<div class="row">'+
                            // '<div class="col-md-12">'+
                            //     '<button type="button" class="btn btn-danger pull-right sortable-index-element" data-id="'+response.data_response.id+'">Elementos eliminados</button>'+
                            // '</div>'+
                        '</div>'+
                    '</div>'+
                    '<hr>'+
                    '<div class="box-body">'+
                        '<div class="row">'+
                            '<div class="col-md-12">'+
                                '<div id="page-content">'+
                                    '<ul id="contents-products" class="sortable contents-products">';
                                        if(response.data_response.contents!=''){

                                            response.data_response.contents.forEach(function(index){
                                                if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                                switch(index.pp_symbol){
                                                    case 1:
                                                        iconPlus =
                                                        '<div id="plus-icon" style="float:right;">'+
                                                            '<h2>(+)</h2>'+
                                                        '</div>';
                                                    break;
                                                    case 2:
                                                        iconPlus =
                                                        '<div id="minus-icon" style="float:right;">'+
                                                            '<h2>(-)</h2>'+
                                                        '</div>';
                                                    break;
                                                    default:
                                                        iconPlus = '';
                                                    break;
                                                }
                                                htmlWorld += 
                                                '<li id="list-product" class="ui-state-default" data-id="'+index.id+'" data-order="">'+
                                                    '<div id="card-style">'+
                                                        '<div id="card-product" class="row">'+
                                                            '<div class="info-options col-md-12" >'+
                                                                '<div class="pull-right">'+
                                                                    '<a href="#" data-id="'+index.id+'" class="active-card-element"><i class="glyphicon glyphicon-ok" style="color: #00a65a; padding-right: 5px;"></i></a>'+
                                                                '</div>'+
                                                            '</div>'+
                                                            '<div class="info-product col-md-12" >'+
                                                                '<dl>';
                                                                    if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                    htmlWorld +=
                                                                    '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                                    iconPlus+
                                                                    '</dt>'+
                                                                    '<dt>Diseño: <span class="design-product" data-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                                    '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                                    '<dt><pre class="measurements-product" data-id="'+index.id+'">'+index.measurements+'</pre></dt>'+
                                                                    '<dt>'+
                                                                        '<table style="width: 100%;">'+
                                                                            '<thead>'+
                                                                                '<tr>'+
                                                                                    '<th><span>SKU</span></th>'+
                                                                                    '<th><span>PP</span></th>'+
                                                                                '</tr>'+
                                                                            '</thead>'+
                                                                            '<tbody>'+
                                                                                '<tr>'+
                                                                                    '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+'</div></td>'+
                                                                                    '<td><span>'+Math.abs(index.punta_precio)+'</span></td>'+
                                                                                '</tr>'+
                                                                            '</tbody>'+
                                                                        '</table>'+
                                                                    '</dt>'+
                                                                '</dl>'+
                                                            '</div>'+
                                                            '<div class="col-md-12">'+
                                                                '<div class="image-product">'+
                                                                    '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</li>';
                                            });
                                        }else{
                                            htmlWorld += 
                                            '<div class="col-md-12" style="padding-left:0px; padding-right:35px;">'+
                                                '<div class="callout callout-danger">'+
                                                    '<h4>No se encontraron productos desactivados</h4>'+
                                                '</div>'+
                                            '</div>';
                                        }
                                        htmlWorld += 
                                    '</ul>'+
                                '</div>'+    
                            '</div>'+
                        '</div>'+
                    '</div>';
                    $('#content-page-magazine').html(htmlWorld);
                    toastr.success('Productos eliminados cargados','',{timeOut: 1000});
                }else{
                    toastr.error('Error al generar pagina','',{timeOut: 1000});
                }
            });
        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });

    //ACCION AL SOLTAR UN ELEMENTO DENTRO DE UN CUADRO EN EL CREADOR DE PAGINAS asdasdasd
    $(document).on('drop','.droppable', function( event, ui ) {

        console.log("PROBANDO")
        console.log( ui.draggable.attr("data-pos"));

        console.log('Registrando elemento en la pagina...');
        let idPage = $('#page-number').attr("data-page");
        console.log("-----------------");
        console.log("idPage");
        console.log(idPage);
        let thisElement = $(this);
        console.log("-----------------");
        console.log("Element");
        console.log(thisElement);
        let idContent = ui.draggable.attr("data-id"); // ID CONTENT ELEMENTO VOLADOR
        console.log("-----------------");
        console.log("Id elemento volador ");
        console.log(idContent);
        let previousPositionVol = ui.draggable.attr("data-pos"); // ID CONTENT ELEMENTO VOLADOR
        console.log("-----------------");
        console.log("POs anterior del elemento volador ");
        console.log(previousPositionVol);
        let nPosition = $(this).attr("data-pos"); // N° ORDEN EN GRILLA
        console.log("-----------------");
        console.log("N position que se agregara");
        console.log(nPosition);
        let textContent = ui.draggable[0].innerHTML;  // TEXTO DE LA TARJETA QUE VA A CAER EN EL ESPACIO
        console.log("-----------------");
        console.log("Texto tarjeta");
        console.log(textContent);
        let idContentParent = thisElement.find('p').attr('data-id'); //ID CONTENT PADRE (SI EXISTE EN EL ESPACIO)
        console.log("-----------------");
        console.log("ID PADRE Si existe");
        console.log(idContentParent);
        let trTbody = thisElement.find('dt').last();
        console.log("-----------------");
        console.log("trTbody");
        console.log(trTbody);
        // console.log(idPage);
        //SI LA CAJA YA POSEE LA CLASE DE SELECCIONADO PREGUNTARA POR UNIR OBJETOS, SI NO LO UTILIZARA
        if(($(this).hasClass('ui-state-highlight')!=true)){
            //AJAX PARA GRABAR LA POSICION Y PAGINA DE LA TARJETA 
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/saveContentInPage',
                dataType: 'json',
                data: {
                    'idContent': idContent,
                    'nPosition': nPosition,
                    'idPage': idPage
                }
            })
            .done(function(response){

                console.log("ACA");
                console.log(response);

                if(response.data_response.is_new==1){styleNew = 'color:black;background-color: #FFED00;'}else{styleNew=''};
                switch(response.data_response.pp_symbol){
                    case 1:
                        iconPlus =
                        '<div id="plus-icon" style="float:right;">'+
                            '<h2>(+)</h2>'+
                        '</div>';
                    break;
                    case 2:
                        iconPlus =
                        '<div id="minus-icon" style="float:right;">'+
                            '<h2>(-)</h2>'+
                        '</div>';
                    break;
                    default:
                        iconPlus = '';
                    break;
                }
                //CREACION DE TARJETA UNA VEZ PUESTA
                let htmlPageStyle = 
                '<div id="card-product" data-pos="'+nPosition+'" data-id="'+idContent+'" class="row ui-widget-content draggable" style="z-index: 100" >';
                    if(response.data_response.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                    htmlPageStyle+=
                    '<div class="col-md-12">'+
                        '<div class="info-options">'+
                            '<div class="pull-left icon-section">'+
                                '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                    '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                '</a>'+
                                '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                    '<div style="width: 60px;">'+
                                        '<span class="icon-product center-block" data-id="'+response.data_response.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                    '</div>'+
                                    '<div class="divider"></div>'+
                                    '<div style="width: 60px;">'+
                                        '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                    '</div>'+
                                    '<div class="divider"></div>'+
                                    '<div style="width: 60px;">'+
                                        '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                    '</div>'+
                                    '<div class="divider"></div>'+
                                    '<div style="width: 60px;">'+
                                        '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="pull-right">'+
                                '<a href="#" data-id="'+response.data_response.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                '<a href="#" data-id="'+response.data_response.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                '<a href="#" data-id="'+response.data_response.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                '<a href="#" data-id="'+response.data_response.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                        '<div class="info-product">'+
                            '<dl>';
                                if(response.data_response.measurements==null){extraText = 'Texto Opcional'}else{extraText=response.data_response.measurements}
                                htmlPageStyle +=
                                '<dt><div id="name-product" style="word-break: break-word;">'+response.data_response.name_desc+'</div>'+
                                iconPlus+
                                '</dt>'+
                                '<dt>Diseño: <span class="design-product" data-id="'+response.data_response.id+'">'+response.data_response.diseno+'</span></dt>'+
                                '<dt>Color: <span class="color-product" data-id="'+response.data_response.id+'">'+response.data_response.color+'</span></dt>'+
                                '<dt><pre class="measurements-product" data-id="'+response.data_response.id+'">'+extraText+'</pre></dt>'+
                                '<dt>'+
                                    '<table style="width: 100%;">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th><span>SKU</span></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>'+
                                            '<tr>'+
                                                '<td><div id="sku-product" style="'+styleNew+'">'+response.data_response.sku_mod+' / <span>PP - </span><span>'+Math.abs(response.data_response.punta_precio)+'</span></div></td>'+
                                                '<td></td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                    '</table>';

                                    
                                     if(response.data_response.childrens != ''){
                                        response.data_response.childrens.forEach(function(index){
                                            if(index.is_new==1){styleNew = 'color:black;background-color:#FFED00;'}else{styleNew=''};
                                            if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                            htmlPageStyle +=
                                            '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                            '<dt>'+
                                                '<table style="width: 100%;">'+
                                                    '<thead>'+
                                                        '<tr>'+
                                                            '<th><span>SKU</span></th>'+
                                                            '<th></th>'+
                                                        '</tr>'+
                                                    '</thead>'+
                                                    '<tbody>'+
                                                        '<tr>'+
                                                            '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                            '<td></td>'+
                                                        '</tr>'+
                                                    '</tbody>'+
                                                '</table>'+
                                            '</dt>';
                                        });
                                    }  


                                htmlPageStyle += 
                                '</dt>'+
                            '</dl>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                        '<div class="image-product">'+
                            '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+response.data_response.sku.substring(5)+'.jpg" alt="product">'+
                        '</div>'+
                    '</div>'+
                '</div>';
                thisElement.addClass("ui-state-highlight").find("p").after(htmlPageStyle);
                thisElement.find("p").attr("data-id",response.data_response.id);
                thisElement.attr("id","slot-"+nPosition+"-id-"+idContent);
                ui.draggable.remove();
                thisElement.removeClass( "highlight" );

                $( ".draggable" ).draggable({ revert: "invalid" });

                if(previousPositionVol != undefined){
                    $( "#slot-"+previousPositionVol+"-id-"+idContent).removeClass('ui-state-highlight');
                    $( "#slot-"+previousPositionVol+"-id-"+idContent).find("p").attr("data-id","");
                    $( "#slot-"+previousPositionVol+"-id-"+idContent).attr("id","");

                }


            });
        }else{
            if(idContent != idContentParent){
                $( this ).removeClass( "highlight" )
                let txt;
                swal({
                    title: '¿Estas seguro de unir estos productos?',
                    text: "Si aceptas, los productos quedaran ligados",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, continuar!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if(result.value) {
                        txt = "UNIENDO";
                        $.ajax({
                            headers: {
                                'X-CSRF-Token': csrfToken
                            },
                            type: 'POST',
                            url: urlBase+'magazines/conectContents',
                            dataType: 'json',
                            data: {
                                'idContent': idContent,
                                'idContentParent': idContentParent
                            }
                        })
                        .done(function(response){
                            if(response.data_response!=''){
                                if(response.data_response.measurements==null){extraText = 'Texto Opcional'}else{extraText=response.data_response.measurements}
                                if(response.data_response.is_new==1){styleNew = 'color:black;background-color:#FFED00;'}else{styleNew=''};
                                let appendContent =
                                '<dt><pre class="measurements-product" data-id="'+response.data_response.id+'">'+extraText+'</pre></dt>'+
                                '<dt>'+
                                    '<table style="width: 100%;">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th><span>SKU</span></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>'+
                                            '<tr>'+
                                                '<td><div id="sku-product" style="'+styleNew+'">'+response.data_response.sku_mod+' / <span>PP - </span><span>'+Math.abs(response.data_response.punta_precio)+'</span></div></td>'+
                                                '<td></td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                    '</table>'+
                                '</dt>';
                                trTbody.append(appendContent);
                                ui.draggable.remove();
                            }else{
                                toastr.error('Intente nuevamente','',{timeOut: 1000});
                            }
                        })
                    }else if(result.dismiss === swal.DismissReason.cancel){
                        txt = "NADA";
                    }
                    console.log(txt);
                })
            }else{
                //AJAX PARA GRABAR LA POSICION Y PAGINA DE LA TARJETA 
                $.ajax({
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    type: 'POST',
                    url: urlBase+'magazines/saveContentInPage',
                    dataType: 'json',
                    data: {
                        'idContent': idContent,
                        'nPosition': nPosition,
                        'idPage': idPage
                    }
                })
                .done(function(response){
                    if(response.data_response.is_new==1){styleNew = 'color:black;background-color:#FFED00;'}else{styleNew=''};
                    switch(response.data_response.pp_symbol){
                        case 1:
                            iconPlus =
                            '<div id="plus-icon" style="float:right;">'+
                                '<h2>(+)</h2>'+
                            '</div>';
                        break;
                        case 2:
                            iconPlus =
                            '<div id="minus-icon" style="float:right;">'+
                                '<h2>(-)</h2>'+
                            '</div>';
                        break;
                        default:
                            iconPlus = '';
                        break;
                    }
                    //CREACION DE TARJETA UNA VEZ PUESTA
                    let htmlPageStyle = 
                    '<div id="card-product" data-pos="'+nPosition+'" data-id="'+idContent+'" class="row ui-widget-content draggable" style="z-index: 100" >';
                        if(response.data_response.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                        htmlPageStyle+=
                        '<div class="col-md-12">'+
                            '<div class="info-options">'+
                                '<div class="pull-left icon-section">'+
                                    '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                        '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                    '</a>'+
                                    '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                        '<div style="width: 60px;">'+
                                            '<span class="icon-product center-block" data-id="'+response.data_response.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                        '</div>'+
                                        '<div class="divider"></div>'+
                                        '<div style="width: 60px;">'+
                                            '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                        '</div>'+
                                        '<div class="divider"></div>'+
                                        '<div style="width: 60px;">'+
                                            '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                        '</div>'+
                                        '<div class="divider"></div>'+
                                        '<div style="width: 60px;">'+
                                            '<img class="icon-product center-block img-fluid" data-id="'+response.data_response.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="pull-right">'+
                                    '<a href="#" data-id="'+response.data_response.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                    '<a href="#" data-id="'+response.data_response.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                    '<a href="#" data-id="'+response.data_response.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                    '<a href="#" data-id="'+response.data_response.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12">'+
                            '<div class="info-product">'+
                                '<dl>';
                                    if(response.data_response.measurements==null){extraText = 'Texto Opcional'}else{extraText=response.data_response.measurements}
                                    htmlPageStyle +=
                                    '<dt><div id="name-product" style="word-break: break-word;">'+response.data_response.name_desc+'</div>'+
                                    iconPlus+
                                    '</dt>'+
                                    '<dt>Diseño: <span class="design-product" data-id="'+response.data_response.id+'">'+response.data_response.diseno+'</span></dt>'+
                                    '<dt>Color: <span class="color-product" data-id="'+response.data_response.id+'">'+response.data_response.color+'</span></dt>'+
                                    '<dt><pre class="measurements-product" data-id="'+response.data_response.id+'">'+extraText+'</pre></dt>'+
                                    '<dt>'+
                                        '<table style="width: 100%;">'+
                                            '<thead>'+
                                                '<tr>'+
                                                    '<th><span>SKU</span></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</thead>'+
                                            '<tbody>'+
                                                '<tr>'+
                                                    '<td><div id="sku-product" style="'+styleNew+'">'+response.data_response.sku_mod+' / <span>PP - </span><span>'+Math.abs(response.data_response.punta_precio)+'</span></div></td>'+
                                                    '<td></td>'+
                                                '</tr>'+
                                            '</tbody>'+
                                        '</table>'; 

                                    if(response.data_response.childrens != ''){
                                        response.data_response.childrens.forEach(function(index){
                                            if(index.is_new==1){styleNew = 'color:black;background-color:#FFED00;'}else{styleNew=''};
                                            if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                            htmlPageStyle +=
                                            '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                            '<dt>'+
                                                '<table style="width: 100%;">'+
                                                    '<thead>'+
                                                        '<tr>'+
                                                            '<th><span>SKU</span></th>'+
                                                            '<th><span>PP</span></th>'+
                                                        '</tr>'+
                                                    '</thead>'+
                                                    '<tbody>'+
                                                        '<tr>'+
                                                            '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+'</div></td>'+
                                                            '<td><span>'+Math.abs(index.punta_precio)+'</span></td>'+
                                                        '</tr>'+
                                                    '</tbody>'+
                                                '</table>'+
                                            '</dt>';
                                        });
                                    } 

                                    htmlPageStyle +=
                                    '</dt>'+
                                '</dl>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12">'+
                            '<div class="image-product">'+
                                '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+response.data_response.sku.substring(5)+'.jpg" alt="product">'+
                            '</div>'+
                        '</div>'+
                    '</div>';
                    thisElement.addClass("ui-state-highlight").find("p").after(htmlPageStyle);
                    thisElement.find("p").attr("data-id",response.data_response.id);
                    thisElement.attr("id","slot-"+nPosition+"-id-"+idContent);
                    ui.draggable.remove();
                    thisElement.removeClass( "highlight" );

                    $( ".draggable" ).draggable({ revert: "invalid" });

                    // if(previousPositionVol != undefined){
                    //     $( "#slot-"+previousPositionVol+"-id-"+idContent).removeClass('ui-state-highlight');
                    //     $( "#slot-"+previousPositionVol+"-id-"+idContent).find("p").attr("data-id","");
                    //     $( "#slot-"+previousPositionVol+"-id-"+idContent).attr("id","");

                    // }


                });
            }
        }
    });

    //ACCION AL SELECCIONAR LAS DIMENSIONES DE LA GRILLA
    $(document).on('click','.change-colum-page', function( event, ui ) {
        console.log('Cambiando dimensiones de la pagina...');

        let currentGrid = $('#page-maker').attr("data-areagrid");
        console.log("area de la grilla pagina ACTUAL");
        console.log(currentGrid);

        let idPage = $(this).closest('div.box-header').find('#page-number').attr("data-page");
        let xGrid = $(this).attr("data-x");
        let yGrid = $(this).attr("data-y");
        let idAgClick = $(this).attr("data-id");
        let grid = xGrid*yGrid;
        // console.log("area grilla que se quiere cambiar");
        // console.log(grid);
        let xParam;
        let changueParams = 1;
        //CALCULA EL MAXIMO DE LAS TARJETAS
        switch(parseInt(xGrid)){
            case 3:
                xParam = 99/xGrid;
            break;
            case 4:
                xParam = 99/xGrid;
            break;
            case 5:
                xParam = 99/xGrid;
            break;
        }

        if(grid < currentGrid){
            swal({
                title: '¿Estas seguro de cambiar el tamaño?',
                text: "Si aceptas, los productos de la pagina actual serán devueltos a la caja de productos",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, continuar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': csrfToken
                        },
                        type: 'POST',
                        url: urlBase+'magazines/getAgInfo',
                        dataType: 'json',
                        data: {
                            'idAgClick': idAgClick,
                            'changueParams': changueParams,
                            'xGrid': xGrid,
                            'yGrid': yGrid,
                            'idPage': idPage,
                            'cleanProduct': 1  
                        }
                    })
                    .done(function(response){
                        let grid = response.page_info.x_value * response.page_info.y_value;
                        let xParam = 99/response.page_info.x_value;                    
                        //CREACUIN DE GRILLA DEPENDIENDO DE LA CANTIDAD DE TARJETAS
                        let worldAlias ='';
                        if(response.data_response.world.wld_alias==null){worldAlias = response.data_response.world.name}else{worldAlias=response.data_response.world.wld_alias}
                        var htmlPageStyle =
                        '<div class="box-header">'+     
                            '<div class="row">'+        
                              '<div class="col-md-5">'+
                                    '<h4 class="pull-left">'+response.data_response.name+'</h4>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<div class="row">'+
                                        '<h4 id="page-number" class="text-center" data-page="'+idPage+'">'+
                                            'Pagina '+idPage+' de '+response.count_page.count+ 
                                        '</h4>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                            '<div class="control-page.button" style="text-align:center;">'+
                                                '<a href="#" data-id="'+response.data_response.id+'" class="go-prev-page">'+
                                                    '<i class="glyphicon glyphicon-chevron-left" style="color:#bfbc00; padding-right:20px;"></i>'+
                                                '</a>'+
                                                '<a href="#" data-id="'+response.data_response.id+'" class="add-new-page">'+
                                                    '<i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding: 0px 5px 0px 5px;"></i>'+
                                                '</a>'+
                                                '<a href="#" data-id="'+response.data_response.id+'" class="go-next-page">'+
                                                    '<i class="glyphicon glyphicon-chevron-right" style="color: #bfbc00; padding-left:20px;"></i>'+
                                                '</a>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+    
                                '</div>'+ 
                                '<div class="col-md-5">'+
                                    '<h4 class="pull-right">'+
                                        '<span class="world-alias" style="color:#dd4b39;" data-id="'+response.data_response.world.id+'">'+worldAlias+'</span> / '+
                                        '<span class="magazine-name" data-id="'+response.data_response.world.magazine.id+'">'+response.data_response.world.magazine.name_feria+'</span> / '+
                                        '<span class="magazine-id" data-id="'+response.data_response.world.magazine.id+'">FERIA '+response.data_response.world.magazine.ferias_id+'</span>'+
                                    '</h4>'+
                                '</div>'+       
                            '</div>'+
                            '<hr>'+       
                            '<div class="row">'+        
                                '<div class="col-md-12">'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="2" data-y="2" data-id="'+response.data_response.id+'">2 X 2</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="3" data-id="'+response.data_response.id+'">3 X 3</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="5" data-id="'+response.data_response.id+'">3 X 5</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="6" data-id="'+response.data_response.id+'">3 X 6</button>'+   
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="4" data-y="4" data-id="'+response.data_response.id+'">4 X 4</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="4" data-y="6" data-id="'+response.data_response.id+'">4 X 6</button>'+       
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="5" data-id="'+response.data_response.id+'">5 X 5</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="6" data-id="'+response.data_response.id+'">5 X 6</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="7" data-id="'+response.data_response.id+'">5 X 7</button>'+     
                                '</div>'+       
                            '</div>'+       
                        '</div>'+       
                        '<div class="box-body">'+       
                            '<div class="row">'+        
                                '<div class="col-md-12" id="page-maker" data-areagrid="'+grid+'">';

                                    console.log("-aca");
                                    console.log(response.message);
                                    console.log(response.page_response.ga_pages[0].contents);
                                            
                                    if(response.page_response.ga_pages[0].contents!= ''){
                                        let strategyPlus = 1;
                                        let varControl = 0;
                                        let j = 1;
                                        let test = 0;
                                        for (let i = 1; i <= grid; i++) {
                                            //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                                            let variableTest = 0;
                                            if(i==strategyPlus+(response.page_info.x_value*test)){
                                                htmlPageStyle +=
                                                '<div class="row">';
                                                if(response.page_info.content_groups!=''){
                                                    response.page_info.content_groups.forEach(function(index){
                                                        if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                                        if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                                        if(index.cg_order==j){
                                                            htmlPageStyle += 
                                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                                            test++;
                                                            varControl++;
                                                        }
                                                    });
                                                    if(varControl==0){
                                                        htmlPageStyle += 
                                                        '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                        '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                        test++;
                                                    }
                                                    varControl = 0;
                                                    j++;
                                                }else{
                                                    htmlPageStyle += 
                                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                    test++;
                                                }
                                                htmlPageStyle +=
                                                '</div>';
                                            }
                                            //FIN LOGICA GRUPO DE ANALISIS 
                                            response.page_response.ga_pages[0].contents.forEach(function(index){
                                                if(index.c_order == i){

                                                    if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                                    switch(index.pp_symbol){
                                                        case 1:
                                                            iconPlus =
                                                            '<div id="plus-icon" style="float:right;">'+
                                                                '<h2>(+)</h2>'+
                                                            '</div>';
                                                        break;
                                                        case 2:
                                                            iconPlus =
                                                            '<div id="minus-icon" style="float:right;">'+
                                                                '<h2>(-)</h2>'+
                                                            '</div>';
                                                        break;
                                                        default:
                                                            iconPlus = '';
                                                        break;
                                                    }
                                                    console.log("PINTANDO");
                                                    console.log(index);
                                                    htmlPageStyle +=
                                                    '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                                        '<div id="slot-'+i+'-id-'+index.id+'" class="ui-widget-header droppable ui-data-pos="'+i+'" style="height: 300px;">'+
                                                            '<p hidden data-id="'+index.id+'">'+
                                                                '<div id="card-product" data-pos="'+nPosition+'" data-id="'+idContent+'" class="row draggable ui-widget-content" style="z-index: 100" >';
                                                                    if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                                                    htmlPageStyle+=
                                                                    '<div class="col-md-12">'+
                                                                        '<div class="info-options">'+
                                                                           '<div class="pull-left icon-section">'+
                                                                                '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                                                    '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                                                '</a>'+
                                                                                '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                                                    '<div style="width: 60px;">'+
                                                                                        '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                                                    '</div>'+
                                                                                    '<div class="divider"></div>'+
                                                                                    '<div style="width: 60px;">'+
                                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                                    '</div>'+
                                                                                    '<div class="divider"></div>'+
                                                                                    '<div style="width: 60px;">'+
                                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                                    '</div>'+
                                                                                    '<div class="divider"></div>'+
                                                                                    '<div style="width: 60px;">'+
                                                                                        '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                                    '</div>'+
                                                                                '</div>'+
                                                                            '</div>'+
                                                                            '<div class="pull-right">'+
                                                                                '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                                '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                                '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                                '<a href="#" data-id="'+index.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                                                            '</div>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                    '<div class="col-md-12">'+
                                                                        '<div class="info-product">'+
                                                                            '<dl>';
                                                                                if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                                htmlPageStyle+=
                                                                                '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                                                iconPlus+
                                                                                '</dt>'+
                                                                                '<dt>Diseño: <span class="design-product" data-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                                                '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                                                '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                                '<dt>'+
                                                                                    '<table style="width: 100%;">'+
                                                                                        '<thead>'+
                                                                                            '<tr>'+
                                                                                                '<th><span>SKU</span></th>'+
                                                                                                '<th></th>'+
                                                                                            '</tr>'+
                                                                                        '</thead>'+
                                                                                        '<tbody>'+
                                                                                            '<tr>'+
                                                                                                '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP</span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                                                '<td></td>'+
                                                                                            '</tr>'+
                                                                                        '</tbody>'+
                                                                                    '</table>'+
                                                                                '</dt>'+
                                                                            '</dl>'+
                                                                        '</div>'+
                                                                    '</div>';
                                                                    if(index.childrens!=''){
                                                                        index.childrens.forEach(function(index){
                                                                            if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                                                            if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                            htmlPageStyle +=
                                                                            '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                            '<dt>'+
                                                                                '<table style="width: 100%;">'+
                                                                                    '<thead>'+
                                                                                        '<tr>'+
                                                                                            '<th><span>SKU</span></th>'+
                                                                                            '<th></th>'+
                                                                                        '</tr>'+
                                                                                    '</thead>'+
                                                                                    '<tbody>'+
                                                                                        '<tr>'+
                                                                                            '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                                            '<td></td>'+
                                                                                        '</tr>'+
                                                                                    '</tbody>'+
                                                                                '</table>'+
                                                                            '</dt>';
                                                                        });
                                                                    }
                                                                    htmlPageStyle +=
                                                                    '<div class="col-md-12">'+
                                                                        '<div class="image-product">'+
                                                                            '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</p>'+
                                                        '</div>'+
                                                    '</div>';
                                                    variableTest++;
                                                }
                                            });
                                            if(variableTest==0){
                                                htmlPageStyle +=
                                                '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                                    '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                                        '<p hidden data-id="">'+
                                                            'DISPONIBLE'+
                                                        '</p>'+
                                                    '</div>'+
                                                '</div>';
                                                variableTest=0;
                                            }
                                        }
                                    }else{
                                        let strategyPlus = 1;
                                        let varControl = 0;
                                        let j = 1;
                                        let test = 0;
                                        for (let i = 1; i <= grid; i++) {
                                            //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                                            let variableTest = 0;
                                            if(i==strategyPlus+(response.page_info.x_value*test)){
                                                htmlPageStyle +=
                                                '<div class="row">';
                                                if(response.page_info.content_groups!=''){
                                                    response.page_info.content_groups.forEach(function(index){
                                                        if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                                        if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                                        if(index.cg_order==j){
                                                            htmlPageStyle += 
                                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                                            test++;
                                                            varControl++;
                                                        }
                                                    });
                                                    if(varControl==0){
                                                        htmlPageStyle += 
                                                        '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                        '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                        test++;
                                                    }
                                                    varControl = 0;
                                                    j++;
                                                }else{
                                                    htmlPageStyle += 
                                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                    test++;
                                                }
                                                htmlPageStyle +=
                                                '</div>';
                                            }
                                            //FIN LOGICA GRUPO DE ANALISIS
                                            htmlPageStyle +=
                                            '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                                '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                                    '<p hidden data-id="">'+
                                                        'DISPONIBLE'+
                                                    '</p>'+
                                                '</div>'+
                                            '</div>';
                                        }
                                    }
                                htmlPageStyle +=
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="box-footer">'+
                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<label for="footerIcon" style="display:block;">Seleccione icono pie de pagina:</label>'+
                                    '<select name="footerIcon" id="footer-icon" class="footer-icon" name="state" data-page="'+response.page_info.id+'" style="width:100%;">'+
                                        '<option data-id="0">Seleccione un icono</option>';
                                        response.tags.forEach(function(index){
                                            let state = '';
                                            if(response.page_info.tags_id==index.id){state='selected';};
                                            htmlPageStyle += 
                                            '<option title="'+index.img_url+'" data-id="'+index.id+'" data-page="'+response.page_info.id+'" '+state+'>'+index.titulo+'</option>';
                                        });
                                    htmlPageStyle += 
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                        $('#finish-page-element').html(htmlPageStyle);
                        $( ".footer-icon" ).select2({
                            templateSelection: formatState
                        });
                        $( ".droppable" ).droppable({
                            accept: "#list-product, #card-product"
                        });

                        $( ".draggable" ).draggable({ revert: "invalid" });

                        toastr.success('Pagina cargada con éxito','',{timeOut: 1000});
                    });
                }
            });
        }else{
            swal({
                title: '¿Estas seguro de cambiar el tamaño?',
                text: "Si aceptas, los productos de la pagina actual serán reajustados en la pagina del catalogo",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, continuar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': csrfToken
                        },
                        type: 'POST',
                        url: urlBase+'magazines/getAgInfo',
                        dataType: 'json',
                        data: {
                            'idAgClick': idAgClick,
                            'changueParams': changueParams,
                            'xGrid': xGrid,
                            'yGrid': yGrid,
                            'idPage': idPage,
                            'cleanProduct': 0 
                        }
                    })
                    .done(function(response){
                        let grid = response.page_info.x_value * response.page_info.y_value;
                        let xParam = 99/response.page_info.x_value;                    
                        //CREACUIN DE GRILLA DEPENDIENDO DE LA CANTIDAD DE TARJETAS
                        let worldAlias ='';
                        if(response.data_response.world.wld_alias==null){worldAlias = response.data_response.world.name}else{worldAlias=response.data_response.world.wld_alias}
                        var htmlPageStyle =
                        '<div class="box-header">'+     
                            '<div class="row">'+        
                              '<div class="col-md-5">'+
                                    '<h4 class="pull-left">'+response.data_response.name+'</h4>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<div class="row">'+
                                        '<h4 id="page-number" class="text-center" data-page="'+idPage+'">'+
                                            'Pagina '+idPage+' de '+response.count_page.count+ 
                                        '</h4>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                            '<div class="control-page.button" style="text-align:center;">'+
                                                '<a href="#" data-id="'+response.data_response.id+'" class="go-prev-page">'+
                                                    '<i class="glyphicon glyphicon-chevron-left" style="color:#bfbc00; padding-right:20px;"></i>'+
                                                '</a>'+
                                                '<a href="#" data-id="'+response.data_response.id+'" class="add-new-page">'+
                                                    '<i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding: 0px 5px 0px 5px;"></i>'+
                                                '</a>'+
                                                '<a href="#" data-id="'+response.data_response.id+'" class="go-next-page">'+
                                                    '<i class="glyphicon glyphicon-chevron-right" style="color: #bfbc00; padding-left:20px;"></i>'+
                                                '</a>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+    
                                '</div>'+ 
                                '<div class="col-md-5">'+
                                    '<h4 class="pull-right">'+
                                        '<span class="world-alias" style="color:#dd4b39;" data-id="'+response.data_response.world.id+'">'+worldAlias+'</span> / '+
                                        '<span class="magazine-name" data-id="'+response.data_response.world.magazine.id+'">'+response.data_response.world.magazine.name_feria+'</span> / '+
                                        '<span class="magazine-id" data-id="'+response.data_response.world.magazine.id+'">FERIA '+response.data_response.world.magazine.ferias_id+'</span>'+
                                    '</h4>'+
                                '</div>'+       
                            '</div>'+
                            '<hr>'+       
                            '<div class="row">'+        
                                '<div class="col-md-12">'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="2" data-y="2" data-id="'+response.data_response.id+'">2 X 2</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="3" data-id="'+response.data_response.id+'">3 X 3</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="5" data-id="'+response.data_response.id+'">3 X 5</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="3" data-y="6" data-id="'+response.data_response.id+'">3 X 6</button>'+   
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="4" data-y="4" data-id="'+response.data_response.id+'">4 X 4</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="4" data-y="6" data-id="'+response.data_response.id+'">4 X 6</button>'+        
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="5" data-id="'+response.data_response.id+'">5 X 5</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="6" data-id="'+response.data_response.id+'">5 X 6</button>'+
                                    '<button type="button" class="btn btn-success pull-left change-colum-page" data-x="5" data-y="7" data-id="'+response.data_response.id+'">5 X 7</button>'+     
                                '</div>'+       
                            '</div>'+       
                        '</div>'+       
                        '<div class="box-body">'+       
                            '<div class="row">'+        
                                '<div class="col-md-12" id="page-maker" data-areagrid="'+grid+'">';

                                console.log("-aca");
                                console.log(response.message);
                                console.log(response.page_response.ga_pages[0].contents);
                                        
                                    if(response.page_response.ga_pages[0].contents!= ''){
                                        let strategyPlus = 1;
                                let varControl = 0;
                                let j = 1;
                                let test = 0;
                                for (let i = 1; i <= grid; i++) {
                                    //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                                    let variableTest = 0;
                                    if(i==strategyPlus+(response.page_info.x_value*test)){
                                        htmlPageStyle +=
                                        '<div class="row">';
                                        if(response.page_info.content_groups!=''){
                                            response.page_info.content_groups.forEach(function(index){
                                                if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                                if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                                if(index.cg_order==j){
                                                    htmlPageStyle += 
                                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                                    test++;
                                                    varControl++;
                                                }
                                            });
                                            if(varControl==0){
                                                htmlPageStyle += 
                                                '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                test++;
                                            }
                                            varControl = 0;
                                            j++;
                                        }else{
                                            htmlPageStyle += 
                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                            test++;
                                        }
                                        htmlPageStyle +=
                                        '</div>';
                                    }
                                    //FIN LOGICA GRUPO DE ANALISIS
                                    console.log("RESPUESTA AL CLICK GA");
                                    console.log(response);
                                    htmlPageStyle += 
                                    '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">';
                                    response.page_response.ga_pages[0].contents.forEach(function(index){
                                        if(index.c_order == i){
                                            let styleNew ='';
                                            if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                            switch(index.pp_symbol){
                                                case 1:
                                                    iconPlus =
                                                    '<div id="plus-icon" style="float:right;">'+
                                                        '<h2>(+)</h2>'+
                                                    '</div>';
                                                break;
                                                case 2:
                                                    iconPlus =
                                                    '<div id="minus-icon" style="float:right;">'+
                                                        '<h2>(-)</h2>'+
                                                    '</div>';
                                                break;
                                                default:
                                                    iconPlus = '';
                                                break;
                                            }
                                            console.log("ACA");
                                            console.log(index.id);
                                            htmlPageStyle +=
                                            '<div id="slot-'+i+'-id-'+index.id+'" class="ui-widget-header droppable ui-state-highlight" data-pos="'+i+'" style="height: 300px;">'+
                                                '<p hidden data-id="'+index.id+'"></p>'+
                                                    '<div id="card-product" data-pos="'+i+'" data-id="'+index.id+'" class="row ui-widget-content draggable" style="z-index: 100">';
                                                        if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                                        htmlPageStyle+=
                                                        '<div class="col-md-12">'+
                                                            '<div class="info-options">'+
                                                                '<div class="pull-left icon-section">'+
                                                                    '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                                        '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                                    '</a>'+
                                                                    '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                                        '</div>'+
                                                                        '<div class="divider"></div>'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                        '</div>'+
                                                                        '<div class="divider"></div>'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                        '</div>'+
                                                                        '<div class="divider"></div>'+
                                                                        '<div style="width: 60px;">'+
                                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                                '<div class="pull-right">'+
                                                                    '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                    '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                    '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                                    '<a href="#" data-id="'+index.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>'+
                                                        '<div class="col-md-12">'+
                                                            '<div class="info-product">'+
                                                                '<dl>';
                                                                    if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                    htmlPageStyle +=
                                                                    '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                                    iconPlus+
                                                                    '</dt>'+
                                                                    '<dt>Diseño: <span class="design-index-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                                    '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                                    '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                    '<dt>'+
                                                                        '<table style="width: 100%;">'+
                                                                            '<thead>'+
                                                                                '<tr>'+
                                                                                    '<th><span>SKU</span></th>'+
                                                                                    '<th></th>'+
                                                                                '</tr>'+
                                                                            '</thead>'+
                                                                            '<tbody>'+
                                                                                '<tr>'+
                                                                                    '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                                    '<td></td>'+
                                                                                '</tr>'+
                                                                            '</tbody>'+
                                                                        '</table>';
                                                                        if(index.childrens!=''){
                                                                            index.childrens.forEach(function(index){
                                                                                if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                                                                if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                                                htmlPageStyle +=
                                                                                '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                                                '<dt>'+
                                                                                    '<table style="width: 100%;">'+
                                                                                        '<thead>'+
                                                                                            '<tr>'+
                                                                                                '<th><span>SKU</span></th>'+
                                                                                                '<th></th>'+
                                                                                            '</tr>'+
                                                                                        '</thead>'+
                                                                                        '<tbody>'+
                                                                                            '<tr>'+
                                                                                                '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                                                '<td></td>'+
                                                                                            '</tr>'+
                                                                                        '</tbody>'+
                                                                                    '</table>'+
                                                                                '</dt>';
                                                                            });
                                                                        }
                                                                    htmlPageStyle +=
                                                                    '</dt>'+
                                                                '</dl>'+
                                                            '</div>'+
                                                        '</div>'+
                                                        '<div class="col-md-12">'+
                                                            '<div class="image-product">'+
                                                                '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+  
                                            '</div>';
                                            variableTest++;
                                        }
                                    });

                                    if(variableTest==0){
                                        htmlPageStyle += 
                                        '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                            '<p hidden data-id="">'+
                                                'DISPONIBLE'+
                                            '</p>'+
                                        '</div>';
                                        variableTest=0;
                                    }
                                    htmlPageStyle +=
                                    '</div>';
                                }
                                    }else{
                                        let strategyPlus = 1;
                                        let varControl = 0;
                                        let j = 1;
                                        let test = 0;
                                        for (let i = 1; i <= grid; i++) {
                                            //LOGICA PARA AGREGAR GRUPO DE ANALISIS
                                            let variableTest = 0;
                                            if(i==strategyPlus+(response.page_info.x_value*test)){
                                                htmlPageStyle +=
                                                '<div class="row">';
                                                if(response.page_info.content_groups!=''){
                                                    response.page_info.content_groups.forEach(function(index){
                                                        if(index.title==null){contentGroupTitle = 'Cuadro de estrategia'}else{contentGroupTitle=index.title}
                                                        if(index.subtitle==null){contentGroupSubTitle = 'Subtitulo de estrategia'}else{contentGroupSubTitle=index.subtitle}
                                                        if(index.cg_order==j){
                                                            htmlPageStyle += 
                                                            '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupTitle+'</span></div>'+
                                                            '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">'+contentGroupSubTitle+'</span></div>';
                                                            test++;
                                                            varControl++;
                                                        }
                                                    });
                                                    if(varControl==0){
                                                        htmlPageStyle += 
                                                        '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                        '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                        test++;
                                                    }
                                                    varControl = 0;
                                                    j++;
                                                }else{
                                                    htmlPageStyle += 
                                                    '<div class="col-md-6"><span class="strategyGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Cuadro de estrategia</span></div>'+
                                                    '<div class="col-md-6"><span class="subTitleGroup" data-pos="'+test+'" data-page="'+response.page_info.id+'" style="font-size:25px;">Subtitulo de estrategia</span></div>';
                                                    test++;
                                                }
                                                htmlPageStyle +=
                                                '</div>';
                                            }
                                            //FIN LOGICA GRUPO DE ANALISIS
                                            htmlPageStyle +=
                                            '<div class="col-md-2" style="padding: 1px 1px 1px 1px; width: '+xParam+'%; height: 300px;">'+
                                                '<div id="" class="ui-widget-header droppable" data-pos="'+i+'" style="height: 300px;">'+
                                                    '<p hidden data-id="">'+
                                                        'DISPONIBLE'+
                                                    '</p>'+
                                                '</div>'+
                                            '</div>';
                                        }
                                    }
                                htmlPageStyle +=
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="box-footer">'+
                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<label for="footerIcon" style="display:block;">Seleccione icono pie de pagina:</label>'+
                                    '<select name="footerIcon" id="footer-icon" class="footer-icon" name="state" data-page="'+response.page_info.id+'" style="width:100%;">'+
                                        '<option data-id="0">Seleccione un icono</option>';
                                        response.tags.forEach(function(index){
                                            let state = '';
                                            if(response.page_info.tags_id==index.id){state='selected';};
                                            htmlPageStyle += 
                                            '<option title="'+index.img_url+'" data-id="'+index.id+'" data-page="'+response.page_info.id+'" '+state+'>'+index.titulo+'</option>';
                                        });
                                    htmlPageStyle += 
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                        $('#finish-page-element').html(htmlPageStyle);
                        $( ".footer-icon" ).select2({
                            templateSelection: formatState
                        });
                        $( ".droppable" ).droppable({
                            accept: "#list-product, #card-product"
                        });

                        $( ".draggable" ).draggable({ revert: "invalid" });

                        toastr.success('Pagina cargada con éxito','',{timeOut: 1000});
                    });
                }
            });
        }

    });

    //ACCION PARA AGREGAR PAGINA NUEVA
    $(document).on('click','.add-new-page', function( event, ui ) {
        console.log('Creando nueva pagina...');
        htmlPageStyle = 'WENA WENA';
        let idAg = $(this).attr('data-id');
        let nPage = $(this).closest('div.box-header').find('#page-number').attr("data-page");
        nPage++;
        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            type: 'POST',
            url: urlBase+'magazines/createNewPage',
            dataType: 'json',
            data: {
                'nPage': nPage,
                'idAg': idAg
            }
        })
        .done(function(response){
            if(response.status==true){
                --nPage;
                toastr.success('Pagina creada con éxito','',{timeOut: 1000});
                $('#page-number').html('Pagina '+nPage+' de '+response.count_page.count);
                // nPage.html = $(this).closest('div.box-header').find('#page-number').attr("data-page");
            }else{
                toastr.error('Error al crear pagina, intente nuevamente','',{timeOut: 2000});
            }
        });
    });

    //SELECCIONAR UN ICONO
    $(document).on('change','.footer-icon',function() {
        console.log('Registrando icono en la pagina...');
        let idPage = $(this).attr('data-page');
        let idIcon = $(this).find("option:selected").attr('data-id');
        if(idIcon!=undefined){   
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/selectIconPage',
                dataType: 'json',
                data: {
                    'idPage': idPage,
                    'idIcon': idIcon
                }
            })
            .done(function(response){
                if(response.status==true){
                    toastr.success('Icono registrado correctamente','',{timeOut: 1000});
                }else{
                    toastr.error('Intente nuevamente','',{timeOut: 1000});
                }
            })
        }
    });

    //SELECCIONAR UN ICONO
    $(document).on('change','.range-age',function() {
        console.log('Aplicando filtro etario...');
        let idPage = $(this).attr('data-page');
        let idRangeAge = $(this).find("option:selected").attr('data-id');
        if(idRangeAge!=undefined){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/rangeAgeFilter',
                dataType: 'json',
                data: {
                    'idPage': idPage,
                    'idRangeAge': idRangeAge
                }
            })
            .done(function(response){
                if(response.status==true){
                    let htmlWorld = '';
                    if(response.data_response.contents!=''){
                        response.data_response.contents.forEach(function(index){
                            if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                            switch(index.pp_symbol){
                                case 1:
                                    iconPlus =
                                    '<div id="plus-icon" style="float:right;">'+
                                        '<h2>(+)</h2>'+
                                    '</div>';
                                break;
                                case 2:
                                    iconPlus =
                                    '<div id="minus-icon" style="float:right;">'+
                                        '<h2>(-)</h2>'+
                                    '</div>';
                                break;
                                default:
                                    iconPlus = '';
                                break;
                            }
                            htmlWorld += 
                            '<li id="list-product" class="ui-state-default" data-id="'+index.id+'" data-order="">'+
                                '<div id="card-style">'+
                                    '<div id="card-product" class="row">';
                                        if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                        htmlWorld+=
                                        '<div class="col-md-12">'+
                                            '<div class="info-options">'+
                                                '<div class="pull-left icon-section">'+
                                                    '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                        '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                    '</a>'+
                                                    '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                        '<div style="width: 60px;">'+
                                                            '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                        '</div>'+
                                                        '<div class="divider"></div>'+
                                                        '<div style="width: 60px;">'+
                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                        '</div>'+
                                                        '<div class="divider"></div>'+
                                                        '<div style="width: 60px;">'+
                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                        '</div>'+
                                                        '<div class="divider"></div>'+
                                                        '<div style="width: 60px;">'+
                                                            '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="pull-right">'+
                                                    '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                    '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                    '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                    '<a href="#" data-id="'+index.id+'" class="delete-put-card-element"><i class="glyphicon glyphicon-remove" style="color: #dd4b39; padding-right: 5px;"></i></a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="info-product">'+
                                                '<dl>';
                                                    if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                    htmlWorld+=
                                                    '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                    iconPlus+
                                                    '</dt>'+
                                                    '<dt>Diseño: <span class="design-product" data-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                    '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                    '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                    '<dt>'+
                                                        '<table style="width: 100%;">'+
                                                            '<thead>'+
                                                                '<tr>'+
                                                                    '<th><span>SKU</span></th>'+
                                                                    '<th><span>PP</span></th>'+
                                                                '</tr>'+
                                                            '</thead>'+
                                                            '<tbody>'+
                                                                '<tr>'+
                                                                    '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+'</div></td>'+
                                                                    '<td><span>'+Math.abs(index.punta_precio)+'</span></td>'+
                                                                '</tr>'+
                                                            '</tbody>'+
                                                        '</table>'+
                                                    '</dt>'+
                                                '</dl>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="image-product">'+
                                                '<img class="product-img center-block" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</li>';
                        });
                    }else{
                        htmlWorld += 
                        '<div class="col-md-12" style="padding-left:0px; padding-right:35px;">'+
                            '<div class="callout callout-success">'+
                                '<h4>No se encontraron productos activos</h4>'+
                            '</div>'+
                        '</div>';
                    }

                    $('#contents-products').html(htmlWorld);
                    toastr.success('Icono registrado correctamente','',{timeOut: 1000});
                }else{
                    toastr.error('Intente nuevamente','',{timeOut: 1000});
                }
            })
        }
    });


    //SELECCIONAR UN ICONO PARA UN PRODUCTO
    $(document).on('click','.icon-product',function() {
        console.log('Registrando icono para un producto...');
        let idContent = $(this).attr('data-id');
        let idIcon = $(this).attr('data-icon-id');
        let colorIcon = $(this).closest('div.icon-section').find('.glyphicon-flag');
        if(idIcon!=undefined){   
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/iconSelectProduct',
                dataType: 'json',
                data: {
                    'idContent': idContent,
                    'idIcon': idIcon
                }
            })
            .done(function(response){
                if(response.status==true){
                    toastr.success('Icono registrado correctamente','',{timeOut: 1000});
                    if(idIcon==0){
                        colorIcon.css('color','#bfbc00');
                    }else{
                        colorIcon.css('color','tomato');
                    }
                }else{
                    toastr.error('Intente nuevamente','',{timeOut: 1000});
                }
            })
        }
    });
    //FUNCION PARA ENVIAR UN MUNDO DEL INDEX DINAMICO AL INDEX DEL CATALOGO
    $('.send-to-catalog').on('click', function( event, ui ) {
        event.stopPropagation();
        Pace.start();
        console.log('..Enviando mundo al indice del catalogo.');
        
        var idWorld = $(this).attr('data-id');
        var idFeria = $(this).attr('data-feria-id');
        var idMagazine = $(this).attr('data-magazine-id');

        if(idWorld!=undefined && idFeria!=undefined && idMagazine!=undefined){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/setWorldToIndex',
                dataType: 'json',
                data: {
                    'idWorld': idWorld,
                    'idFeria': idFeria,
                    'idMagazine': idMagazine
                    },
                cache:false
            })
            .done(function(response){
                console.log('..done');
                if(response.status==true){

                    swal({
                        closeOnClickOutside: false,
                        title: 'Notificación',
                        text: response.msn,
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Continuar!'
                    }).then((result) => {
                        Pace.stop();
                        location.reload();
                    });

                }else{
                    Pace.stop();
                    swal({
                        closeOnClickOutside: false,
                        title: 'Notificación',
                        text: response.msn,
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Continuar!'
                    });
                }
            });
        }
        Pace.stop();
    });

    //FUNCION PARA ORDENAR EL CONTENIDO
    $(document).on('click','.filters',function() {

        console.log('Obteniendo información de grupo de análisis...');

        let filter = $(this).attr("data-filter");
        let idAgClick = $(this).attr("data-idga");
        'Nombre<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>'
        let statusFilter = $(this).attr("data-status");
        if(statusFilter == 'asc'){
            $(this).attr("data-status",'desc');
            $(this).find('span').removeClass('glyphicon-chevron-down');
            $(this).find('span').addClass('glyphicon-chevron-up');
            statusFilter = 'desc'
        }else{
            $(this).attr("data-status",'asc');
            $(this).find('span').removeClass('glyphicon-chevron-up');
            $(this).find('span').addClass('glyphicon-chevron-down');
            statusFilter = 'asc'

        }

        if(idAgClick!=undefined){
            let createPage=1;
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/getAgInfo',
                dataType: 'json',
                data: {
                    'idAgClick': idAgClick,
                    'createPage': createPage
                },
                cache:false
            })
            .done(function(response){
                if(response.status==true){

                    let styleNew ='';
                    console.log(response);
                    sortarr(response.data_response.contents, filter, statusFilter)

                    let htmlWorld =
                    '<ul id="contents-products" class="sortable contents-products">';
                        if(response.data_response.contents!=''){

                            response.data_response.contents.forEach(function(index){
                                if(index.is_new==1){styleNew = 'color:black;background-color:goldenrod;'}else{styleNew=''};
                                switch(index.pp_symbol){
                                    case 1:
                                        iconPlus =
                                        '<div id="plus-icon" style="float:right;">'+
                                            '<h2>(+)</h2>'+
                                        '</div>';
                                    break;
                                    case 2:
                                        iconPlus =
                                        '<div id="minus-icon" style="float:right;">'+
                                            '<h2>(-)</h2>'+
                                        '</div>';
                                    break;
                                    default:
                                        iconPlus = '';
                                    break;
                                }
                                htmlWorld += 
                                '<li id="list-product" class="ui-state-default" data-id="'+index.id+'" data-order="">'+
                                    '<div id="card-style">'+
                                        '<div id="card-product" class="row">';
                                            if(index.tags_id==null){colorIcon = '#bfbc00'}else{colorIcon='tomato'}
                                            htmlWorld+=
                                            '<div class="col-md-12">'+
                                                '<div class="info-options">'+
                                                    '<div class="pull-left icon-section">'+
                                                        '<a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown" aria-expanded="false">'+
                                                            '<i class="glyphicon glyphicon-flag" style="color: '+colorIcon+'; padding-right: 5px;"></i>'+
                                                        '</a>'+
                                                        '<div class="dropdown-menu" style="border: solid;border-color: tomato;">'+
                                                            '<div style="width: 60px;">'+
                                                                '<span class="icon-product center-block" data-id="'+index.id+'" data-icon-id="0" style="width:100%; height:100%;">SIN IMAGEN</span>'+
                                                            '</div>'+
                                                            '<div class="divider"></div>'+
                                                            '<div style="width: 60px;">'+
                                                                '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="3" src="'+urlBase+'img/icon_4x/rec_4.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                            '</div>'+
                                                            '<div class="divider"></div>'+
                                                            '<div style="width: 60px;">'+
                                                                '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="4" src="'+urlBase+'img/icon_4x/rec_5.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                            '</div>'+
                                                            '<div class="divider"></div>'+
                                                            '<div style="width: 60px;">'+
                                                                '<img class="icon-product center-block img-fluid" data-id="'+index.id+'" data-icon-id="5" src="'+urlBase+'img/icon_4x/rec_6.png" alt="iconProduct" style="width:100%; height:100%;">'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '<div class="pull-right">'+
                                                        '<a href="#" data-id="'+index.id+'" class="minus-put-card-element"><i class="glyphicon glyphicon-minus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                        '<a href="#" data-id="'+index.id+'" class="plus-put-card-element"><i class="glyphicon glyphicon-plus" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                        '<a href="#" data-id="'+index.id+'" class="mark-put-card-element"><i class="glyphicon glyphicon-star" style="color: #bfbc00; padding-right: 5px;"></i></a>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+

                                            '<div class="col-md-12">'+
                                                '<div class="info-product">'+
                                                    '<dl>';
                                                        if(index.measurements==null){extraText = 'Texto Opcional'}else{extraText=index.measurements}
                                                        htmlWorld +=
                                                        '<dt><div id="name-product" style="word-break: break-word;">'+index.name_desc+'</div>'+
                                                        iconPlus+
                                                        '</dt>'+
                                                        '<dt>Diseño: <span class="design-product" data-id="'+index.id+'">'+index.diseno+'</span></dt>'+
                                                        '<dt>Color: <span class="color-product" data-id="'+index.id+'">'+index.color+'</span></dt>'+
                                                        '<dt><pre class="measurements-product" data-id="'+index.id+'">'+extraText+'</pre></dt>'+
                                                        '<dt>'+
                                                            '<table style="width: 100%;">'+
                                                                '<thead>'+
                                                                    '<tr>'+
                                                                        '<th><span>SKU</span></th>'+
                                                                        '<th></th>'+
                                                                    '</tr>'+
                                                                '</thead>'+
                                                                '<tbody>'+
                                                                    '<tr>'+
                                                                        '<td><div id="sku-product" style="'+styleNew+'">'+index.sku_mod+' / <span>PP - </span><span>'+Math.abs(index.punta_precio)+'</span></div></td>'+
                                                                        '<td></td>'+
                                                                    '</tr>'+
                                                                '</tbody>'+
                                                            '</table>'+
                                                        '</dt>'+
                                                    '</dl>'+
                                                '</div>'+
                                            '</div>'+

                                            '<div class="col-md-12">'+
                                                '<div class="image-product">'+
                                                    '<img class="product-img center-block img-fluid" src="'+urlBase+'img/c_muestra/'+index.sku.substring(5)+'.jpg" alt="product">'+
                                                '</div>'+
                                            '</div>'+

                                        '</div>'+
                                    '</div>'+
                                '</li>';
                            });
                        }else{
                            htmlWorld += 
                            '<div class="col-md-12" style="padding-left:0px; padding-right:35px;">'+
                                '<div class="callout callout-success">'+
                                    '<h4>No se encontraron productos activos</h4>'+
                                '</div>'+
                            '</div>';
                        }
                        htmlWorld +=                        
                    '</ul>';

                    $('#contents-products').html(htmlWorld);
                    $( ".sortable" ).sortable();
                    $( ".sortable" ).disableSelection();
                    $( ".droppable" ).droppable({
                        accept: "#list-product, #card-product"
                    });

                    $( ".draggable" ).draggable({ revert: "invalid" });

                }
            });

        }else{
            toastr.error('Intente nuevamente','',{timeOut: 1000});
        }
    });


    function sortarr (arr,item,dir) {

        if(dir == 'desc'){
            arr.sort(function (a,b) {
                if (typeof a == "object" && typeof b == "object" ) {
                    //console.log(a[item]);
                    if ( a[item] > b[item] )
                        return -1;
                    if ( a[item] < b[item] )
                        return 1;
                return 0;
                }
            });

            
        }else{
            arr.sort(function (a,b) {
                if (typeof a == "object" && typeof b == "object" ) {
                    //console.log(a[item]);
                    if ( a[item] < b[item] )
                        return -1;
                    if ( a[item] > b[item] )
                        return 1;
                return 0;
                }
            });
        }

        return arr;
    }

    //FUNCION PARA ELIMINAR UN MUNDO DEL INDICE DINAMICO
    $('.remove-world').on('click', function( event, ui ) {
        event.stopPropagation();
        Pace.start();
        console.log('..Eliminando mundo del indice dinamico.');
        
        var idWorld = $(this).attr('data-id');
        var idFeria = $(this).attr('data-feria-id');
        var idMagazine = $(this).attr('data-magazine-id');
        console.log(' / '+idWorld +' / '+ idFeria +' / '+ idMagazine +' / ');
        if(idWorld!=undefined && idFeria!=undefined && idMagazine!=undefined){



            swal({
                title: '¿Estas seguro de quitar el mundo del índice del catálogo?',
                text: "Si aceptas, los cambios realizados sobre este mundo en el catálogo no podrán ser recuperados.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, continuar!'
            }).then((result) => {
                console.log(result);
                if(result.value){
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': csrfToken
                        },
                        type: 'POST',
                        url: urlBase+'magazines/removeWorldFromIndex',
                        dataType: 'json',
                        data: {
                            'idWorld': idWorld,
                            'idFeria': idFeria,
                            'idMagazine': idMagazine
                            },
                        cache:false
                    })
                    .done(function(response){
                        console.log('..done');
                        if(response.status==true){
                            toastr.success(response.msn, '',{timeOut: 6000});
                            Pace.stop();
                            location.reload();

                        }else{
                            Pace.stop();
                            toastr.error(response.msn, '',{timeOut: 6000});
                        }
                    });
                }
            });
        }
        Pace.stop();
    });


    //FUNCION PARA ENVIAR UN GA DEL INDEX DINAMICO AL INDEX DEL CATALOGO EN SU MUNDO
    $('.send-ga-to-catalog').on('click', function( event, ui ) {
        event.stopPropagation();
        Pace.start();
        console.log('..Enviando GA a su mundo en indice del catalogo.');
        
        var idWorld = $(this).attr('data-id');
        var idFeria = $(this).attr('data-feria-id');
        var idMagazine = $(this).attr('data-magazine-id');
        var idGa = $(this).attr('data-ga-id');

        if(idWorld!=undefined && idFeria!=undefined && idMagazine!=undefined && idGa!=undefined){
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: urlBase+'magazines/setGaToWorldInIndex',
                dataType: 'json',
                data: {
                    'idWorld': idWorld,
                    'idFeria': idFeria,
                    'idMagazine': idMagazine,
                    'idGa': idGa
                    },
                cache:false
            })
            .done(function(response){
                if(response.status==true){

                    swal({
                        closeOnClickOutside: false,
                        title: 'Notificación',
                        text: response.msn,
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Continuar!'
                    }).then((result) => {
                        Pace.stop();
                        location.reload();
                    });
                }else{
                    Pace.stop();
                    swal({
                        closeOnClickOutside: false,
                        title: 'Notificación',
                        text: response.msn,
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Continuar!'
                    });
                }
            });
        }
        Pace.stop();
    });


    //FUNCION PARA ELIMINAR UN GA DEL CATALOG
    $('.remove-ga-from-catalog').on('click', function( event, ui ) {
        event.stopPropagation();
        Pace.start();
        console.log('..remove ga from catalog');

        var idGa = $(this).attr('data-id');
        var idFeria = $(this).attr('data-feria-id');
        var idMagazine = $(this).attr('data-magazine-id');
        console.log(' / '+idGa +' / '+ idFeria +' / '+ idMagazine +' / ');
        if(idGa!=undefined && idFeria!=undefined && idMagazine!=undefined){



            swal({
                title: '¿Estas seguro de quitar el grupo de análisis del índice del catálogo?',
                text: "Si aceptas, los cambios realizados sobre este grupo de análisis en el catálogo no podrán ser recuperados.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, continuar!'
            }).then((result) => {
                console.log(result);
                if(result.value){
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': csrfToken
                        },
                        type: 'POST',
                        url: urlBase+'magazines/removeGaFromWorld',
                        dataType: 'json',
                        data: {
                            'idGa': idGa,
                            'idFeria': idFeria,
                            'idMagazine': idMagazine
                            },
                        cache:false
                    })
                    .done(function(response){
                        console.log('..done');
                        if(response.status==true){
                            swal({
                                closeOnClickOutside: false,
                                title: 'Notificación',
                                text: response.msn,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Continuar!'
                            }).then((result) => {
                                Pace.stop();
                                location.reload();
                            });

                        }else{
                            Pace.stop();
                            swal({
                                closeOnClickOutside: false,
                                title: 'Notificación',
                                text: response.msn,
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Continuar!'
                            });
                        }
                    });
                }
            });
        }

    });

});