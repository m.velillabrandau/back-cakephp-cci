<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->addExtensions(['pdf']);
    $routes->resources('Users');
    $routes->resources('Magazines');
    $routes->resources('Worlds');
    $routes->resources('Ags');
    $routes->resources('Contents');
    $routes->resources('Tags');
    $routes->resources('Ferias');
    $routes->resources('Pages');
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    //$routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */


    $routes->fallbacks(DashedRoute::class);
});



Router::prefix('api', function ($routes) {
    $routes->setExtensions(['json', 'xml']);
    $routes->resources('Users');
    Router::connect('/api/users/register', ['controller' => 'Users', 'action' => 'add']);
    Router::connect('/api/users/token', ['controller' => 'Users', 'action' => 'token']);
    $routes->fallbacks('InflectedRoute');
});

Router::scope('/', function ($routes) {

    $routes->resources('Magazines', function ($routes) {
        $routes->resources('Worlds', [
            'map' => [
                'getByFeria' => [
                    'action' => 'getByFeria',
                    'method' => 'GET',
                    'path' => '/getworlds/:id'
                ],
                'magazineContent' => [
                    'action' => 'magazineContent',
                    'method' => 'GET',
                    'path' => '/magazinecontent/:id'
                ],
                'saveWorldStructure' => [
                    'action' => 'saveWorldStructure',
                    'method' => 'POST',
                    'path' => '/saveworldstructure'
                ],
                'getLocalWorlds' => [
                    'action' => 'getLocalWorlds',
                    'method' => 'GET',
                    'path' => '/getlocalworlds/:id'
                ],
                'deleteWorldStructure' => [
                    'action' => 'deleteWorldStructure',
                    'method' => 'POST',
                    'path' => '/deleteWorldStructure'
                ],
                'editCover' => [
                    'action' => 'editCover',
                    'method' => 'PUT',
                    'path' => '/editcover'
                ],
                'reorder' => [
                    'action' => 'reorder',
                    'method' => 'POST',
                    'path' => '/reorder'
                ],
            ]
        ]);
    });
});

Router::scope('/', function ($routes) {

    $routes->resources('Worlds', function ($routes) {
        $routes->resources('Ags', [
            'map' => [
                'getGas' => [
                    'action' => 'getGas',
                    'method' => 'GET',
                   'path' => '/getgas/:n1/:n2'
                ],
                'saveLocalGa' => [
                    'action' => 'saveLocalGa',
                    'method' => 'POST',
                   'path' => '/savelocalga'
                ],
                'getLocalGa' => [
                    'action' => 'getLocalGa',
                    'method' => 'GET',
                    'path' => '/getlocalga/:id'
                ],
                'getFirstPage' => [
                    'action' => 'getFirstPage',
                    'method' => 'GET',
                    'path' => '/getfirstpage/:id'
                ],
                'resizePage' => [
                    'action' => 'resizePage',
                    'method' => 'PUT',
                    'path' => '/resizepage/:id'
                ],
                'addIconFooterPage' => [
                    'action' => 'addIconFooterPage',
                    'method' => 'PUT',
                    'path' => '/addiconfooterpage/:id'
                ],
                'getIconPage' => [
                    'action' => 'getIconPage',
                    'method' => 'GET',
                    'path' => '/geticonpage/:id'
                ],
                'checkExistProduct' => [
                    'action' => 'checkExistProduct',
                    'method' => 'GET',
                    'path' => '/checkexistproduct/:idPage/:corder'
                ],
                'getContentsFromPage' => [
                    'action' => 'getContentsFromPage',
                    'method' => 'GET',
                    'path' => '/getcontentsfrompage/:id'
                ],
                'reorder' => [
                    'action' => 'reorder',
                    'method' => 'POST',
                    'path' => '/reorder'
                ],
                'getContentGroupByPage' => [
                    'action' => 'getContentGroupByPage',
                    'method' => 'GET',
                   'path' => '/getcontentgroupbypage/'
                ],

            ]
        ]);
    });
});

Router::scope('/', function ($routes) {
    $routes->resources('Ags', function ($routes) {
        $routes->resources('Contents', [
            'map' => [
                'saveIconographyProduct' => [
                    'action' => 'saveIconographyProduct',
                    'method' => 'PUT',
                   'path' => '/saveiconographyproduct/:id'
                ],
                'setValueProduct' => [
                    'action' => 'setValueProduct',
                    'method' => 'PUT',
                    'path' => '/setvalueproduct/:id'
                ],
                'setValueAllProduct' => [
                    'action' => 'setValueAllProduct',
                    'method' => 'PUT',
                    'path' => '/setvalueallproduct/:id'
                ],
                'saveOptionalText' => [
                    'action' => 'saveOptionalText',
                    'method' => 'PUT',
                    'path' => '/saveoptionaltext/:id'
                ],
                'combine' => [
                    'action' => 'combine',
                    'method' => 'POST',
                    'path' => '/combine'
                ],
            ]
        ]);
    });
});



Router::scope('/', function ($routes) {
    $routes->resources('Pages', [
        'map' => [
            'addPage' => [
                'action' => 'addPage',
                'method' => 'POST',
               'path' => '/addpage/'
            ],
            'getPages' => [
                'action' => 'getPages',
                'method' => 'GET',
               'path' => '/getpages/:id'
            ],
            'getPageWithProduct' => [
                'action' => 'getPageWithProduct',
                'method' => 'GET',
               'path' => '/getpagewithproduct/:id'
            ],
            'addSquareStrategy' => [
                'action' => 'addSquareStrategy',
                'method' => 'POST',
               'path' => '/addsquarestrategy/'
            ],
            'addSubtitleStrategy' => [
                'action' => 'addSubtitleStrategy',
                'method' => 'POST',
               'path' => '/addsubtitlestrategy/'
            ],
            
        ]
    ]);
});
