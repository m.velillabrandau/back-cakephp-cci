<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'token']);

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->Users->find('all')
        ->where(['active' => 1]);
        $this->response->type('json');
        $this->response->body(json_encode($users));
        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->response->type('json');
        $this->response->body(json_encode($user));
        return $this->response;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->response->type('json');
            $user = $this->Users->newEntity();
            $user->username = $this->request->getData('username');
            $user->password = $this->request->getData('password');
            $user->active = 1;
            

            if ($this->Users->save($user)) {
                $user = [
                    'data' => $user,
                    'success' => true
                ];

                $this->response->body(json_encode($user));
            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(400);
            }
            return $this->response;
        }
    }

    /**
     * Edit method
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id);
        // pr($this->request->getData('username'));
        // pr($this->request->getData('pass'));
        $user->username = $this->request->getData('username');
        if($this->request->getData('pass') != null){
            $user->password = $this->request->getData('pass');
        }
        $this->response->type('json');
        if($this->Users->save($user)) {
            $this->response->body(json_encode($user));
        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }
        return $this->response;
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->response->type('json');
        $user = $this->Users->get($id);
        $user->active = 0;
        if($this->Users->save($user)) {
            $this->response->body(json_encode($user));
        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }
        return $this->response;
    }

    public function token()
    {
        $this->response->type('json');
        $user = $this->Auth->identify();
        
        if (!$user) {
            throw new UnauthorizedException(__('Invalid username or password'));
        }

        $this->set([
            'success' => true,
            'data' => [
                'token' => JWT::encode([
                    'sub' => $user['id'],
                    // 'exp' =>  time() + 3600 //se quita el tiempo de expiracion del token
                ],
                Security::salt())
            ],
            '_serialize' => ['success', 'data']
        ]);
    }
}
