<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ferias Controller
 *
 *
 * @method \App\Model\Entity\Feria[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FeriasController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Casaideas');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $ferias = $this->Casaideas->getFerias();
        $this->response->type('json');
        $this->response->body(json_encode($ferias));
        return $this->response;
    }
}
