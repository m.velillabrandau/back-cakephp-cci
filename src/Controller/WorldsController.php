<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WorldsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Casaideas');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $idMagazine = $this->request->getParam('magazine_id');
        if(!empty($this->request->getParam('magazine_id'))){
            $worlds = $this->Worlds->find()
                ->where([
                    'Worlds.magazines_id' => $idMagazine,
                    'Worlds.active'=>1
                ]);
            $this->response->type('json');
            $this->response->body(json_encode($worlds));

        }
        return $this->response;
    }

    public function getByFeria($id){
        $worlds = $this->Casaideas->getMundosByFeriaId($id);
        $this->response->type('json');
        $this->response->body(json_encode($worlds));
        return $this->response;
    }

    //Obtiene los mundos locales en base a un id de catálogo
    public function getLocalWorlds($id) {
        if(isset($id) && !empty($id)) {
            $worlds = $this->Worlds->find()
            ->order(['w_order ASC'])
            ->where([
                'Worlds.magazines_id' => $id,
                'Worlds.active'=>1
            ]);


        } else {
            $worlds = "wrong parameters";
            $this->response->statusCode(400);
        }
        $this->response->type('json');
        $this->response->body(json_encode($worlds));
        return $this->response;
    }
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        // pr("Estoy en view worlds");exit();

        $magazine = $this->Worlds->get($id);

        $this->response->type('json');
        $this->response->body(json_encode($magazine));
        return $this->response;
    }

    // public function edit($id)
    // {
    //     pr("ESTOY EN EDIT");
    //     pr($id);
    //     pr($this->request->getData());exit();

    //     $world = $this->Worlds->get($id);
    //     if ($this->request->is(['post', 'put'])) {
    //         $world = $this->Worlds->patchEntity($world, $this->request->getData());
    //         pr($world);exit();
    //         if ($this->Worlds->save($world)) {
    //             $message = 'Saved';
    //             $this->response->type('json');
    //     $this->response->body(json_encode($magazine));
    //         } else {
    //             $message = 'Error';
    //             $this->response->body(json_encode('error'));
    //         }
    //     }
    //     return $this->response;
    // }

    //Recibe el id del mundo y guarda el mundo y los ga en la base de datos local del proyecto. Necesita el id del catálogo
    public function saveWorldStructure() {
        $result = 'error';
        if ($this->request->is('post')) {
            //Se crea un mundo
            $world = $this->Worlds->newEntity();
            $data = json_decode($this->request->getData('data'));
            $idWorld = explode("-", $data->idWorld);
            $world->nivel1_id = $idWorld[0];
            $world->nivel2_id = $idWorld[1];
            $world->wld_alias = $data->name;
            $world->is_checked = 1;
            $world->active = 1;
            $world->with_cover = 1;
            $world->magazines_id = $data->id_category;
            $result = $world;
            // Antes de guardar se busca el orden
            $worldsOrder = $this->Worlds->find()
                ->Where(['magazines_id' => $data->id_category, 'active' => 1])
                ->order(['w_order DESC'])
                ->first();
            if(empty($worldsOrder)) {
                $world->w_order = 1;
            } else {
                $world->w_order = $worldsOrder->w_order + 1;
            }
            if($this->Worlds->save($world)) {
                $result = $world->id;

                $updateMagazine = $this->Casaideas->updateLastModification($data->id_category);

            } else {
                $this->response->statusCode(500);
            }
            $this->response->type('json');
            $this->response->body(json_encode($result));
        }
        return $this->response;
    }
    // Elimina un mundo guardado de forma local
    public function deleteWorldStructure() {
        if ($this->request->is('post')) {
            $this->response->type('json');
            $idWorld = explode("-", $this->request->getData('idworld'));
            $catalogue = $this->request->getData('idCatalogue');
            $world = $this->Worlds->find()
                ->where([
                    'active' => 1,
                    'nivel1_id' => $idWorld[0],
                    'nivel2_id' => $idWorld[1],
                    'magazines_id' => $catalogue
            ])->first();
            $world->active = 0;
            // Se reordenan los activos

            if($this->Worlds->save($world)) {

                $updateMagazine = $this->Casaideas->updateLastModification($world->magazines_id);

                $worldsOrder = $this->Worlds->find()
                    ->Where(['magazines_id' => $catalogue, 'active' => 1])
                    ->order(['w_order ASC']);
                $count = 1;
                foreach ($worldsOrder as $worldOrder) {
                    $worldOrder->w_order = $count;
                    $this->Worlds->save($worldOrder);
                    $count ++;
                }
                $this->response->body(json_encode('success'));
            } else {
                $this->response->body(json_encode('error'));
            }

            return $this->response;
        }
    }

    public function editCover($id)
    {

        $world = $this->Worlds->get($id);
        if ($this->request->is(['post', 'put'])) {
            $world->with_cover = $this->request->getData('with_cover');
            if ($this->Worlds->save($world)) {

                $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

                if($updateMagazine){
                    $this->response->type('json');
                    $this->response->body(json_encode($world));
                }else{
                    $this->response->body(json_encode('error'));
                }
            } else {

                $this->response->body(json_encode('error'));
            }
        }
        return $this->response;
    }
    // Reordena los mundos cuando se cambia el orden
    public function reorder() {
        if ($this->request->is('post')) {
            $this->response->type('json');
            $idCat = $this->request->getData('idCat');
            $idWorld = $this->request->getData('idWorld');
            $position = $this->request->getData('position');
            $prevPosition =  $this->request->getData('prevPosition');
            // Se listan los mundos y se recorren para reordenar
            $toOrder = $this->Worlds->get($idWorld);
            $worlds = $this->Worlds->find()
                ->where([
                    'active' => 1,
                    'magazines_id'=> $idCat
                ])
                ->order(['w_order ASC'])->toArray();
            $arr1 = [];
            $arr3 = [];
            $arrFinal = [];
            if($position < $prevPosition) {
                // Sube
                foreach ($worlds as $key => $world) {
                    if($world->id != $idWorld) {
                        if($key < $position) {

                            array_push($arr1, $world);
                        }
                        if($key >= $position) {
                            array_push($arr3, $world);
                        }
                    }
                }
            } else {
                // Baja
                foreach ($worlds as $key => $world) {
                    if($world->id != $idWorld) {
                        if($key <= $position) {

                            array_push($arr1, $world);
                        }
                        if($key > $position) {
                            array_push($arr3, $world);
                        }
                    }
                }
            }

            foreach ($arr1 as $key => $arr1) {
               array_push($arrFinal, $arr1);
            }
            array_push($arrFinal, $toOrder);
            foreach ($arr3 as $key => $arr3) {
                array_push($arrFinal, $arr3);
             }
            foreach($arrFinal as $keyfinal => $worldSave) {
                $worldSave->w_order = $keyfinal +1;
                $this->Worlds->save($worldSave);
            }

            $this->response->body(json_encode($arrFinal));
            return $this->response;
        }
    }


    /**
     * [changeAlias Función para registrar cambios en el alias de la pagina en el mundo]
     * @return [json] [respuesta ajax]
     * @author [Fernando Molina]
     */
    public function changeAlias(){

        if($this->request->is('put')){
            $this->response->type('json');

            if (null !== $this->request->getData('world_id') && null !== $this->request->getData('world_alias')){
                $idWorld = $this->request->getData('world_id');
                $textInput = $this->request->getData('world_alias');
                if($idWorld!=''){
                    $world = $this->Worlds->find()->where(['Worlds.active' => 1, 'Worlds.id' => $idWorld])->first();
                    $world->wld_alias = $textInput;
                    if($this->Worlds->save($world)){

                        $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

                        if($updateMagazine){
                            $response = [
                                'status'=>true,
                                'msn'=>'AJAX OK'
                            ];
                        }else{
                            $response = [
                                'status'=>false,
                                'msn'=>'Error al guardar los datos'
                            ];
                        }

                    }else{
                        $response = [
                            'status'=>false,
                            'msn'=>'Error al guardar los datos'
                        ];
                    }
                }else{
                    $response = [
                        'status'=>false,
                        'msn'=>'id de entrada se encuentra vació'
                    ];
                }
            }else{
                $response = [
                    'status'=>false,
                    'msn'=>'Datos de entrada vacíos'
                ];
            }
        }else{
            $response = [
                'status'=>false,
                'msn'=>'No permitido / ERROR AJAX'
            ];
        }
        $response = $this->response->withType("application/json")->withStringBody(json_encode($response));
        return $response;
    }

}
