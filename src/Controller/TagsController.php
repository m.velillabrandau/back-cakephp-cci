<?php
namespace App\Controller;

use App\Controller\AppController;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TagsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tags = $this->Tags->find('all')
            ->where(['active' => 1]);
        $this->response->type('json');
        $this->response->body(json_encode($tags));
        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        // exit();
        $tags = $this->Tags->get($id);
        $this->response->type('json');
        $this->response->body(json_encode($tags));
        return $this->response;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $tag = $this->Tags->newEntity($this->request->getData());
        $tag->active = 1;
        $this->response->type('json');
        if ($this->Tags->save($tag)) {
            $this->response->body(json_encode($tag));
        } else {
            $this->response->body(json_encode('error'));
        }
        return $this->response;
    }

    /**
     * Edit method
     */
    public function edit($id = null)
    {

        // $this->response->type('json');
        // pr($this->request->getData());
        // pr("En el Back");
        // // 

        // $tag = $this->Tags->get($id);
        // //pr($tag);
        // $path = "../webroot/img/icon_4x/".$tag->img_url."";

        // $tag = $this->Tags->patchEntity($tag, $this->request->getData());

        // pr($tag);

        // if ($this->Tags->save($tag)) {
        //     if(file_exists($path)){
        //         if(unlink($path)){ //Borro archivo antiguo
        //             $this->response->body(json_encode($tag));
        //         }else{
        //             $this->response->body(json_encode('error'));
        //             $this->response->statusCode(500);
        //         }  
        //     }else{
        //         $this->response->body(json_encode('error'));
        //         $this->response->statusCode(500);
        //     }
        // } else {
        //     $this->response->body(json_encode('error'));
        //     $this->response->statusCode(500);
        // }

        // return $this->response;

    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->response->type('json');
        $tag = $this->Tags->get($id);
        $path = "../webroot/img/icon_4x/".$tag->img_url."";
        // pr($path);exit();
        if(file_exists($path)){
            if(unlink($path)){
                $tag->active = 0;
                if($this->Tags->save($tag)) {
                    $this->response->body(json_encode($tag));
                } else {
                    $this->response->body(json_encode('error'));
                    $this->response->statusCode(500);
                }
            }else{
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
            
        }else{
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }
        
        return $this->response;
    }

}
