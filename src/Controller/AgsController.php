<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AgsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Casaideas');
        $this->loadModel('Magazines');
        $this->loadModel('Worlds');
    }

    public function getGas() {
        // $nivel1 = false, $nivel2 = false, $ferid = false
        $nivel1 = '10000';
        $nivel2 = '10000';
        $ferid = '56';
        $ag = $this->Casaideas->getGrupoAnalisisProducto($nivel1, $nivel2, $ferid);
        $this->response->type('json');
        $this->response->body(json_encode($ag));
        return $this->response;
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $idWorld = $this->request->getParam('world_id');
        if(!empty($this->request->getParam('world_id'))){
            $ag = $this->Magazines->Worlds->AnalysisGroups->find()
                ->order(['ag_order ASC'])
                ->where([
                    'AnalysisGroups.worlds_id' => $idWorld,
                    'AnalysisGroups.active'=>1
                ]);
            $this->response->type('json');
            $this->response->body(json_encode($ag));
        }else{
            $ag = $this->Magazines->Worlds->AnalysisGroups->find('all');
            $this->response->type('json');
            $this->response->body(json_encode($ag));
        }
        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $ag = $this->Magazines->Worlds->AnalysisGroups->get($id);
        $this->response->type('json');
        $this->response->body(json_encode($ag));
        return $this->response;
    }

    // Carga un ga de forma local
    public function saveLocalGa() {
        $result = 'error';
        if ($this->request->is('post')) {
            $this->response->type('json');
            $id = $this->request->getData('id');
            $name = $this->request->getData('name');
            $idWorld = explode('-', $this->request->getData('idWorld'));
            $idCatalogue = $this->request->getData('idCatalogue');

            $world = $this->Magazines->Worlds->find()
                ->where([
                    'active' => 1,
                    'nivel1_id'=> $idWorld[0],
                    'nivel2_id'=> $idWorld[1],
                    'magazines_id' => $idCatalogue
                ])->first();
            if(empty($world)) {
            //El mundo no está cargado en el catálogo
                $result = 'No existe mundo';
            } else {
                $ga = $this->Magazines->Worlds->AnalysisGroups->newEntity();
                $ga->grupoanalisis_id = $id;
                $ga->ag_alias = $name;
                $ga->active = 1;
                $ga->worlds_id = $world->id;
                $ga->is_checked = 1;

                $gaOrder = $this->Worlds->AnalysisGroups->find()
                    ->Where(['worlds_id' => $world->id, 'active' => 1])
                    ->order(['ag_order DESC'])
                    ->first();
                if(empty($gaOrder)) {
                    $ga->ag_order = 1;
                } else {
                    $ga->ag_order = $gaOrder->ag_order + 1;
                }

                if($this->Magazines->Worlds->AnalysisGroups->save($ga)) {
                    $result = $ga->id;
                    //se debe crear la página por defecto
                    $page = $this->Magazines->Worlds->AnalysisGroups->GaPages->newEntity();
                    $page->active = 1;
                    $page->analysis_groups_id = $ga->id;
                    $page->x_value = 5;
                    $page->y_value = 5;
                    $page->page_order = 1;

                    if($this->Magazines->Worlds->AnalysisGroups->GaPages->save($page)){
                        $updateMagazine = $this->Casaideas->updateLastModification($idCatalogue);
                    }

                    
                } else {
                    $result = 'error';
                    $this->response->statusCode(500);
                }

            }
        }
        $this->response->body(json_encode($result));
        return $this->response;
    }
     // Obtiene los ga locales en base a un id de mundo y catálogo PENDIENTE
     public function getLocalGa($idWorld) {
        if(isset($idWorld) && !empty($idWorld)) {
            $gas = $this->Worlds->AnalysisGroups->find()
            ->order(['ag_order ASC'])
            ->where([
                'worlds_id' => $idWorld,
                'active'=>1
            ]);
        } else {
            $gas = "wrong parameters";
            $this->response->statusCode(400);
        }
        $this->response->type('json');
        $this->response->body(json_encode($gas));
        return $this->response;
    }

    //Elimina gas locales del catálogo
    public function delete($id = null)
    {
        if ($this->request->is('delete')) {
            $this->response->type('json');
            $ga = $this->Worlds->AnalysisGroups->get($id);
            $ga->active = 0;
            if($this->Worlds->AnalysisGroups->save($ga)) {

                $magazine = $this->Worlds->AnalysisGroups->find()
                ->where([
                        'AnalysisGroups.id' => $id,
                    ])
                ->contain([
                        'Worlds'=>[
                            'conditions'=>[
                                'Worlds.active' => 1,
                                'Worlds.id' =>$ga->worlds_id,
                            ]
                        ]
                    ])
                ->first();

                $updateMagazine = $this->Casaideas->updateLastModification($magazine['world']->magazines_id);

                if($updateMagazine){
                    $this->response->body(json_encode($ga));
                }else{
                    $this->response->body(json_encode('error'));
                    $this->response->statusCode(500);
                }

            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
            return $this->response;
        }
    }

    // Obtiene la primera página según el  orden
      public function getFirstPage($idLocalGa) {
        $this->loadModel('GaPages');
        $page = $this->GaPages->find()
            ->where(['active' => 1, 'analysis_groups_id' => $idLocalGa])
            ->order(['page_order'])
            ->first();
        $this->response->type('json');
        $this->response->body(json_encode($page));
        return $this->response;
    }
    // Modifica una dimensión de página
    public function resizePage($idPage) {
        $response = 'error';
        if ($this->request->is('put')) {
            $this->loadModel('GaPages');
            $this->response->type('json');
            $page = $this->GaPages->get($idPage);
            $page->x_value = $this->request->getData('x');
            $page->y_value = $this->request->getData('y');
            if($this->GaPages->save($page)) {
                $response = $page->analysis_groups_id;
                //Se deben desactivar los productos que exedan la grilla si hubiese
                $maxorder = $this->request->getData('x') * $this->request->getData('y');
                $this->loadModel('Contents');
                $contents = $this->Contents->find()
                    ->where(['active' => 1, 'ga_pages_id' => $idPage, 'c_order >' => $maxorder]);
                foreach ($contents as $content) {
                   $content->active = 0;
                   $this->Contents->save($content);
                }

                $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

                if($updateMagazine){
                    $response = $page->analysis_groups_id;
                }else{
                    $this->response->statusCode(500);
                }

            } else {
                $this->response->statusCode(500);
            }
        }
        $this->response->body(json_encode($response));
        return $this->response;
    }
    // Agrega un icono al pie de pagina
    public function addIconFooterPage($idPage){

        $this->loadModel('GaPages');
        $page = $this->GaPages->find()
            ->where(['active' => 1, 'id' => $idPage])
            ->first();
        $page->tags_id = $this->request->getData('idTag');

        if ($this->GaPages->save($page)) {

            $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

            if($updateMagazine){
                $this->response->type('json');
                $this->response->body(json_encode($page));
            }else{
                $this->response->body(json_encode('error'));
            }

        } else {
            $this->response->body(json_encode('error'));
        }
        return $this->response;
    }

     // Trae el Id del tag utlizado en la pagina
    public function getIconPage($idPage = null){
        $this->loadModel('GaPages');

        $page = $this->GaPages->find()
            ->where(['active' => 1, 'id' => $idPage])
            ->first();

        if ($page != null) {
            $this->response->type('json');
            $this->response->body(json_encode($page));
        } else {
            $this->response->body(json_encode('error'));
        }
        return $this->response;
    }
    // Check si existe un producto en una casilla de una página
    public function checkExistProduct($idPage, $corder) {
        $this->loadModel('Contents');
        $content = $this->Contents->find()
            ->where(['active' => 1, 'ga_pages_id' => $idPage, 'c_order' => $corder ]);
            // ->first();
        $this->response->type('json');
        if(empty($content)) {
            $this->response->body(json_encode('0'));
        } else {
            $this->response->body(json_encode($content));
        }

        return $this->response;
    }

    // Trae todos los productos de una página
    public function getContentsFromPage($id) {
        $this->loadModel('Contents');
        $contents = $this->Contents->find()
            ->where(['active' => 1, 'ga_pages_id' => $id ]);
        $this->response->type('json');
        $this->response->body(json_encode($contents));
        return $this->response;
    }
    public function reorder() {
        if ($this->request->is('post')) {
            $this->response->type('json');
            $idGa = $this->request->getData('idGa');
            $idWorld = $this->request->getData('idWorld');
            $position = $this->request->getData('position');
            $prevPosition =  $this->request->getData('prevPosition');
            // Se listan los ga y se recorren para reordenar
            $toOrder = $this->Worlds->AnalysisGroups->get($idGa);
            $gas = $this->Worlds->AnalysisGroups->find()
                ->where([
                    'active' => 1,
                    'worlds_id'=> $idWorld
                ])
                ->order(['ag_order ASC'])->toArray();
            $arr1 = [];
            $arr3 = [];
            $arrFinal = [];
            if($position < $prevPosition) {
                // Sube
                foreach ($gas as $key => $ga) {
                    if($ga->id != $idGa) {
                        if($key < $position) {

                            array_push($arr1, $ga);
                        }
                        if($key >= $position) {
                            array_push($arr3, $ga);
                        }
                    }
                }
            } else {
                // Baja
                foreach ($gas as $key => $ga) {
                    if($ga->id != $idGa) {
                        if($key <= $position) {

                            array_push($arr1, $ga);
                        }
                        if($key > $position) {
                            array_push($arr3, $ga);
                        }
                    }
                }
            }

            foreach ($arr1 as $key => $arr1) {
               array_push($arrFinal, $arr1);
            }
            array_push($arrFinal, $toOrder);
            foreach ($arr3 as $key => $arr3) {
                array_push($arrFinal, $arr3);
             }
            foreach($arrFinal as $keyfinal => $gaSave) {
                $gaSave->ag_order = $keyfinal +1;
                $this->Worlds->AnalysisGroups->save($gaSave);
            }

            $this->response->body(json_encode('success'));
            return $this->response;
        }
    }

    //Trae los contents group de una pagina en especifico
    public function getContentGroupByPage()
    {
        // pr($this->request->getQuery('idPage'));exit();

        $this->loadModel('ContentGroups');

        $currentContentGroup = $this->ContentGroups->find()
            ->where([
                'active' => 1,
                'ga_page_id' => $this->request->getQuery('idPage')
                ]);

        $this->response->type('json');
        $this->response->body(json_encode($currentContentGroup));
        return $this->response;
    }
}
