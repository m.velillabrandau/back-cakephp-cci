<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MagazinesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Casaideas');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $magazines = $this->Magazines->find()
            ->where([
                'Magazines.active' => 1
            ])->order(['Magazines.id' => 'DESC']);
        $this->response->type('json');
        $this->response->body(json_encode($magazines));
        return $this->response;
    }

    public function magazineContent($id) {
        $magazine = $this->Casaideas->getMagazineContent($id);
        $this->response->type('json');
        $this->response->body(json_encode($magazine));
        return $this->response;
    }
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $magazine = $this->Magazines->get($id);
        $this->response->type('json');
        $this->response->body(json_encode($magazine));
        return $this->response;
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $magazine = $this->Magazines->newEntity();
            $magazine->active = 1;
            $magazine->ferias_id = $this->request->getData('ferias_id');
            $magazine->feria_alias = $this->request->getData('feria_alias');
            if ($this->Magazines->save($magazine)) {
                $this->response->body(json_encode($magazine));
            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
            return $this->response;
        }
    }

    public function edit($id = null)
    {
        $this->response->type('json');
        $magazine = $this->Magazines->get($id);
        if ($this->request->is(['put'])) {
            $magazine->feria_alias = $this->request->getData('feria_alias');
            $magazine->ferias_id = $this->request->getData('ferias_id');
            if ($this->Magazines->save($magazine)) {
                $this->response->body(json_encode($magazine));
            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
        }else{
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }
        return $this->response;
    }

    public function delete($id = null)
    {
        $this->response->type('json');
        $magazine = $this->Magazines->get($id);
        $magazine->active = 0;
        if($this->Magazines->save($magazine)) {
            $this->response->body(json_encode($magazine));
        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }
        return $this->response;
    }
    /**
     * permite ver el catalogo creado
     * @param  [type] $magazineId [description]
     * @return [type]             [description]
     */
    public function viewPdf($magazineId = null) {
        $magazine = $this->Magazines->find()
        ->where(['Magazines.id' => $magazineId,'Magazines.active' => 1])
        ->contain([
            'Worlds' => function ($q) {
                return $q->where([
                    'Worlds.active' => 1,
                    'Worlds.is_checked' => 1
                ])
                ->order([
                    'Worlds.w_order' => 'ASC'
                ]);
            },
            'Worlds.AnalysisGroups' => function ($q) {
                return $q->where([
                    'AnalysisGroups.active' => 1,
                    'AnalysisGroups.is_checked' => 1
                ])
                ->order([
                    'AnalysisGroups.ag_order' => 'ASC'
                ]);
            },
            'Worlds.AnalysisGroups.GaPages' => function ($q) {
                return $q->where([
                    'GaPages.active' => 1
                ])
                ->order([
                    'GaPages.page_order' => 'ASC'
                ]);
            },
            'Worlds.AnalysisGroups.GaPages.ContentGroups' => function ($q) {
                return $q->where([
                    'ContentGroups.active' => 1
                ]);
            },
            'Worlds.AnalysisGroups.GaPages.Tags' => function ($q) {
                return $q->where([
                    'Tags.active' => 1
                ]);
            },
            'Worlds.AnalysisGroups.GaPages.Contents' => function ($q) {
                return $q->where([
                    'Contents.active' => 1
                ])
                ->order([
                    'Contents.c_order' => 'ASC'
                ]);
            },
            'Worlds.AnalysisGroups.GaPages.Contents.Childrens' => function ($q) {
                return $q->where([
                    'Childrens.active' => 1
                ])
                ->order([
                    'Childrens.parent_order' => 'ASC'
                ]);
            },
            'Worlds.AnalysisGroups.GaPages.Contents.Tags' => function ($q) {
                return $q->where([
                    'Tags.active' => 1
                ]);
            }
        ])->first();

        

        if(empty($magazine)){
            $this->Flash->error('No se pudo encontro el catálogo deseado, intente nuevamente.');
            return $this->Redirect(['controller' => 'Magazines', 'action' => 'index']);
        } else {

            $this->set(compact('magazine'));
        }

    }

}
