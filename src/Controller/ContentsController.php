<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Magazines');
        $this->loadModel('Worlds');
        $this->loadModel('AnalysisGroups');
        $this->loadComponent('Casaideas');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $idAg = $this->request->getParam('ag_id');
        if(!empty($this->request->getParam('ag_id'))){

            pr("Existe");
            $contents = $this->Contents->find()
                ->where([
                    'Contents.analysis_groups_id' => $idAg,
                    'Contents.active'=>1
                ]);

            $this->response->type('json');
            $this->response->body(json_encode($contents));
        }else{
            $contents = $this->Contents->find('all');
            $this->response->type('json');
            $this->response->body(json_encode($contents));
        }
        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $contents = $this->Contents->get($id);
        $this->response->type('json');
        $this->response->body(json_encode($contents));
        return $this->response;
    }
    // Agrega contenidos
    public function add() {

        if ($this->request->is('post')) {

            $this->response->type('json');
            $content = $this->Contents->newEntity();
            $content->active = 1;
            $content->sku = $this->request->getData('sku');
            $content->analysis_groups_id = $this->request->getData('localGa');
            $content->c_order = $this->request->getData('corder');
            $content->ga_pages_id = $this->request->getData('idPage');
            $content->madre_id = $this->request->getData('madreid');
            $content->etariocod = $this->request->getData('etariocod');
            $content->codn3 = $this->request->getData('codn3');
            $content->codn4 = $this->request->getData('codn4');
            // Antes de guardar el contenido, se debe desactivar si existe uno existente, esto ocurre cuando toma una targeta de la grilla y la mueve a otra posición dentro de la grilla
            $existentContent = $this->Contents->find()
                ->where([
                    'ga_pages_id' => $this->request->getData('idPage'),
                    'sku' => $this->request->getData('sku'),
                    'active' => 1
                    ])
                ->first();
            if(!empty($existentContent)) {
                $existentContent->active = 0;
                $this->Contents->save($existentContent);
            }
            if($this->Contents->save($content)) {
                if(!empty($existentContent)) {
                    //Se debe mover también a los hijos
                    $childs = $this->Contents->find()
                    ->where([
                        'content_parent_id' => $existentContent->id,
                        'active' => 1
                    ]);
                    foreach ($childs as $child) {
                        $child->content_parent_id = $content->id;
                        $child->c_order = $this->request->getData('corder');
                        $this->Contents->save($child);
                    }
                }

                $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('idCat'));
                if($updateMagazine){
                    $this->response->body(json_encode($content));
                }else{
                    $this->response->body(json_encode('error'));
                    $this->response->statusCode(500);
                }

            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
            return $this->response;
        }
    }

    public function saveIconographyProduct($id){

        $this->response->type('json');
        // pr($id);
        //pr($this->request);
        $content = $this->Contents->find()
            ->where([
                    'Contents.ga_pages_id' => $this->request->getData('pageId'),
                    'Contents.active'=>1,
                    'Contents.sku'=>$id
                ])
            ->first();

        if($this->request->getData('tagId') == 0){
            $content->tags_id = null;
        }else{
           $content->tags_id = $this->request->getData('tagId');
        }



        if ($this->Contents->save($content)) {

            $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

            if($updateMagazine){
                $this->response->type('json');
                $this->response->body(json_encode($content));
            }else{
                $this->response->body(json_encode('error'));
            }

        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }


        return $this->response;
    }

    //Guarda las propiedades del producto (MENOS; MAS; NUEVO;)
    public function setValueProduct($id){

        $this->response->type('json');
        // pr($id);
        // pr($this->request);
        $content = $this->Contents->find()
            ->where([
                    'Contents.ga_pages_id' => $this->request->getData('pageId'),
                    'Contents.active'=>1,
                    'Contents.sku'=>$id
                ])
            ->first();

        switch ($this->request->getData('typeValue')) {
            //PRODUCTO NUEVO
            case 0:
                if($content->is_new == 1){
                    $content->is_new = 0;
                }else{
                    $content->is_new = 1;
                }
            break;
            //ETIQUETAPOSITIVA
            case 1:
                if($content->pp_symbol == 1){
                    $content->pp_symbol = 0;
                }else{
                    $content->pp_symbol = 1;
                }
            break;
            //ETIQUETA NEGATIVA
            case 2:
                if($content->pp_symbol == 2){
                    $content->pp_symbol = 0;
                }else{
                    $content->pp_symbol = 2;
                }
            break;
        }


        if ($this->Contents->save($content)) {

            $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

            if($updateMagazine){
                $this->response->type('json');
                $this->response->body(json_encode($content));
            }else{
                $this->response->body(json_encode('error'));
            }

        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }

        return $this->response;
    }

    //Guarda las propiedades del producto (MENOS; MAS; NUEVO;)
    public function setValueAllProduct($id){

        $this->response->type('json');
        

        $fatherContent = $this->Contents->get($id);

        $sonsContents = $this->Contents->find()
            ->where([
                    'Contents.content_parent_id' => $id,
                    'Contents.active'=>1,
                ]);

        // pr($id);
        // pr($this->request);
        // pr($fatherContent);
        // pr($sonsContents);
        // exit();

        switch ($this->request->getData('typeValue')) {
            //PRODUCTO NUEVO
            case 0:

                if($fatherContent->is_new == 1){

                    $fatherContent->is_new = 0;

                    foreach ($sonsContents as $key => $sonContent) {

                        $sonContent->is_new = 0;

                        if ($this->Contents->save($sonContent)) {

                        }else{
                            $this->response->body(json_encode('error'));
                            $this->response->statusCode(500);
                            return $this->response;
                        }

                    }
                }else{
                    $fatherContent->is_new = 1;

                    foreach ($sonsContents as $key => $sonContent) {

                        $sonContent->is_new = 1;

                        if ($this->Contents->save($sonContent)) {

                        }else{
                            $this->response->body(json_encode('error'));
                            $this->response->statusCode(500);
                            return $this->response;
                        }

                    }
                } 
            break;
            //ETIQUETAPOSITIVA
            case 1:

                if($fatherContent->pp_symbol == 1){

                    $fatherContent->pp_symbol = 0;

                    foreach ($sonsContents as $key => $sonContent) {

                        $sonContent->pp_symbol = 0;

                        if ($this->Contents->save($sonContent)) {

                        }else{
                            $this->response->body(json_encode('error'));
                            $this->response->statusCode(500);
                            return $this->response;
                        }

                    }
                }else{
                    $fatherContent->pp_symbol = 1;

                    foreach ($sonsContents as $key => $sonContent) {

                        $sonContent->pp_symbol = 1;

                        if ($this->Contents->save($sonContent)) {

                        }else{
                            $this->response->body(json_encode('error'));
                            $this->response->statusCode(500);
                            return $this->response;
                        }

                    }
                } 

            break;
            //ETIQUETA NEGATIVA
            case 2:

                if($fatherContent->pp_symbol == 2){

                    $fatherContent->pp_symbol = 0;

                    foreach ($sonsContents as $key => $sonContent) {

                        $sonContent->pp_symbol = 0;

                        if ($this->Contents->save($sonContent)) {

                        }else{
                            $this->response->body(json_encode('error'));
                            $this->response->statusCode(500);
                            return $this->response;
                        }

                    }
                }else{
                    $fatherContent->pp_symbol = 2;

                    foreach ($sonsContents as $key => $sonContent) {

                        $sonContent->pp_symbol = 2;

                        if ($this->Contents->save($sonContent)) {

                        }else{
                            $this->response->body(json_encode('error'));
                            $this->response->statusCode(500);
                            return $this->response;
                        }

                    }
                } 
            break;
        }


        if ($this->Contents->save($fatherContent)) {

            $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

            if($updateMagazine){
                $this->response->type('json');
                $this->response->body(json_encode($fatherContent));
            }else{
                $this->response->body(json_encode('error'));
            }

        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }

        return $this->response;
    }

    //Guarda elt exto opcional de un producto
    public function saveOptionalText($id){

        $this->response->type('json');

        $content = $this->Contents->find()
            ->where([
                    'Contents.ga_pages_id' => $this->request->getData('pageId'),
                    'Contents.active'=>1,
                    'Contents.sku'=>$id
                ])
            ->first();

        $content->measurements =  $this->request->getData('optionalText');

        if ($this->Contents->save($content)) {

            $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

            if($updateMagazine){
                $this->response->type('json');
                $this->response->body(json_encode($content));
            }else{
                $this->response->body(json_encode('error'));
            }

        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }


        return $this->response;
    }
    public function delete($id) {
        $this->response->type('json');
        $content = $this->Contents->get($id);
        $content->active = 0;
        $allDeleted = [];
        if($this->Contents->save($content)) {
            array_push($allDeleted, $content);
            //Se busca a los hijos si tuviese para eliminarlos
            $children = $this->Contents->find()
                ->where(['active' => 1, 'content_parent_id' => $id]);
            foreach ($children as $child) {
                $child->active = 0;
                $this->Contents->save($child);
                array_push($allDeleted, $child);
            }
            $magazine = $this->Contents->find()
                ->where([
                        'Contents.id' => $id
                    ])
                ->contain([
                        'AnalysisGroups'=>[
                            'conditions'=>[
                                'AnalysisGroups.active' => 1,
                                'AnalysisGroups.id' => $content->analysis_groups_id,
                            ],
                            'Worlds' => [
                                'conditions'=>[
                                    'Worlds.active' => 1
                                ]
                            ]
                        ]
                    ])
                ->first();
            $updateMagazine = $this->Casaideas->updateLastModification($magazine['analysis_group']['world']->magazines_id);

            if($updateMagazine){
                $this->response->type('json');
                $this->response->body(json_encode($allDeleted));
            }else{
                $this->response->body(json_encode('error'));
            }
        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }
        return $this->response;
    }
    public function combine() {
        if ($this->request->is('post')) {
            $this->response->type('json');
            $data = $this->request->getData('body');
            $parent = $this->Contents->find()
                ->order(['parent_order ASC'])
                ->where([
                    'active' => 1,
                    'ga_pages_id' => $data['idPage'],
                    'c_order' => $data['corder']
                ])->first();
            $last = $this->Contents->find()
                ->order(['parent_order DESC'])
                ->where([
                    'active' => 1,
                    'ga_pages_id' => $data['idPage'],
                    'c_order' => $data['corder']
                ])->first();
            $child = $this->Contents->find()
            ->order(['parent_order ASC'])
            ->where([
                'active' => 1,
                'ga_pages_id' => $data['idPage'],
                'sku' => $data['sku']
            ])->first();
            // Validación por si existiese
            $allContentsOrder = $this->Contents->find()
                ->order(['parent_order ASC'])
                ->where([
                    'active' => 1,
                    'ga_pages_id' => $data['idPage'],
                    'c_order' => $data['corder']
                ]);
            $valid = 1;
            foreach ($allContentsOrder as $key => $value) {
               if($value->sku == $data['sku']) {
                   $valid = 0;
               }
            }
            if($valid == 1) {
                if(empty($child)) {
                    //No hay contenido, se crea uno y se guarda
                    $content = $this->Contents->newEntity();
                    $content->active = 1;
                    $content->sku = $data['sku'];
                    $content->analysis_groups_id = $data['localGa'];
                    $content->c_order = $data['corder'];
                    $content->ga_pages_id = $data['idPage'];
                    $content->madre_id = $data['madreid'];
                    $content->etariocod = $data['etariocod'];
                    $content->codn3 = $data['codn3'];
                    $content->codn4 = $data['codn4'];
                    $content->content_parent_id = $parent->id;
                    $content->parent_order = $last->parent_order + 1;
                    $content->c_order =  $data['corder'];

                    if( $this->Contents->save($content) ){
                       $updateMagazine = $this->Casaideas->updateLastModification($data['idCat']);
                    }else{

                    }
                }
                else {
                    $child->content_parent_id = $parent->id;
                    $child->parent_order = $last->parent_order + 1;
                    $child->c_order =  $data['corder'];
                    $this->Contents->save($child);
                }
            }
            $this->response->body(json_encode($child));
            return $this->response;
        }
    }

}
