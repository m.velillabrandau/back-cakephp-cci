<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
/**
 * Casaideas component
 */
class CasaideasComponent extends Component
{

	/**
	 * Default configuration.
	 *
	 * @var array
	 */
	protected $_defaultConfig = [];

	/**
	 * retorna las ferias desde los datos de casaideas
	 * @return object
	 */
	public function getFerias() {
		$connection = ConnectionManager::get('cci_corp');
		$sql   = "SELECT ferid, ferdesc FROM  ferias ORDER BY ferid ASC";
		$query = $connection->prepare($sql);
		$query->execute();
		$result = $query->fetchAll('assoc');

		$arrFerias = [];
		foreach ($result as $key => $value) {
			$arrFerias[$value['ferid']] = $value['ferid'] . ' - ' . $value['ferdesc'];
		}
		return $arrFerias;
	}

	/**
	 * recibe el id de la feria y retorna los mundos asociados a la feria.
	 */
	public function getMundosByFeriaId($ferid = false){
		$connection = ConnectionManager::get('cci_corp');
		$sql   =
		"SELECT `b`.`codn1`, `b`.`codn2`, `c`.`descn1`, `d`.`descn2`
		FROM  `producto` AS a
		LEFT JOIN `madre` AS b ON `a`.`madid` = `b`.`madid`
		LEFT JOIN `nivel1` c ON `c`.`codn1` = `b`.`codn1`
		LEFT JOIN `nivel2` d ON `d`.`codn1` = `b`.`codn1`
		WHERE
		`a`.`prodstatuscomp` = 'S'
		AND
		`d`.`codn2` = `b`.`codn2`
		AND
		`a`.`ferid` = $ferid
		GROUP BY `b`.`codn1`, `b`.`codn2`";
		$query = $connection->prepare($sql);
		$query->execute();
		$result = $query->fetchAll('assoc');

		$arrMundos = [];
		foreach ($result as $key => $value) {
			$arrMundos[$value['codn1'].'-'.$value['codn2']] = $value['descn1'] . ' ' . $value['descn2'];
		}
		return $arrMundos;
	}

	public function getGrupoAnalisisProducto($nivel1 = false, $nivel2 = false, $ferid = false){
		$connection = ConnectionManager::get('cci_corp');
		$sql   = "
		SELECT
			`c`.`ProductoMATNR` ,`a`.`madid`, `a`.`ganid`, `b`.`gandesc`, `a`.`maddesc`
		FROM
			madre AS a
		LEFT JOIN grupoanalisis as b ON `a`.`ganid` = `b`.`ganid`
		LEFT JOIN producto as c ON `a`.`madid` = `c`.`madid`

		WHERE
			`a`.`codn1` = $nivel1
		AND
			`a`.`codn2` = $nivel2
		AND
			`c`.`prodstatuscomp` = 'S'
		AND
			`c`.`ferid` = $ferid
		GROUP BY `a`.`madid`
		";
		$query = $connection->prepare($sql);
		$query->execute();
		$result = $query->fetchAll('assoc');
		$arrGruposAnalisis = [];
		foreach ($result as $key => $value) {
			$arrGruposAnalisis[$value['ganid']]['ga_name'] = $value['gandesc'];
			$arrGruposAnalisis[$value['ganid']]['products'][$value['ProductoMATNR']] = $value['maddesc'];
		}
		return $arrGruposAnalisis;

	}

	public function getMagazineContent($ferid = null){
		$connection = ConnectionManager::get('cci_corp');
		$sql   = "
		SELECT
			`a`.`madid`, `d`.`codn1`, `e`.`codn2`, `d`.`descn1`, `e`.`descn2`, `a`.`ganid`, `b`.`gandesc`, `c`.`ProductoMATNR`, `a`.`maddesc`, `a`.`etariocod`,`a`.`codn3`,`a`.`codn4`
		FROM
			madre AS a
		LEFT JOIN `nivel1` AS d ON `d`.`codn1` = `a`.`codn1`
		LEFT JOIN `nivel2` AS e ON `e`.`codn1` = `a`.`codn1`
		LEFT JOIN grupoanalisis AS b ON `a`.`ganid` = `b`.`ganid`
		LEFT JOIN producto AS c ON `a`.`madid` = `c`.`madid`

		WHERE
			`c`.`prodstatuscomp` = 'S'
		AND
			`c`.`ferid` = $ferid
		AND
			`e`.`codn2` = `a`.`codn2`
		ORDER BY `d`.`codn1` ASC, `e`.`codn2` ASC
		";
		$query = $connection->prepare($sql);
		$query->execute();
		$result = $query->fetchAll('assoc');

		$arrMagazine = [];

		//CREA ARRAY DE MUNDOS
		foreach ($result as $value) {
			$arrMagazine[$value['codn1'].'-'.$value['codn2']] = [
				'world' => [
					'id' => $value['codn1'].'-'.$value['codn2'],
					'name' => $value['descn1'] . ' ' . $value['descn2'],
					'ga' => [
						//$value['ganid'] => []
					]
				]

			];
		}
		// pr($arrMagazine);exit;
		foreach ($result as $value) {
			if (!isset($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']])) {
				$arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']] = [];
			}
			if(isset($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']])){

				$arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']] = [
				'id' => $value['ganid'],
					'name' => $value['gandesc'],
					'products'=> []
				];
			}

		}
		// pr($arrMagazine);exit;
		foreach ($arrMagazine as $mgz) {
			foreach ($mgz['world']['ga'] as $gaKey => $ga) {
				foreach ($result as $value) {
					if($value['ganid'] == $gaKey ){
						$obj = [
							'codn3' => $value['codn3'],
                            'codn4' => $value['codn4'],
                            'color' => $this->getColorProduct($value['ProductoMATNR']),
                            'diseno' => $this->getDisenoProduct($value['ProductoMATNR']),
                            'punta_precio' => $this->getPuntaPrecioProduct($value['ProductoMATNR']),
							'etariocod' => $value['etariocod'],
							'sku' => $value['ProductoMATNR'],
							'name' => $value['maddesc'],
							'madre_id' => (!empty($value['madid']) ? $value['madid'] : '')
						];
						if(!isset($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$gaKey])){
							continue;
						}
						if ($this->in_array_field($value['ProductoMATNR'], 'sku', $arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$gaKey]['products'])){
   							continue;
						}else{
							array_push($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$gaKey]['products'], $obj);
						}
					}
				}
			}
		}
        $arrMagazine = $this->orderGa($arrMagazine);

		return $arrMagazine;
    }
    private function getPuntaPrecioProduct($sku) {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT preciosug FROM  producto WHERE `ProductoMATNR` = '". $sku ."'";
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $puntaPrecio = '';
        foreach ($result as $row) {
            $puntaPrecio = reset($row);
        }
        return $puntaPrecio;
    }
    private function getColorProduct($sku) {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT `colordesc` FROM `colores` AS `a` LEFT JOIN `producto` AS `b` ON `a`.`colorid` = `b`.`colorid` WHERE `b`.`ProductoMATNR` = '". $sku ."'";
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $color = '';
        foreach ($result as $row) {
            $color = reset($row);
        }
        return $color;
    }
    private function getDisenoProduct($sku){
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT `disid` FROM  `producto` WHERE `ProductoMATNR` = '". $sku ."'";
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $diseno = '';
        foreach ($result as $row) {
            $diseno = reset($row);
        }
        return $diseno;
    }
	private function orderGa($arrMagazine){
		//pr('..orderGa');
		//pr($arrMagazine);
		$arrFinal = [];
		foreach ($arrMagazine as $mgzn) {
			$preOutput = [];
			foreach ($mgzn['world']['ga'] as $wrldga) {
				$preOutput[$wrldga['id']] = $wrldga['name'];
			}
			asort($preOutput);
			$arrFinal[$mgzn['world']['id']] = $preOutput;
		}
		//Limpieza de GA
		$arrMagazine2 = $arrMagazine;
		foreach ($arrMagazine2 as &$mgznOut) {
			// foreach ($arrFinal[$mgznOut['world']['id']] as $finalGa) {
			// 	pr($finalGa);
			// }
			unset($mgznOut['world']['ga']);
			//
			//pr('..out');
			//pr($mgznOut);
		}
		//Entran los GA ordenados
		//pr($arrMagazine2);
		foreach ($arrFinal as $keyFinal => $aFinal) {
			foreach ($aFinal as $keyGa => $gaName) {
				$arrMagazine2[$keyFinal]['world']['ga'][$keyGa] = $arrMagazine[$keyFinal]['world']['ga'][$keyGa];
			}
		}
		return $arrMagazine2;

	}

	public function in_array_field($needle, $needle_field, $haystack, $strict = false) {
    if ($strict) {
        foreach ($haystack as $item)
            if (isset($item[$needle_field]) && $item[$needle_field] === $needle)
                return true;
    }
    else {
        foreach ($haystack as $item){
            if (isset($item[$needle_field]) && $item[$needle_field] == $needle)
                return true;
        }
    }
    return false;
}

	/**
	 * retorna los etarios desde los datos de casaideas
	 * @return object
	 */
	public function getEtarios(){
		$connection = ConnectionManager::get('cci_corp');
		$sql   = "SELECT etariocod, etarionom, etariodc FROM  etario ORDER BY etariocod ASC";
		$query = $connection->prepare($sql);
		$query->execute();
		$result = $query->fetchAll('assoc');

		$etarios = [];
		foreach ($result as $key => $value) {
			$etarios[$value['etariocod']] = $value['etarionom'].'';
		}
		return $etarios;
    }
    public function updateLastModification($id) {


    	$this->Magazines = TableRegistry::get('Magazines');
     	$today = date("Y-m-d H:i:s");
        $magazine = $this->Magazines->get($id);
        $magazine->modified = $today;
        if ($this->Magazines->save($magazine)) {
            return true;
        } else {
            return false;
        }
    }
    //SE EVALUARA CONSUlTAR NUEVAMENTE CON ESTE CODIGO SI ES QUE ES LENTO PASAR LA ESTRUCTURA POR EL SERVICIO
    // public function getWorldContent($ferid=null, $id1 = null, $id2 = null){
	// 	$connection = ConnectionManager::get('cci_corp');
	// 	$sql   = "
	// 	SELECT
	// 		`a`.`madid`, `d`.`codn1`, `e`.`codn2`, `d`.`descn1`, `e`.`descn2`, `a`.`ganid`, `b`.`gandesc`, `c`.`ProductoMATNR`, `a`.`maddesc`, `a`.`etariocod`,`a`.`codn3`,`a`.`codn4`
	// 	FROM
	// 		madre AS a
	// 	LEFT JOIN `nivel1` AS d ON `d`.`codn1` = `a`.`codn1`
	// 	LEFT JOIN `nivel2` AS e ON `e`.`codn1` = `a`.`codn1`
	// 	LEFT JOIN grupoanalisis AS b ON `a`.`ganid` = `b`.`ganid`
	// 	LEFT JOIN producto AS c ON `a`.`madid` = `c`.`madid`

	// 	WHERE
	// 		`c`.`prodstatuscomp` = 'S'
	// 	AND
    //         `c`.`ferid` = $ferid
    //     AND
    //         `a`.`codn1` = $id1
    //     AND
    //         `a`.`codn2` = $id2
	// 	AND
	// 		`e`.`codn2` = `a`.`codn2`
	// 	ORDER BY `d`.`codn1` ASC, `e`.`codn2` ASC
	// 	";
	// 	$query = $connection->prepare($sql);
	// 	$query->execute();
	// 	$result = $query->fetchAll('assoc');

	// 	$arrMagazine = [];

	// 	//CREA ARRAY DE MUNDOS
	// 	foreach ($result as $value) {
	// 		$arrMagazine[$value['codn1'].'-'.$value['codn2']] = [
	// 			'world' => [
	// 				'id' => $value['codn1'].'-'.$value['codn2'],
	// 				'name' => $value['descn1'] . ' ' . $value['descn2'],
	// 				'ga' => [
	// 					//$value['ganid'] => []
	// 				]
	// 			]

	// 		];
	// 	}
	// 	// pr($arrMagazine);exit;
	// 	foreach ($result as $value) {
	// 		if (!isset($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']])) {
	// 			$arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']] = [];
	// 		}
	// 		if(isset($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']])){

	// 			$arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$value['ganid']] = [
	// 			'id' => $value['ganid'],
	// 				'name' => $value['gandesc'],
	// 				'products'=> []
	// 			];
	// 		}

	// 	}
	// 	// pr($arrMagazine);exit;
	// 	foreach ($arrMagazine as $mgz) {
	// 		foreach ($mgz['world']['ga'] as $gaKey => $ga) {
	// 			foreach ($result as $value) {
	// 				if($value['ganid'] == $gaKey ){
	// 					$obj = [
	// 						'codn3' => $value['codn3'],
	// 						'codn4' => $value['codn4'],
	// 						'etariocod' => $value['etariocod'],
	// 						'sku' => $value['ProductoMATNR'],
	// 						'name' => $value['maddesc'],
	// 						'madre_id' => (!empty($value['madid']) ? $value['madid'] : '')
	// 					];
	// 					if(!isset($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$gaKey])){
	// 						continue;
	// 					}
	// 					if ($this->in_array_field($value['ProductoMATNR'], 'sku', $arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$gaKey]['products'])){
   	// 						continue;
	// 					}else{
	// 						array_push($arrMagazine[$value['codn1'].'-'.$value['codn2']]['world']['ga'][$gaKey]['products'], $obj);
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	$arrMagazine = $this->orderGa($arrMagazine);
	// 	return $arrMagazine;
	// }

}
