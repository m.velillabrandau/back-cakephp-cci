<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function view($id)
    {
        $this->loadModel('GaPages');

        $page = $this->GaPages->get($id);

        $this->response->type('json');
        $this->response->body(json_encode($page));
        return $this->response;
    }

     /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function delete($id)
    {
        $this->loadModel('GaPages');
        $this->loadModel('Contents');
        $this->loadModel('ContentGroups');
        $this->loadModel('Worlds');
        $this->loadModel('AnalysisGroups');
        $this->loadComponent('Casaideas');

        $this->response->type('json');

        //ELIMINO LOS PRODUCTOS.
        $contents = $this->Contents->find()
            ->where([
                    'Contents.ga_pages_id' => $id
                ]);

        // pr($contents);exit();

        foreach ($contents as $key => $content) {

            $content->active = 0;
            if( $this->Contents->save($content) ){

            }else{
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
                return $this->response;
            }
        }

        //ELIMINO CONTENTS GROUPS
        $contentsGroups = $this->ContentGroups->find()
            ->where([
                    'ContentGroups.ga_page_id' => $id
                ]);

        // pr($contentsGroups);exit();

        foreach ($contentsGroups as $key => $contentGroup) {

            $contentGroup->active = 0;
            if( $this->ContentGroups->save($contentGroup) ){

            }else{
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
                return $this->response;
            }
        }


        //ELIMINO LA PAGINA
        $page = $this->GaPages->get($id);

        //GUARDO ANALYSIS GROUP
        $analysis_groups_id = $page->analysis_groups_id;

        $page->active = 0;

        if( $this->GaPages->save($page) ){

            $magazine = $this->GaPages->find()
                ->where([
                        'GaPages.id' => $id,
                    ])
                ->contain([
                        'AnalysisGroups'=>[
                            'conditions'=>[
                                'AnalysisGroups.active' => 1,
                                'AnalysisGroups.id' => $analysis_groups_id,
                            ],
                            'Worlds' => [
                                'conditions'=>[
                                    'Worlds.active' => 1
                                ]
                            ]
                        ]
                    ])
                ->first();

            $updateMagazine = $this->Casaideas->updateLastModification($magazine['analysis_group']['world']->magazines_id);

            if($updateMagazine){

            }else{
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
                return $this->response;
            }


        }else{
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
            return $this->response;
        }


        //REORGANIZO PAGINAS
        $pages = $this->GaPages->find()
            ->where([
                'GaPages.analysis_groups_id' => $analysis_groups_id,
                'GaPages.active' => 1
            ])
            ->order([
                'GaPages.page_order' => 'ASC'
            ]);

        $i = 1;
        foreach ($pages as $key => $page) {
            $page->page_order = $i;

            if( $this->GaPages->save($page) ){
                $this->response->body(json_encode($page));
            }else{
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
                return $this->response;
            }

            $i++;
        }

        return $this->response;
        
    }

  /**
     * get Pages
     *
     * @return \Cake\Http\Response|void
     */
    public function getPages($id)
    {
        $this->loadModel('GaPages');

        $currentPage = $this->GaPages->get($id);




        $pages = $this->GaPages->find('all')
            ->where([
                'active' => 1,
                'analysis_groups_id' => $currentPage->analysis_groups_id
            ])
            ->order(['page_order' => 'ASC']);

        $this->response->type('json');
        $this->response->body(json_encode($pages));
        return $this->response;
    }

     /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function getPageWithProduct($id)
    {
        $this->loadModel('GaPages');
        $this->loadModel('Contents');

        $currentPage = $this->GaPages->get($id);

        // $page = $this->GaPages->find()
        //     ->where([
        //         'GaPages.active' => 1,
        //         'GaPages.id' => $id
        //         ]);

        $content = $this->Contents->find()
            ->where([
                'Contents.active' => 1,
                'Contents.analysis_groups_id' => $currentPage->analysis_groups_id
                ]);


        // $currentPage->contents = $content;

        $this->response->type('json');
        $this->response->body(json_encode($currentPage));
        return $this->response;
    }

    //Agrega una pagina nueva
    public function add()
    {

        $this->loadModel('GaPages');
        $this->loadComponent('Casaideas');

        $currentPage = $this->GaPages->find()
            ->where([
                'active' => 1,
                'id' => $this->request->getData('id_page')
                ])
            ->first();



        $lastPage = $this->GaPages->find()
            ->where([
                'active' => 1,
                'analysis_groups_id' => $currentPage->analysis_groups_id
                ])
            ->order(['page_order' => 'DESC'])
            ->first();

        $newPage = $this->GaPages->newEntity();
        $newPage->active = 1;
        $newPage->analysis_groups_id = $currentPage->analysis_groups_id;
        $newPage->page_order = ($lastPage->page_order) + 1;
        $newPage->x_value = 5;
        $newPage->y_value = 5;


        if ($this->GaPages->save($newPage)) {

            $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

            if($updateMagazine){
                $this->response->type('json');
                $this->response->body(json_encode($newPage));
            }else{
                $this->response->body(json_encode('error'));
            }

        } else {
            $this->response->body(json_encode('error'));
            $this->response->statusCode(500);
        }
        return $this->response;

    }

    //Agrega o edita el cuadro de estrategia
    public function addSquareStrategy()
    {
        // pr($this->request->getData());
        $this->loadComponent('Casaideas');
        $this->loadModel('ContentGroups');

        $currentContentGroup = $this->ContentGroups->find()
            ->where([
                'active' => 1,
                'cg_order' => $this->request->getData('yVal'),
                'ga_page_id' => $this->request->getData('pageId')
                ])
            ->first();

        if(!empty($currentContentGroup)){
            $currentContentGroup->title = $this->request->getData('squareStrategy');
            if ($this->ContentGroups->save($currentContentGroup)) {

                $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

                if($updateMagazine){
                    $this->response->type('json');
                    $this->response->body(json_encode($currentContentGroup));
                }else{
                    $this->response->body(json_encode('error'));
                    $this->response->statusCode(500);
                }

            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
        }else{
            $newContentGroup = $this->ContentGroups->newEntity();
            $newContentGroup->active = 1;
            $newContentGroup->title = $this->request->getData('squareStrategy');
            $newContentGroup->subtitle = "";
            $newContentGroup->title_bg_color = 'black';
            $newContentGroup->cg_order = $this->request->getData('yVal');
            $newContentGroup->ga_page_id = $this->request->getData('pageId');

            if ($this->ContentGroups->save($newContentGroup)) {

                $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

                if($updateMagazine){
                    $this->response->type('json');
                    $this->response->body(json_encode($newContentGroup));
                }else{
                    $this->response->body(json_encode('error'));
                }

            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
        }

        return $this->response;
    }

    // Agrega o edita el subtitulo de estrategia
    public function addSubtitleStrategy()
    {
        //pr($this->request);
        $this->loadComponent('Casaideas');
        $this->loadModel('ContentGroups');

        $currentContentGroup = $this->ContentGroups->find()
            ->where([
                'active' => 1,
                'cg_order' => $this->request->getData('yVal'),
                'ga_page_id' => $this->request->getData('pageId')
                ])
            ->first();

        if(!empty($currentContentGroup)){
            $currentContentGroup->subtitle = $this->request->getData('subtitleStrategy');
            if ($this->ContentGroups->save($currentContentGroup)) {

                $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

                if($updateMagazine){
                    $this->response->type('json');
                    $this->response->body(json_encode($currentContentGroup));
                }else{
                    $this->response->body(json_encode('error'));
                    $this->response->statusCode(500);
                }

            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
        }else{
            $newContentGroup = $this->ContentGroups->newEntity();
            $newContentGroup->active = 1;
            $newContentGroup->title = "";
            $newContentGroup->subtitle = $this->request->getData('subtitleStrategy');;
            $newContentGroup->title_bg_color = 'black';
            $newContentGroup->cg_order = $this->request->getData('yVal');
            $newContentGroup->ga_page_id = $this->request->getData('pageId');

            if ($this->ContentGroups->save($newContentGroup)) {

                $updateMagazine = $this->Casaideas->updateLastModification($this->request->getData('id_cat'));

                if($updateMagazine){
                    $this->response->type('json');
                    $this->response->body(json_encode($newContentGroup));
                }else{
                    $this->response->body(json_encode('error'));
                    $this->response->statusCode(500);
                }
                
            } else {
                $this->response->body(json_encode('error'));
                $this->response->statusCode(500);
            }
        }

        return $this->response;
    }


}
