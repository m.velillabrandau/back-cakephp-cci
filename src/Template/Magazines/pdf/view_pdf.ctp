<!-- echo $magazine->id .'</br>';
echo $magazine->ferias_id.'</br>';
echo $magazine->feria_alias.'</br>';
echo $magazine->name_feria.'</br>';
echo $magazine->feria_desde.'</br>';
#recorrer los mundos de la magazine -->
<!-- FOREACH MUNDOS -->
<?php foreach ($magazine->worlds as $world) : ?>


<?php if( $world->with_cover != 0 ){ ?>

	<div class="portada">
		<div class="valores">
			<div class="titulo">
				<span style="font-size:34pt; float:right;">
					<?= strtoupper($magazine->name_feria); ?> / <span style="font-weight: bold;">FERIA <?= strtoupper($magazine->ferias_id); ?></span>
				</span>
			</div>
			<div class="clearfix"></div>
			<div class="arrival-date">
				<span style="float:right; font-size:15pt; padding-top: 5px"><?= $magazine->feria_desde; ?></span>
			</div>
			<div class="clearfix"></div>
			<?php
				if($world->nivel1_id == 10000){
					$bgColor = '#E30613';
				} else {
					$bgColor = '#F39200';
				}
			?>
			<div style="min-width:50%;background-color: <?= $bgColor ?>;min-height:50px;float:right; padding:2px 5px 2px 20px;margin-top: 5px">
				<span style="font-size:30pt; color:white;float:right;"><?= mb_strtolower($world->name, 'UTF-8'); ?></span>
			</div>
			<div class="clearfix"></div>
			<div class="logo-portada">
				<?= $this->Html->image('ci_logo.png',['fullBase' => true, 'style' => 'width:20%;float:right;']); ?>
			</div>
		</div>
	</div>
	<div class="pbreak"></div>

<?php } ?>


	<!-- echo '###WORLD#######</br>';
	echo $world->id .'</br>';
	echo $world->bg_color.'</br>';
	echo $world->w_order.'</br>';
	echo $world->name.'</br>'; -->
	<!-- FOREACH ANALYSIS GROUP -->
	<?php foreach ($world->analysis_groups as $ag) : ?>
		<!-- IF PAGES -->
		<?php if (!empty($ag->ga_pages)) : ?>


			<!-- # code...
			echo '---AG----</br>';
			echo $ag->id .'</br>';
			echo $ag->ag_order.'</br>';
			echo $ag->ag_alias.'</br>';
			echo $ag->name.'</br>'; -->


			<!-- foreach GA PAGES -->
			<?php foreach ($ag->ga_pages as $page) : ?>
				<?php  if(!empty($page->contents)): ?> <!-- Si tiene content se muestra -->
					<div class="page">
						<?php
							$xVal = 210;
							$yVal = 300;
							$cOrder = 0;
							// 2x2
							if ($page->x_value == 2 && $page->y_value == 2) {
								$rowHeight = 140;

								$infoHeight = 40;
								$imageHeight = 100;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;

							}
							//3x3
							elseif ($page->x_value == 3 && $page->y_value == 3) {
								$rowHeight = 90;

								$infoHeight = 30;
								$imageHeight = 60;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							}
							//3x5
							elseif ($page->x_value == 3 && $page->y_value == 5) {
								$rowHeight = 50;

								$infoHeight = 19;
								$imageHeight = 31;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							}
							//3x6
							elseif ($page->x_value == 3 && $page->y_value == 6) {
								$rowHeight = 46;

								$infoHeight = 20;
								$imageHeight = 26;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							}
							//4x4
							elseif ($page->x_value == 4 && $page->y_value == 4) {
								$rowHeight = 71;

								$infoHeight = 25;
								$imageHeight = 46;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							}
							//4x6
							elseif ($page->x_value == 4 && $page->y_value == 6) {
								$rowHeight = 46;

								$infoHeight = 19;
								$imageHeight = 27;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							}
							//5x5
							elseif ($page->x_value == 5 && $page->y_value == 5) {
								$rowHeight = 56;

								$infoHeight = 24;
								$imageHeight = 32;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							}
							//5x6
							elseif ($page->x_value == 5 && $page->y_value == 6) {
								$rowHeight = 46;

								$infoHeight = 19;
								$imageHeight = 27;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							}
							//5x7
							elseif ($page->x_value == 5 && $page->y_value == 7) {

								$rowHeight = 40;

								$infoHeight = 18;
								$imageHeight = 22;

								$fontSize = 9;

								$xCalcWidth = $xVal/$page->x_value;
								$yCalcHeight = $yVal/$page->y_value;
							} else {
								//(5x5 por defecto)
								//por defecto cuando hay matricez que no esten configuradas aca
								$rowHeight = 56;

								$infoHeight = 24;
								$imageHeight = 32;

								$fontSize = 9;

								$xCalcWidth = $xVal/5;
								$yCalcHeight = $yVal/5;
							}
						?>
						<div class="pheader">
							<span class="left" style="font-weight: bold ;font-size: 14pt">
								GA: <?= strtoupper($ag->name); ?>
							</span>
							<span class="right">
								<span style="font-size:12pt">
									<span style="color:#DD2914;"><?= ($world->wld_alias == null) ?  strtoupper($world->name) : strtoupper($world->wld_alias) ; ?></span> / <span style="color:#727277;"><?= strtoupper($magazine->name_feria); ?> / FERIA <?= strtoupper($magazine->ferias_id); ?></span>
								</span>
							</span>
							<div class="clearfix"></div>
							<div class="arrival-date">
								<span class="right" style="color:#727277;font-size:7pt;padding-top: 4px;"><?= $magazine->feria_desde; ?></span>
							</div>
						</div>
						<!-- FOR Y -->
						<?php for($ii=1; $ii <= $page->y_value; $ii++): ?>
							<div class="row" style="min-height: <?= $rowHeight; ?>;max-height: <?= $rowHeight; ?>">

								<?php if(!empty($page->content_groups)): ?>
									<?php foreach($page->content_groups as $cGroups): ?>
										<?php if($page->id == $cGroups->ga_page_id && $ii == $cGroups->cg_order): ?>
											<div class="estrategia">
												<div class="strat-button"><?= $cGroups->title ?></div><span class="strat-2"><?= $cGroups->subtitle ?></span>
											</div>
											<div class="clearfix"></div>
										<?php endif ?>
									<?php endforeach; ?>
								<?php endif; ?>
								<!-- FOR X -->
								<?php for($xx=1; $xx <= $page->x_value; $xx++): ?>
									<?php $cOrder = $cOrder + 1; ?>
									<div class="col" style="min-width: <?= $xCalcWidth ?>mm;max-width: <?= $xCalcWidth ?>mm; min-height: <?= $yCalcHeight ?>mm;max-height: <?= $yCalcHeight ?>mm;">
										<?php foreach($page->contents as $product): ?>
											<?php if ($cOrder == $product->c_order ) : ?>
												<!-- si no tiene hijos es solito, si no es uno agrupado -->
												<?php if (empty($product->childrens) && ($product->content_parent_id == 0)) : ?><!-- SOLITOS -->
													<div class="product">
														<div class="info" style="min-height: <?= $infoHeight?>mm;max-height: <?= $infoHeight?>mm; border-bottom: 1.5px; position: relative; z-index: 5;">


															<table cellspacing="0" cellpadding="0" style="font-size:<?= $fontSize ?>pt;border: none;font-family: MyriadProRegular;min-width:50%;">
																<tr>
																	<td style="vertical-align: middle;"><?= $product->name_desc; ?></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td style="vertical-align: middle;">Diseño: <?= $product->diseno; ?> </td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td style="vertical-align: middle;">Color: <?= $product->color; ?></td>
																	<td align="center"></td>
																	<td></td>
																</tr>
																<?php if(!empty($product->measurements)): ?>
																	<tr>
																		<td style="vertical-align: middle;"><pre><?= $product->measurements; ?></pre></td>
																		<td></td>
																		<td></td>
																	</tr>
																<?php endif; ?>
																<tr>
																	<td style="vertical-align: middle;">SKU: <?= ($product->is_new) ? '<span style="background-color: #FFED00">'.$product->sku_mod.'</span>': $product->sku_mod; ?> <span align="center" class="<?=strlen(abs($product->punta_precio)) > 1 ? 'pp-value': 'pp-value-single'?>"><?= abs($product->punta_precio); ?></span><span class="pdf-symbol"><?= ($product->pp_symbol > 0) ? $product->pp_symbol == 1 ? '( + )' : '( - )' : ' ' ?></span></td>

																	<td align="center"></td>
																	<td style="position:relative;"></td>
																</tr>
															</table>

														</div>



														<div class="row" style="height: <?= $imageHeight?>mm;max-height: <?= $imageHeight?>mm; position: relative;">

																<?= $this->Html->image('c_muestra/'.substr($product->sku, 5).'.jpg',
																['fullBase' => true, 'style' => 'max-width:100%;max-height:100%;position:absolute;left:0;top:0;']); ?>

																<?= ($product->tags_id != null) ? $this->Html->image('icon_4x/'.$product->tag->img_url, ['fullBase' => true,'style' => 'width:14mm;height:11mm;position:absolute;right:0;bottom:0;']) : ' ' ?>


														</div>

													</div>

												<?php elseif($product->content_parent_id == 0): ?>
													<!-- AGRUPADOS -->
													<div class="product">
														<div class="info" style="min-height: <?= $infoHeight?>mm;max-height: <?= $infoHeight?>mm;position: relative; z-index: 5;">


															<table cellspacing="0" cellpadding="0" style="font-size:<?= $fontSize ?>pt;border: none;font-family: MyriadProRegular;min-width:50%">
																<tr>
																	<td style="vertical-align: middle;"><?= $product->name_desc; ?></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td style="vertical-align: middle;">Diseño: <?= $product->diseno; ?> </td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td style="vertical-align: middle;">Color: <?= $product->color; ?></td>
																	<td align="center"></td>
																	<td></td>
																</tr>
																<?php if(!empty($product->measurements)): ?>
																	<tr>
																		<td style="vertical-align: middle;"><pre><?= $product->measurements; ?></pre></td>
																		<td></td>
																		<td></td>
																	</tr>
																<?php endif; ?>
																<tr>
																	<td style="vertical-align: middle;">SKU: <?= ($product->is_new) ? '<span style="background-color: #FFED00">'.$product->sku_mod.'</span>': $product->sku_mod; ?> <span align="center" class="<?=strlen(abs($product->punta_precio)) > 1 ? 'pp-value': 'pp-value-single'?>"><?= abs($product->punta_precio); ?></span><span class="pdf-symbol"><?= ($product->pp_symbol > 0) ? $product->pp_symbol == 1 ? '( + )' : '( - )' : ' ' ?></td>
																	<td align="center"></td>
																	<td></td>
																	<td style="position:relative; "></td>
																</tr>

																<?php foreach ($product->childrens as $child): ?>
																	<?php if(!empty($child->measurements)): ?>
																		<tr>
																			<td style="vertical-align: middle;"><pre><?= $child->measurements; ?></pre></td>
																			<td></td>
																			<td></td>
																		</tr>
																	<?php endif; ?>
																	<tr>
																		<td style="vertical-align: middle;">SKU: <?= ($child->is_new) ? '<span style="background-color:#FFED00">'.$child->sku_mod.'</span>': $child->sku_mod; ?> <span align="center" class="<?=strlen(abs($product->punta_precio)) > 1 ? 'pp-value': 'pp-value-single'?>"><?= abs($child->punta_precio); ?></span><span class="pdf-symbol"><?= ($child->pp_symbol > 0) ? $child->pp_symbol == 1 ? '( + )' : '( - )' : ' ' ?></td>
																		<td align="center"></td>
																		<td style="position:relative;"></td>
																	</tr>
																<?php endforeach; ?>

															</table>

														</div>
														<br>
														<div class="row" style="height: <?= $imageHeight?>mm;max-height: <?= $imageHeight?>mm;position: relative;">

															<?= $this->Html->image('c_muestra/'.substr($product->sku, 5).'.jpg', ['fullBase' => true, 'style' => 'max-width:100%;max-height:100%;position:absolute;left:0;top:0;']); ?>

															<?= ($product->tags_id != null) ? $this->Html->image('icon_4x/'.$product->tag->img_url, ['fullBase' => true,'style' => 'width:14mm;height:11mm;position:absolute;left:0;bottom:0;']) : ' ' ?>
														</div>
													</div>
												<?php endif; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									</div>
								<?php endfor; ?><!-- .FOR X -->
							</div>
						<?php endfor; ?><!-- .FOR Y -->
						<!--
						echo '---AG----</br>';
						echo $page->id .'</br>';
						echo $page->page_order.'</br>';
						echo $page->page_alias.'</br>';
						echo $page->name.'</br>';
						echo '---FIN AG----</br>'; -->
						<div class="right-tag" style="">
							<?= (!empty($page->tag)) ? $this->Html->image('icon_4x/'.$page->tag->img_url, ['fullBase' => true, 'class' => 'img-page-tag','style' => '' ]) : ''; ?>
						</div>
					</div>
					<div class="pbreak"></div>
				<?php endif; ?>
			<?php endforeach; ?><!-- .foreach GA PAGES -->
			<!-- echo '---FIN AG----</br>'; -->
		<?php endif; ?><!-- .IF PAGES -->
	<?php endforeach; ?><!-- .FOREACH ANALYSIS GROUP -->
	<!-- echo '###FIN WORLD#######</br></br></br></br></br></br></br></br>'; pr($world);-->
<?php endforeach; ?> <!-- FOREACH MUNDOS -->


