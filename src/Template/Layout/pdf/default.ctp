<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title>Magazine CI</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- RESET -->
        <?= $this->Html->css('reset.css', ['fullBase' => true]) ?>
        <!-- PRINTPDF -->
        <?= $this->Html->css('printpdf.css', ['fullBase' => true]) ?>
    </head>
    <body>
        <div class="container-fluid">
            <?= $this->fetch('content') ?>
        </div>
    </body>
</html>
