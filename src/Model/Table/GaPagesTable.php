<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GaPages Model
 *
 * @property \App\Model\Table\AnalysisGroupsTable|\Cake\ORM\Association\BelongsTo $AnalysisGroups
 * @property \App\Model\Table\TagsTable|\Cake\ORM\Association\BelongsTo $Tags
 *
 * @method \App\Model\Entity\GaPage get($primaryKey, $options = [])
 * @method \App\Model\Entity\GaPage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\GaPage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GaPage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GaPage|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GaPage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GaPage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\GaPage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GaPagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ga_pages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AnalysisGroups', [
            'foreignKey' => 'analysis_groups_id'
        ]);
        $this->belongsTo('Tags', [
            'foreignKey' => 'tags_id'
        ]);
        $this->hasMany('Contents', [
            'foreignKey' => 'ga_pages_id'
        ]);
        $this->hasMany('ContentGroups', [
            'foreignKey' => 'ga_page_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('active');

        $validator
            ->integer('x_value')
            ->allowEmpty('x_value');

        $validator
            ->integer('y_value')
            ->allowEmpty('y_value');

        $validator
            ->integer('page_order')
            ->allowEmpty('page_order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['analysis_groups_id'], 'AnalysisGroups'));
        $rules->add($rules->existsIn(['tags_id'], 'Tags'));

        return $rules;
    }
}
