<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Worlds Model
 *
 * @property \App\Model\Table\MagazinesTable|\Cake\ORM\Association\BelongsTo $Magazines
 * @property \App\Model\Table\Nivel1sTable|\Cake\ORM\Association\BelongsTo $Nivel1s
 * @property \App\Model\Table\Nivel2sTable|\Cake\ORM\Association\BelongsTo $Nivel2s
 *
 * @method \App\Model\Entity\World get($primaryKey, $options = [])
 * @method \App\Model\Entity\World newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\World[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\World|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\World|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\World patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\World[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\World findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WorldsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('worlds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Magazines', [
            'foreignKey' => 'magazines_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Nivel1s', [
            'foreignKey' => 'nivel1_id'
        ]);
        $this->belongsTo('Nivel2s', [
            'foreignKey' => 'nivel2_id'
        ]);
        $this->hasMany('AnalysisGroups', [
            'foreignKey' => 'worlds_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('active');

        $validator
            ->scalar('bg_color')
            ->maxLength('bg_color', 20)
            ->allowEmpty('bg_color');

        $validator
            ->integer('order')
            ->allowEmpty('order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['magazines_id'], 'Magazines'));
        // $rules->add($rules->existsIn(['nivel1_id'], 'Nivel1s'));
        // $rules->add($rules->existsIn(['nivel2_id'], 'Nivel2s'));

        return $rules;
    }

    public function deleteWorldsByMagazineId($magazineId){
        return $this->updateAll(['active' => 0], ['active' => 1, 'magazines_id' => $magazineId]);
    }
}
