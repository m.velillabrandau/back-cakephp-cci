<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AnalysisGroups Model
 *
 * @property \App\Model\Table\WorldsTable|\Cake\ORM\Association\BelongsTo $Worlds
 * @property \App\Model\Table\GrupoanalisesTable|\Cake\ORM\Association\BelongsTo $Grupoanalises
 * @property \App\Model\Table\TagsTable|\Cake\ORM\Association\BelongsTo $Tags
 *
 * @method \App\Model\Entity\AnalysisGroup get($primaryKey, $options = [])
 * @method \App\Model\Entity\AnalysisGroup newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AnalysisGroup[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AnalysisGroup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AnalysisGroup|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AnalysisGroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AnalysisGroup[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AnalysisGroup findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AnalysisGroupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('analysis_groups');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Worlds', [
            'foreignKey' => 'worlds_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Grupoanalises', [
            'foreignKey' => 'grupoanalisis_id'
        ]);
        $this->belongsTo('Tags', [
            'foreignKey' => 'tags_id'
        ]);
        $this->hasMany('Contents', [
            'foreignKey' => 'analysis_groups_id'
        ]);
        $this->hasMany('GaPages', [
            'foreignKey' => 'analysis_groups_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('active');

        $validator
            ->integer('order')
            ->allowEmpty('order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        //$rules->add($rules->existsIn(['worlds_id'], 'Worlds'));
        //$rules->add($rules->existsIn(['tags_id'], 'Tags'));
        //$rules->add($rules->existsIn(['grupoanalisis_id'], 'AnalysisGroups'));

        return $rules;
    }

    public function deleteAnalysisGroupsByWorldId($worldId){
        return $this->updateAll(['active' => 0], ['active' => 1, 'worlds_id' => $worldId]);
    }
}
