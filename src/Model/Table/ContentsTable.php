<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contents Model
 *
 * @property \App\Model\Table\AnalysisGroupsTable|\Cake\ORM\Association\BelongsTo $AnalysisGroups
 * @property \App\Model\Table\ContentGroupsTable|\Cake\ORM\Association\BelongsTo $ContentGroups
 * @property \App\Model\Table\TagsTable|\Cake\ORM\Association\BelongsTo $Tags
 *
 * @method \App\Model\Entity\Content get($primaryKey, $options = [])
 * @method \App\Model\Entity\Content newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Content[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Content|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Content[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Content findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contents');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AnalysisGroups', [
            'foreignKey' => 'analysis_groups_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ContentGroups', [
            'foreignKey' => 'content_groups_id'
        ]);
        $this->belongsTo('Tags', [
            'foreignKey' => 'tags_id'
        ]);
        $this->belongsTo('GaPages', [
            'foreignKey' => 'ga_pages_id'
        ]);
        $this->hasMany('Childrens', [
            'className' => 'Contents',
            'dependent' => true,
            'joinType' => 'INNER',
            'foreignKey' => 'content_parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('active');

        $validator
            ->scalar('sku')
            ->maxLength('sku', 18)
            ->allowEmpty('sku');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        //$rules->add($rules->existsIn(['analysis_groups_id'], 'AnalysisGroups'));
        $rules->add($rules->existsIn(['content_groups_id'], 'ContentGroups'));
        $rules->add($rules->existsIn(['tags_id'], 'Tags'));

        return $rules;
    }

    public function deleteContentByAnalysisGroupId($analysisGroupId){
        return $this->updateAll(['active' => 0], ['active' => 1, 'analysis_groups_id' => $analysisGroupId]);
    }
}
