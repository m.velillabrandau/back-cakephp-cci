<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Datasource\ConnectionManager;

/**
 * World Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $active
 * @property string $bg_color
 * @property int $order
 * @property int $magazines_id
 * @property string $nivel1_id
 * @property string $nivel2_id
 *
 * @property \App\Model\Entity\Magazine $magazine
 * @property \App\Model\Entity\Nivel1 $nivel1
 * @property \App\Model\Entity\Nivel2 $nivel2
 */
class World extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'active' => true,
        'bg_color' => true,
        'order' => true,
        'magazines_id' => true,
        'nivel1_id' => true,
        'nivel2_id' => true,
        'magazine' => true,
        'nivel1' => true,
        'nivel2' => true
    ];

    protected $_virtual = ['name'];

    protected function _getName()
    {
        /*pr('..vf getName');
        pr($this->_properties);exit;*/
        if(isset($this->_properties['nivel1_id']) && isset($this->_properties['nivel2_id'])){

            $connection = ConnectionManager::get('cci_corp');
            $sql   =
            "SELECT `a`.`descn1`, `b`.`descn2`
            FROM  `nivel1` AS `a`
            LEFT JOIN `nivel2` AS `b` ON `a`.`codn1` = `b`.`codn1`
            WHERE
            `a`.`codn1` = ". $this->_properties['nivel1_id'] ."
            AND
            `b`.`codn2` = ". $this->_properties['nivel2_id'] ."
            GROUP BY `b`.`codn1`, `b`.`codn2`";
            $query = $connection->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            $worldName = '';
            foreach ($result as $row) {
                $worldName = reset($row) . " " . end($row);
            }
            return $worldName;

        } else {
            return 'Sin Nombre';
        }
    }
}
