<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Datasource\ConnectionManager;

/**
 * Content Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $active
 * @property string $sku
 * @property int $analysis_groups_id
 * @property int $content_groups_id
 * @property int $tags_id
 *
 * @property \App\Model\Entity\AnalysisGroup $analysis_group
 * @property \App\Model\Entity\ContentGroup $content_group
 * @property \App\Model\Entity\Tag $tag
 */
class Content extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'active' => true,
        'sku' => true,
        'analysis_groups_id' => true,
        'content_groups_id' => true,
        'tags_id' => true,
        'analysis_group' => true,
        'content_group' => true,
        'tag' => true,
        'product_order' => true,
        'madre_id' => true
    ];

    protected $_virtual = ['sku_mod', 'punta_precio', 'name_desc', 'diseno', 'color'];

    protected function _getSkuMod()
    {
        return substr($this->_properties['sku'],5,1).' '.
                substr($this->_properties['sku'],6,6).' '.
                substr($this->_properties['sku'],12,3).' '.
                substr($this->_properties['sku'],15 ,3);
    }

    protected function _getPuntaPrecio()
    {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT preciosug FROM  producto WHERE `ProductoMATNR` = '". $this->_properties['sku'] ."'";
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $puntaPrecio = '';
        foreach ($result as $row) {
            $puntaPrecio = reset($row);
        }
        return $puntaPrecio;
    }

    protected function _getNameDesc()
    {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT `a`.`maddesc` FROM `madre` AS `a` WHERE `a`.`madid` = '" . $this->_properties['madre_id'] ."'";
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $nameDesc = '';
        foreach ($result as $row) {
            $nameDesc = reset($row);
        }
        return $nameDesc;
    }

    protected function _getDiseno()
    {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT `disid` FROM  `producto` WHERE `ProductoMATNR` = '". $this->_properties['sku'] ."'";
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $diseno = '';
        foreach ($result as $row) {
            $diseno = reset($row);
        }
        return $diseno;
    }

    protected function _getColor()
    {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT `colordesc` FROM `colores` AS `a` LEFT JOIN `producto` AS `b` ON `a`.`colorid` = `b`.`colorid` WHERE `b`.`ProductoMATNR` = '". $this->_properties['sku'] ."'";
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $color = '';
        foreach ($result as $row) {
            $color = reset($row);
        }
        return $color;
    }
}
