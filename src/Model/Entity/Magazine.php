<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Datasource\ConnectionManager;

/**
 * Magazine Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $active
 * @property int $ferias_id
 *
 * @property \App\Model\Entity\Feria $feria
 */
class Magazine extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'active' => true,
        'ferias_id' => true,
        'feria' => true
    ];

    protected $_virtual = ['name_feria', 'feria_desde'];


    protected function _getNameFeria()
    {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT ferdesc FROM  ferias WHERE ferid = " . $this->_properties['ferias_id'];
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $feriaName = '';
        foreach ($result as $row) {
            $feriaName = reset($row);
        }
        return $feriaName;
    }

    protected function _getFeriaDesde()
    {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT ferdesde FROM  ferias WHERE ferid = " . $this->_properties['ferias_id'];
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $feriaDesde = '';
        foreach ($result as $row) {
            $feriaDesde = reset($row);
        }
        switch (date('m', strtotime($feriaDesde))) {
            case '01':
                $month = 'ENERO';
                break;
            case '02':
                $month = 'FEBRERO';
                break;
            case '03':
                $month = 'MARZO';
                break;
            case '04':
                $month = 'ABRIL';
                break;
            case '05':
                $month = 'MAYO';
                break;
            case '06':
                $month = 'JUNIO';
                break;
            case '07':
                $month = 'JULIO';
                break;
            case '08':
                $month = 'AGOSTO';
                break;
            case '09':
                $month = 'SEPTIEMBRE';
                break;
            case '10':
                $month = 'OCTUBRE';
                break;
            case '11':
                $month = 'NOVIEMBRE';
                break;
            case '12':
                $month = 'DICIEMBRE';
                break;
            
            default:
                $month = '';
                break;
        }
        $feriaDesde = __('LLEGADA {0} {1}', [$month, date('Y', strtotime($feriaDesde))]);
        return $feriaDesde;
    }
}
