<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Datasource\ConnectionManager;

/**
 * AnalysisGroup Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $active
 * @property int $order
 * @property int $worlds_id
 * @property int $grupoanalisis_id
 * @property int $tags_id
 *
 * @property \App\Model\Entity\World $world
 * @property \App\Model\Entity\Grupoanalise $grupoanalise
 * @property \App\Model\Entity\Tag $tag
 */
class AnalysisGroup extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'active' => true,
        'order' => true,
        'worlds_id' => true,
        'grupoanalisis_id' => true,
        'tags_id' => true,
        'world' => true,
        'grupoanalise' => true,
        'tag' => true,
    ];

    protected $_virtual = ['name'];

    protected function _getName()
    {
        $connection = ConnectionManager::get('cci_corp');
        $sql   = "SELECT gandesc FROM  grupoanalisis WHERE ganid = " . $this->_properties['grupoanalisis_id'];
        $query = $connection->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $gaName = '';
        foreach ($result as $row) {
            $gaName = reset($row);
        }
        return $gaName;
    }

}
