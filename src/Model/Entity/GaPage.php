<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GaPage Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $active
 * @property int $analysis_groups_id
 * @property int $x_value
 * @property int $y_value
 * @property int $page_order
 * @property int $tags_id
 *
 * @property \App\Model\Entity\AnalysisGroup $analysis_group
 * @property \App\Model\Entity\Tag $tag
 */
class GaPage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'active' => true,
        'analysis_groups_id' => true,
        'x_value' => true,
        'y_value' => true,
        'page_order' => true,
        'tags_id' => true,
        'analysis_group' => true,
        'tag' => true
    ];
}
